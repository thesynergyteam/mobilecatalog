<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false, 'verify' => false, 'reset' => false,]);

Route::post('/login', 'Auth\LoginController@login');
Route::middleware(['auth'])->group(function() {
	Route::get('/querytsz/{tsz}','ConfigurationController@queryTsz');
	Route::get('/queryimei/{imei}','ConfigurationController@queryIMEI');
	Route::get('/querysim/{icc_tel}/{id}','ConfigurationController@querySim');
	Route::get('/queryaccessory','ConfigurationController@queryAccessory');

	Route::get('/filedownload', function (Request $request) {
		$utvonal = $request->input('utvonal');
		$fajlnev = $request->input('fajlnev');
		return response()->download(storage_path('app/' . $utvonal), $fajlnev);
	});
	Route::get('/showfile', function (Request $request) {
		$utvonal = $request->input('utvonal');
		$fajlnev = $request->input('fajlnev');
		return response()->file(storage_path('app/' . $utvonal));
	});
	Route::get('/delfile/{tipus}/{id}', function (Request $request, $tipus, $id) {
		switch ($tipus){
			case 'szamhordozas': $file = \App\SzamhordozasFajl::find($id);
				break;
			case 'konfiguracio':  $file = \App\Fajl::find($id);
				break;
		}
		if ($file && is_file(storage_path('app/' .$file->utvonal))){
			unlink(storage_path('app/' .$file->utvonal));
			$file->delete();
			return redirect()->back()
				->with('flash_message', 'Fájl törölve!');
		} else {
			return redirect()->back()
				->withErrors(['Fájl nem található!']);
		}
	})->middleware('permission:Fájlok törlése');
	Route::get('/suppliers/{id}/history','SupplierController@history')->name('suppliers.history');
	Route::get('/configurations/update_users','ConfigurationController@updateUsers');
	Route::get('/configurations/{id}/history','ConfigurationController@history')->name('configurations.history');
	Route::get('/configurations/query','ConfigurationController@query')->name('configurations.query');
	Route::get('/configurations/fetch_data', 'ConfigurationController@fetch_data');
	Route::get('/configurations/{id}/download', 'ConfigurationController@saveConfiguration')->name('configurations.download');	//download konfiguracio Word file with given id
	Route::get('/sims/masscreate','SimController@masscreate')->name('sims.masscreate');
	Route::post('/sims/csvimport','SimController@csvimport')->name('sims.csvimport');
	Route::get('/devices/masscreate','DeviceController@masscreate')->name('devices.masscreate');
	Route::post('/devices/csvimport','DeviceController@csvimport')->name('devices.csvimport');
	//Sim icc kódok szűkítése gépelés közben
	Route::get('/sims/fetch_data', 'SimController@fetch_data');
	Route::get('/devices/fetch_data', 'DeviceController@fetch_data');
	Route::get('/transfers/{id}/download', 'TransferController@downloadTransfer')->name('transfers.download');
	Route::get('/transfers/{id}/setready', 'TransferController@setReadyTransfer')->name('transfers.setready');
	Route::resources([
		'users' => 'UserController',
		'roles' => 'RoleController',
		'permissions' => 'PermissionController',
		'devices' => 'DeviceController',
		'suppliers' => 'SupplierController',
		'configurations' => 'ConfigurationController',
		'sims' => 'SimController',
		'types' => 'TypeController',
		'accessories' => 'AccessoryController',
		'transfers' => 'TransferController'
	]);
});