<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		$min=10000000;
		$max=99999999;
		$imin=100000000000000;
		$imax=999999999999999;
		DB::table('mobile__beszallito')->insert([
			['partner' => 'Telenor Magyarország Zrt.', 'cim' => '2045 Törökbálint, Pannon út 1', 'telefon' => '1220']
		]);

		DB::table('mobile__tipus')->insert([
			['gyarto' => 'Samsung', 'tipus' => 'Galaxy S6'],
			['gyarto' => 'Samsung', 'tipus' => 'Galaxy S7'],
			['gyarto' => 'Samsung', 'tipus' => 'Galaxy S8'],
			['gyarto' => 'Samsung', 'tipus' => 'Galaxy S9'],
			['gyarto' => 'Samsung', 'tipus' => 'Galaxy S10'],
			['gyarto' => 'Samsung', 'tipus' => 'Galaxy S21']
		]);

		DB::table('mobile__kategoria')->insert([
			['nev' => 'Vezérigazgató', 'limit' => 30000],
			['nev' => 'Vezérigazgató h.', 'limit' => 30000],
			['nev' => 'Igazgató', 'limit' => 30000],
			['nev' => 'Osztályvezető', 'limit' => 20000],
			['nev' => 'Csoportvezető', 'limit' => 10000],
			['nev' => 'más', 'limit' => 5000]
		]);

		DB::table('mobile__statusz')->insert([
			['nev' => 'aktív - kiadva'],['nev' => 'selejt'],['nev' => 'garancia'],['nev' => 'lopás'],['nev' => 'új- raktáron'],['nev' => 'használt - raktáron'],['nev' => 'sérült - raktáron'],
		]);
		DB::table('mobile__simstatusz')->insert([
			['nev' => 'raktáron'],['nev' => 'kiadva'],['nev' => 'selejt'],['nev' => 'újra lecserélve'],
		]);
	}
}
