<?php

use Illuminate\Database\Seeder;

class NetCsomagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mobile__net_csomagok')->insert([
			['nev' => 'nincs net csomag', 'limit' => '-', 'up' => '-', 'down' => '-'],
			['nev' => 'Hipernet Start', 'limit' => '4GB', 'up' => '1', 'down' => '7'],
			['nev' => 'Hipernet Active', 'limit' => '7GB', 'up' => '2', 'down' => '10'],
			['nev' => 'Hipernet Medium', 'limit' => '13GB', 'up' => '5', 'down' => '30'],
			['nev' => 'Hipernet Heavy', 'limit' => '24GB', 'up' => '10', 'down' => '60'],
			['nev' => 'Hipernet Unlimited', 'limit' => 'korlátlan', 'up' => '20', 'down' => '150'],
			['nev' => 'Mobil Online 500M', 'limit' => '500MB', 'up' => ' ', 'down' => ' '],
			['nev' => 'Mobil Online 1G', 'limit' => '1GB', 'up' => ' ', 'down' => ' '],
			['nev' => 'Mobil Online 2G', 'limit' => '2GB', 'up' => ' ', 'down' => ' ']
		]);
    }
}
