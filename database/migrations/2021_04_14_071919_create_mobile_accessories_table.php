<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMobileAccessoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile__accessories', function (Blueprint $table) {
            $table->id();
			$table->string('nev');
			$table->string('leltariszam')->nullable();
			$table->bigInteger('beszallito_id')->unsigned();
			$table->bigInteger('felhasznalo_id')->unsigned()->nullable();
			$table->bigInteger('konfiguracio_id')->unsigned()->nullable();
			$table->string('megjegyzes')->nullable();
			$table->unsignedInteger('ertek');
			$table->date('datum');
			$table->bigInteger('statusz_id')->unsigned()->nullable();
			$table->foreign('felhasznalo_id')
				->references('id')
				->on('mobile__felhasznalo')
				->onDelete('set null');
			$table->foreign('beszallito_id')
				->references('id')
				->on('mobile__beszallito')
				->onDelete('cascade');
			$table->foreign('konfiguracio_id')
				->references('id')
				->on('mobile__konfig')
				->onDelete('set null');
			$table->foreign('statusz_id')
				->references('id')
				->on('mobile__statusz')
				->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile__accessories');
    }
}
