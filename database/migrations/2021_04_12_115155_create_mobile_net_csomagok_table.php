<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMobileNetCsomagokTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile__net_csomagok', function (Blueprint $table) {
            $table->id();
			$table->string('nev');
			$table->string('limit');
			$table->string('up');
			$table->string('down');
        });

		Artisan::call('db:seed',['--force' => true, '--class' => 'NetCsomagSeeder' ]);
		
		Schema::table('mobile__sim', function (Blueprint $table) {
			$table->bigInteger('netcsomag_id')->unsigned()->default(1);
			$table->foreign('netcsomag_id')->references('id')->on('mobile__net_csomagok');
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('mobile__sim', function (Blueprint $table) {
			$table->dropForeign(['netcsomag_id']);
			$table->dropColumn(['netcsomag_id']);
		});
        Schema::dropIfExists('mobile__net_csomagok');
    }
}
