<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBeruhazonCoumnToDevice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mobile__eszkoz', function (Blueprint $table) {
            $table->string('beruhazasi_id')->nullable();
        });
		Schema::table('mobile__accessories', function (Blueprint $table) {
            $table->string('beruhazasi_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mobile__eszkoz', function (Blueprint $table) {
            $table->dropColumn(['beruhazasi_id']);
        });
		Schema::table('mobile__accessories', function (Blueprint $table) {
            $table->dropColumn(['beruhazasi_id']);
        });
    }
}
