<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSzamhordozasFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('mobile__szamhordozas_files', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('szamhordozas_id')->unsigned();
			$table->string('utvonal')->nullable();
			$table->string('fajlnev')->nullable();
			$table->bigInteger('moduser_id')->unsigned()->nullable();
			$table->timestamp('mod_datum')->useCurrent();

			$table->foreign('moduser_id')
				->references('id')
				->on('mobile__users')
				->onDelete('set null');
			$table->index('szamhordozas_id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile__szamhordozas_files');
    }
}
