<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSzamhordozasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile__szamhordozas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('sim_id')->unsigned()->nullable();
			$table->bigInteger('felhasznalo_id')->unsigned()->nullable();
			$table->date('datum');
			$table->bigInteger('moduser_id')->unsigned()->nullable();
			$table->string('statusz')->default('Elhordozás folyamatban');
			$table->string('megjegyzes')->nullable();

			$table->foreign('felhasznalo_id')
				->references('id')
				->on('mobile__felhasznalo')
				->onDelete('set null');
			$table->foreign('sim_id')
				->references('id')
				->on('mobile__sim')
				->onDelete('set null');
			$table->foreign('moduser_id')
				->references('id')
				->on('mobile__users')
				->onDelete('set null');
        });
		Schema::table('mobile__felhasznalo', function (Blueprint $table) {
			$table->string('anyjaneve')->nullable();
			$table->string('lakcim')->nullable();
			$table->date('szuletesi_ido')->nullable();
			$table->string('szemigszam')->nullable();
			$table->string('koltseghely')->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile__szamhordozas');
		Schema::table('mobile__felhasznalo', function (Blueprint $table) {
			$table->dropColumn(['anyjaneve', 'lakcim', 'szuletesi_ido', 'szemigszam', 'koltseghely']);
		});
    }
}
