<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeManyToManyConfigDeviceSim extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile__konfig_has_eszkoz', function (Blueprint $table) {
            $table->unsignedBigInteger('konfig_id');
			$table->unsignedBigInteger('eszkoz_id');

            $table->foreign('konfig_id')
                ->references('id')
                ->on('mobile__konfig')
                ->onDelete('cascade');
			$table->foreign('eszkoz_id')
                ->references('id')
                ->on('mobile__eszkoz')
                ->onDelete('cascade');

            $table->primary(['konfig_id', 'eszkoz_id'], 'mobile__konfig_has_eszkoz_primary');
        });

		Schema::create('mobile__konfig_has_sim', function (Blueprint $table) {
            $table->unsignedBigInteger('konfig_id');
			$table->unsignedBigInteger('sim_id');

            $table->foreign('konfig_id')
                ->references('id')
                ->on('mobile__konfig')
                ->onDelete('cascade');
			$table->foreign('sim_id')
                ->references('id')
                ->on('mobile__sim')
                ->onDelete('cascade');

            $table->primary(['konfig_id', 'sim_id'], 'mobile__konfig_has_sim_primary');
        });

		Schema::create('mobile__konfig_has_accessory', function (Blueprint $table) {
            $table->unsignedBigInteger('konfig_id');
			$table->unsignedBigInteger('accessory_id');

            $table->foreign('konfig_id')
                ->references('id')
                ->on('mobile__konfig')
                ->onDelete('cascade');
			$table->foreign('accessory_id')
                ->references('id')
                ->on('mobile__accessories')
                ->onDelete('cascade');

            $table->primary(['konfig_id', 'accessory_id'], 'mobile__konfig_has_accessory_primary');
        });

		DB::table('mobile__konfig_has_eszkoz')->insertUsing(['konfig_id','eszkoz_id'], DB::table('mobile__konfig')->select(['id as konfig_id', 'eszkoz_id'])->whereNotNull('eszkoz_id'));
		DB::table('mobile__konfig_has_sim')->insertUsing(['konfig_id','sim_id'], DB::table('mobile__konfig')->select(['id as konfig_id', 'sim_id'])->whereNotNull('sim_id'));
		DB::table('mobile__konfig_has_accessory')->insertUsing(['konfig_id','accessory_id'], DB::table('mobile__accessories')->select(['id as accessory_id', 'konfig_id'])->whereNotNull('konfig_id'));

		Schema::table('mobile__konfig', function (Blueprint $table) {
			$table->dropForeign(['eszkoz_id']);
			$table->dropForeign(['sim_id']);
			$table->dropColumn(['eszkoz_id','sim_id']);
		});
		Schema::table('mobile__accessories', function (Blueprint $table) {
			$table->dropForeign(['konfiguracio_id']);
			$table->dropColumn(['konfiguracio_id']);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('mobile__accessories', function (Blueprint $table) {
            $table->bigInteger('konfiguracio_id')->unsigned()->nullable();
			$table->foreign('konfiguracio_id')
				->references('id')
				->on('mobile__konfig')
				->onDelete('set null');
        });
		$configaccessories = DB::table('mobile__konfig_has_accessory')->get();
		foreach ($configaccessories as $ca) {
			DB::table('mobile__accessories')->where('id',$ca->accessory_id)->update(['konfig_id' => $ca->konfig_id]);
		}
        Schema::table('mobile__konfig', function (Blueprint $table) {
            $table->bigInteger('eszkoz_id')->unsigned()->nullable();
			$table->bigInteger('sim_id')->unsigned()->nullable();
			$table->foreign('eszkoz_id')
				->references('id')
				->on('mobile__eszkoz')
				->onDelete('set null');
			$table->foreign('sim_id')
				->references('id')
				->on('mobile__sim')
				->onDelete('set null');
        });
		$configdevices = DB::table('mobile__konfig_has_eszkoz')->get();
		foreach ($configdevices as $cd) {
			DB::table('mobile__konfig')->where('id',$cd->konfig_id)->update(['eszkoz_id' => $cd->eszkoz_id]);
		}
		$configsims = DB::table('mobile__konfig_has_sim')->get();
		foreach ($configsims as $cs) {
			DB::table('mobile__konfig')->where('id',$cs->konfig_id)->update(['sim_id' => $cs->sim_id]);
		}

		Schema::dropIfExists('mobile__konfig_has_eszkoz');
		Schema::dropIfExists('mobile__konfig_has_sim');
		Schema::dropIfExists('mobile__konfig_has_accessory');
    }
}
