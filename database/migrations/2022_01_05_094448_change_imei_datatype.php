<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeImeiDatatype extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mobile__eszkoz', function (Blueprint $table) {
			$table->string('IMEI')->change();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mobile__eszkoz', function (Blueprint $table) {
			$table->bigInteger('IMEI')->unsigned()->change();
		});
    }
}
