<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMobileTables extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mobile__felhasznalo', function (Blueprint $table) {
			$table->id();
			$table->string('nev');
			$table->string('tsz');
		});
		Schema::create('mobile__beszallito', function (Blueprint $table) {
			$table->id();
			$table->string('partner');
			$table->string('cim');
			$table->string('kontakt')->nullable();
			$table->string('email')->nullable();
			$table->string('telefon')->nullable();
			$table->string('megjegyzes')->nullable();
			$table->tinyInteger('aktiv')->default(1);
			$table->bigInteger('moduser_id')->unsigned()->nullable();
		});
		Schema::create('mobile__beszallito_history', function (Blueprint $table) {
			$table->bigInteger('id')->unsigned();
			$table->string('partner');
			$table->string('cim');
			$table->string('kontakt')->nullable();
			$table->string('email')->nullable();
			$table->string('telefon')->nullable();
			$table->tinyInteger('aktiv')->default(1);
			$table->bigInteger('moduser_id')->unsigned()->nullable();
			$table->timestamp('mod_datum')->useCurrent();

			$table->foreign('moduser_id')
				->references('id')
				->on('mobile__users')
				->onDelete('set null');
			$table->index('id');
		});
		Schema::create('mobile__tipus', function (Blueprint $table) {
			$table->id();
			$table->string('gyarto');
			$table->string('tipus');
		});
		Schema::create('mobile__kategoria', function (Blueprint $table) {
			$table->id();
			$table->integer('limit');
			$table->string('nev');
		});
		Schema::create('mobile__statusz', function (Blueprint $table) {
			$table->id();
			$table->string('nev');
		});
		Schema::create('mobile__simstatusz', function (Blueprint $table) {
			$table->id();
			$table->string('nev');
		});
		Schema::create('mobile__eszkoz', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('beszallito_id')->unsigned()->nullable();
			$table->bigInteger('felhasznalo_id')->unsigned()->nullable();
			$table->bigInteger('tipus_id')->unsigned()->nullable();
			$table->unsignedInteger('ertek');
			$table->string('szamlaszam');
			$table->date('datum');
			//$table->date('garanciadatum');
			$table->bigInteger('IMEI')->unsigned()->unique();
			$table->bigInteger('IMEI2')->unsigned()->nullable();
			$table->string('gyartasiszam')->nullable();
			$table->bigInteger('statusz_id')->unsigned()->nullable();

			$table->foreign('beszallito_id')
				->references('id')
				->on('mobile__beszallito')
				->onDelete('set null');
			$table->foreign('felhasznalo_id')
				->references('id')
				->on('mobile__felhasznalo')
				->onDelete('set null');
			$table->foreign('tipus_id')
				->references('id')
				->on('mobile__tipus')
				->onDelete('set null');
			$table->foreign('statusz_id')
				->references('id')
				->on('mobile__statusz')
				->onDelete('set null');
		});
		Schema::create('mobile__sim', function (Blueprint $table) {
			$table->id();
			$table->string('icc')->unique();
			$table->bigInteger('beszallito_id')->unsigned()->nullable();
			$table->bigInteger('felhasznalo_id')->unsigned()->nullable();
			$table->string('megrendelesszam');
			$table->string('rendelesszam');
			$table->string('szallitolevelszam');
			$table->string('telefonszam');
			$table->date('datum');
			$table->string('tipus')->default('hang');
			$table->string('pin1')->nullable();
			$table->string('pin2')->nullable();
			$table->string('puk1')->nullable();
			$table->string('puk2')->nullable();
			$table->bigInteger('simstatusz_id')->unsigned()->default(1);

			$table->foreign('beszallito_id')
				->references('id')
				->on('mobile__beszallito')
				->onDelete('cascade');
			$table->foreign('felhasznalo_id')
				->references('id')
				->on('mobile__felhasznalo')
				->onDelete('set null');
			$table->foreign('simstatusz_id')
				->references('id')
				->on('mobile__simstatusz')
				->onDelete('cascade');
		});
		Schema::create('mobile__konfig', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('eszkoz_id')->unsigned()->nullable();
			$table->bigInteger('sim_id')->unsigned()->nullable();
			$table->bigInteger('felhasznalo_id')->unsigned()->nullable();
			$table->timestamp('datum')->useCurrent();
			$table->bigInteger('moduser_id')->unsigned()->nullable();
			$table->bigInteger('kategoria_id')->unsigned()->nullable();
			$table->string('statusz')->default('kiadás');
			$table->string('megjegyzes')->nullable();

			$table->foreign('eszkoz_id')
				->references('id')
				->on('mobile__eszkoz')
				->onDelete('set null');
			$table->foreign('felhasznalo_id')
				->references('id')
				->on('mobile__felhasznalo')
				->onDelete('set null');
			$table->foreign('sim_id')
				->references('id')
				->on('mobile__sim')
				->onDelete('set null');
			$table->foreign('moduser_id')
				->references('id')
				->on('mobile__users')
				->onDelete('set null');
			$table->foreign('kategoria_id')
				->references('id')
				->on('mobile__kategoria')
				->onDelete('set null');
		});
		Schema::create('mobile__konfig_files', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('konfiguracio_id')->unsigned();
			$table->string('utvonal')->nullable();
			$table->string('fajlnev')->nullable();
			$table->bigInteger('moduser_id')->unsigned()->nullable();
			$table->timestamp('mod_datum')->useCurrent();

			$table->foreign('moduser_id')
				->references('id')
				->on('mobile__users')
				->onDelete('set null');
			$table->index('konfiguracio_id');
		});
		Schema::create('mobile__konfig_history', function (Blueprint $table) {
			$table->id();
			$table->bigInteger('konfiguracio_id')->unsigned();
			$table->bigInteger('kategoria_id')->unsigned()->nullable();
			$table->string('statusz')->nullable();
			$table->string('megjegyzes')->nullable();
			$table->bigInteger('moduser_id')->unsigned()->nullable();
			$table->timestamp('mod_datum')->useCurrent();

			$table->foreign('moduser_id')
				->references('id')
				->on('mobile__users')
				->onDelete('set null');
			$table->foreign('kategoria_id')
				->references('id')
				->on('mobile__kategoria')
				->onDelete('set null');
		});
		DB::unprepared('CREATE TRIGGER `mobile__beszallito_insert` AFTER INSERT ON `mobile__beszallito` FOR EACH ROW
				BEGIN
					insert into mobile__beszallito_history (id,partner,cim,kontakt,email,telefon,aktiv,moduser_id) values
						(NEW.id,NEW.partner,NEW.cim,NEW.kontakt,NEW.email,NEW.telefon,NEW.aktiv,NEW.moduser_id);
				END');
		DB::unprepared('CREATE TRIGGER `mobile__beszallito_update` AFTER UPDATE ON `mobile__beszallito` FOR EACH ROW
				BEGIN
					IF (!(NEW.partner<=>OLD.partner) || !(NEW.cim <=> OLD.cim) || !(NEW.kontakt<=>OLD.kontakt) || !(NEW.email<=>OLD.email) || !(NEW.telefon <=> OLD.telefon) || NEW.aktiv<>OLD.aktiv) THEN
					insert into mobile__beszallito_history (id,partner,cim,kontakt,email,telefon,aktiv,moduser_id) values
						(NEW.id,NEW.partner,NEW.cim,NEW.kontakt,NEW.email,NEW.telefon,NEW.aktiv,NEW.moduser_id);
					END IF;
				END');
		DB::unprepared('CREATE TRIGGER `mobile__konfig_insert` AFTER INSERT ON `mobile__konfig` FOR EACH ROW
				BEGIN
					insert into mobile__konfig_history (konfiguracio_id,kategoria_id,statusz,megjegyzes,moduser_id) values
						(NEW.id,NEW.kategoria_id,NEW.statusz,NEW.megjegyzes,NEW.moduser_id);
				END');
		DB::unprepared('CREATE TRIGGER `mobile__konfig_update` AFTER UPDATE ON `mobile__konfig` FOR EACH ROW
				BEGIN
					IF (!(NEW.kategoria_id<=>OLD.kategoria_id) || !(NEW.statusz<=>OLD.statusz) || !(NEW.megjegyzes <=> OLD.megjegyzes) ) THEN
						IF (!(NEW.statusz<=>OLD.statusz)) THEN
							insert into mobile__konfig_history (konfiguracio_id,kategoria_id,statusz,megjegyzes,moduser_id) values
							(NEW.id,NEW.kategoria_id,NEW.statusz,NEW.megjegyzes,NEW.moduser_id);
						ELSE
							insert into mobile__konfig_history (konfiguracio_id,kategoria_id,statusz,megjegyzes,moduser_id) values
							(NEW.id,NEW.kategoria_id,null,NEW.megjegyzes,NEW.moduser_id);
						END IF;
					END IF;
				END');
		Artisan::call('db:seed',['--force' => true ]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('mobile__konfig_files');
		Schema::dropIfExists('mobile__konfig_history');
		Schema::dropIfExists('mobile__konfig');
		Schema::dropIfExists('mobile__sim');
		Schema::dropIfExists('mobile__eszkoz');
		Schema::dropIfExists('mobile__tipus');
		Schema::dropIfExists('mobile__kategoria');
		Schema::dropIfExists('mobile__statusz');
		Schema::dropIfExists('mobile__beszallito_history');
		Schema::dropIfExists('mobile__beszallito');
		Schema::dropIfExists('mobile__felhasznalo');
		Schema::dropIfExists('mobile__simstatusz');
		DB::unprepared('DROP TRIGGER IF EXISTS mobile__beszallito_insert');
		DB::unprepared('DROP TRIGGER IF EXISTS mobile__beszallito_update');
		DB::unprepared('DROP TRIGGER IF EXISTS mobile__konfig_insert');
		DB::unprepared('DROP TRIGGER IF EXISTS mobile__konfig_update');
	}
}
