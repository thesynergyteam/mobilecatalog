<?php
/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App;

class Felhasznalo extends BaseModel
{

	protected $table = 'mobile__felhasznalo';
	public $timestamps = false;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'nev', 'tsz', 'anyjaneve', 'lakcim', 'szuletesi_ido', 'szemigszam', 'koltseghely'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		//
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		//
	];

	public function getNevTszAttribute()
	{
		return $this->nev.($this->tsz?' ('.$this->tsz.')':'');
	}
	public function eszkozok()
	{
		return $this->hasMany(\App\Mobileszkoz::class,'felhasznalo_id');
	}
	public function simek()
	{
		return $this->hasMany(\App\Sim::class,'felhasznalo_id');
	}
	public function konfigok()
	{
		return $this->hasMany(\App\Konfiguracio::class,'felhasznalo_id');
	}
}
