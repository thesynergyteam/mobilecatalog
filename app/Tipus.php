<?php
/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App;

class Tipus extends BaseModel
{
	protected $table = 'mobile__tipus';
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'gyarto','tipus'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
		//
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //'email_verified_at' => 'datetime',
    ];
	public function getNevAttribute()
	{
		return $this->gyarto.' '.$this->tipus;
	}
	public function eszkoz()
	{
		return $this->HasMany('App\Mobileszkoz');
	}
}
