<?php
/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2022(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App;

class SzamhordozasFajl extends BaseModel
{
	protected $table = 'mobile__szamhordozas_files';
	public $timestamps = false;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'szamhordozas_id', 'utvonal', 'fajlnev','moduser_id'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		//
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		//
	];

	public function szamhordozas()
	{
		return $this->belongsTo('App\Szamhordozas');
	}
	public function getDatumAttribute() {
		return \Carbon\Carbon::parse($this->mod_datum)->format('Y.m.d H:i:s');
	}
	public function moduser()
	{
		return $this->belongsTo('App\User');
	}
}
