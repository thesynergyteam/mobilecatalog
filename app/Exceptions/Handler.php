<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use App\Mail\ExceptionOccured;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\ErrorHandler\ErrorRenderer\HtmlErrorRenderer;
use Symfony\Component\ErrorHandler\Exception\FlattenException;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Throwable
     */
    public function report(Throwable $exception)
    {
		if ($this->shouldReport($exception)) {
			$this->sendEmail($exception); // sends an email
		}
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        return parent::render($request, $exception);
    }

	public function sendEmail(Throwable $exception)
	{
		try {
			$e = FlattenException::create($exception);
			$handler = new HtmlErrorRenderer(true); // boolean, true raises debug flag...
            $css = $handler->getStylesheet();
            $content = '<h3>User: '.(\Auth::user()?\Auth::user()->username:'unauthenticated').'</h3>'.$handler->getBody($e);
		/*	Mail::send('emails.exception', compact('css','content'), function ($message) {
                $message
                    ->to(config('custom.mail.send_to_exception'))
                    ->subject('MobileCatalog'.(\App::environment('dev')?'-dev':'').' Exception')
                ;
            });*/
			Mail::to(config('custom.mail.send_to_exception'))->send(new ExceptionOccured($content,$css));
		} catch (Exception $ex) {
			dd($ex);
		}
	}
}
