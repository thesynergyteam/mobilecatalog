<?php
/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2022(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App;

class Szamhordozas extends BaseModel
{
	protected $table = 'mobile__szamhordozas';
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = [
		'sim_id', 'felhasznalo_id', 'datum', 'moduser_id', 'statusz','megjegyzes'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //'email_verified_at' => 'datetime',
    ];
	public function sim()
	{
		return $this->belongsTo('App\Sim');
	}
	public function fajlok()
	{
		return $this->hasMany('App\SzamhordozasFajl');
	}

	public function felhasznalo()
	{
		return $this->belongsTo('App\Felhasznalo');
	}
	public function moduser()
	{
		return $this->belongsTo('App\User');
	}
	public function getDatumAttribute($value) {
		return \Carbon\Carbon::parse($value)->format('Y.m.d');
	}
}
