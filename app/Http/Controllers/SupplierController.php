<?php
/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Beszallito;
use DB;

class SupplierController extends Controller
{
	public function __construct() {
		$this->middleware(['auth']);
		$this->middleware('permission:Beszállítók szerkesztése', ['except' => ['index','edit']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$suppliers = Beszallito::get()->sortBy('partner');
		return view('suppliers.index')->with('suppliers', $suppliers);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('suppliers.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'partner'=>'required|min:3',
			'cim'=>'required|min:7',
			//'email'=>'email'
			]);
		Beszallito::create(['partner'=>$request['partner'],'cim' =>$request['cim'], 'kontakt' => (!empty($request['kontakt'])?$request['kontakt']:null), 'email' => (!empty($request['email'])?$request['email']:null), 'telefon' => (!empty($request['telefon'])?$request['telefon']:null), 'megjegyzes' => (!empty($request['megjegyzes'])?$request['megjegyzes']:null), 'aktiv' => isset($request['aktiv']), 'moduser_id' => $request->user()->id]);

		return redirect()->route('suppliers.index')
			->with('flash_message', 'Eszköz hozzáadva!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id = car id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$supplier = Beszallito::where('id',$id)->firstOrFail();
		return view('suppliers.edit')->with('supplier', $supplier);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'partner'=>'required|min:3',
			'cim'=>'required|min:7'
			]);
		Beszallito::where('id',$id)->update(['partner'=>$request['partner'],'cim' =>$request['cim'], 'kontakt' => (!empty($request['kontakt'])?$request['kontakt']:null), 'email' => (!empty($request['email'])?$request['email']:null), 'telefon' => (!empty($request['telefon'])?$request['telefon']:null), 'megjegyzes' => (!empty($request['megjegyzes'])?$request['megjegyzes']:null), 'aktiv' => isset($request['aktiv']), 'moduser_id' => $request->user()->id]);

		return redirect()->route('suppliers.index')
			->with('flash_message', 'Eszköz frissítve!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{

	}

	public function history($id)
	{
		$history = DB::table('mobile__beszallito_history')->leftJoin('mobile__users','mobile__users.id','=','mobile__beszallito_history.moduser_id')
				->select(['mobile__beszallito_history.*','mobile__users.name'])
				->where('mobile__beszallito_history.id',$id)->orderBy('mobile__beszallito_history.mod_datum','desc')->get();
		return view('suppliers.history')->with('history', $history);
	}
}
