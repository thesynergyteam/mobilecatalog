<?php
/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Accessory;
use Response;

class AccessoryController extends Controller
{
	public function __construct() {
		//$this->middleware(['auth','permission:View devices']);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$accessories = Accessory::paginate(200);
		return view('accessories.index')->with('accessories', $accessories);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('accessories.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'nev'=>'required',
			'beszallito'=>'required',
			'ertek'=>'required',
			'datum'=>'required',
			'db' => 'required|numeric|min:1'
			]);
		for ($x=1;$x<=$request['db'];$x++){
			Accessory::create(['nev'=>$request['nev'], 'leltariszam' => $request['leltariszam'], 'megjegyzes' => $request['megjegyzes'], 'ertek' => $request['ertek'], 'datum' => $request['datum'], 'statusz_id' => $request['statusz'], 'beszallito_id' => $request['beszallito']]);
		}
		return redirect()->route('accessories.index')
			->with('flash_message', 'Kellék hozzáadva!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id = car id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$accessory = Accessory::where('id',$id)->firstOrFail();
		return view('accessories.edit')->with('accessory', $accessory);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'nev'=>'required'
			]);
		Accessory::where('id',$id)->update(['nev' => $request['nev'], 'leltariszam' => $request['leltariszam'], 'megjegyzes' => $request['megjegyzes'], 'statusz_id' => $request['statusz']]);

		return redirect()->route('accessories.index')
			->with('flash_message', 'Kellék frissítve!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{

	}
}
