<?php
/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Mobileszkoz;
use Response;

class DeviceController extends Controller
{
	public function __construct() {
		$this->middleware(['auth']);
		$this->middleware('permission:Mobil eszközök szerkesztése', ['except' => ['index','edit','fetch_data']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$devices = Mobileszkoz::with(['tipus','statusz', 'felhasznalo'])->orderBy('tipus_id')->paginate(20);
		return view('devices.index')->with('devices', $devices);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('devices.create');
	}

	public function masscreate()
	{
		return view('devices.masscreate');
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if ($request['mode']=='mass'){
			$this->validate($request, [
				'beszallito'=>'required',
				'szamlaszam'=>'required',
				'datum'=>'required',
				'imei'=>'required'
				]);
			$index =0;
			foreach($request['imei'] as $key => $value){
				if (Mobileszkoz::where('imei',$value)->count()>0) {
					$index++;
					continue;
				}
				Mobileszkoz::create(['tipus_id'=>$request['tipus'][$key],'IMEI' =>$request['imei'][$key], 'IMEI2' => (!empty($request['imei2'][$key])?$request['imei2'][$key]:null), 'szamlaszam' => $request['szamlaszam'], 'ertek' => $request['ertek'][$key], 'beszallito_id' => $request['beszallito'], 'datum' => $request['datum'],'statusz_id'=>5,'leltariszam' => (!empty($request['leltari'][$key])?$request['leltari'][$key]:null)]);
			}
			return redirect()->route('devices.index')
				->with('flash_message', 'Új eszközök bevételezve!'.($index>0?' Kihagyott darab: '.$index:''));
		}
		$this->validate($request, [
			'tipus'=>'required|gt:0',
			'imei'=>'required|unique:App\Mobileszkoz,imei|unique:App\Mobileszkoz,imei2',
			'imei2'=>'nullable|digits:15|unique:App\Mobileszkoz,imei|unique:App\Mobileszkoz,imei2',
			'szamlaszam'=>'required',
			'ertek'=>'required',
			'datum'=>'required',
			'beszallito'=>'required|gt:0'
			]);
		Mobileszkoz::create(['tipus_id'=>$request['tipus'],'IMEI' =>$request['imei'], 'IMEI2' => (!empty($request['imei2'])?$request['imei2']:null), 'szamlaszam' => $request['szamlaszam'], 'ertek' => $request['ertek'], 'beszallito_id' => $request['beszallito'], 'datum' => $request['datum'], 'gyartasiszam' => (!empty($request['gyartasiszam'])?$request['gyartasiszam']:null),'statusz_id'=>$request['statusz'], 'leltariszam' => (!empty($request['leltariszam'])?$request['leltariszam']:null), 'beruhazasi_id' => $request['beruhazasi_id']]);

		return redirect()->route('devices.index')
			->with('flash_message', 'Eszköz hozzáadva!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id = car id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$device = Mobileszkoz::where('id',$id)->firstOrFail();
		return view('devices.edit')->with('device', $device);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'imei2'=>'nullable|digits:15|unique:App\Mobileszkoz,imei|unique:App\Mobileszkoz,imei2,'.$id,
			]);
		Mobileszkoz::where('id',$id)->update(['statusz_id'=>$request['statusz'], 'IMEI2' => (!empty($request['imei2'])?$request['imei2']:null), 'gyartasiszam' => (!empty($request['gyartasiszam'])?$request['gyartasiszam']:null), 'leltariszam' => (!empty($request['leltariszam'])?$request['leltariszam']:null)]);

		return redirect()->route('devices.index')
			->with('flash_message', 'Eszköz frissítve!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{

	}
	public function csvimport(Request $request)
	{
		$index = 0;
		$imeiArr = [];
		$usertsz = null;
		foreach($request['data'] as $data){
		/*	tipus, imei, leltari, ar, datum, beszallito, szamlaszam, usertsz */
			$gyar = explode(" ", $data['tipus'])[0];
			$tip = trim(substr($data['tipus'], strlen($gyar)));
			$tipus = \App\Tipus::firstOrCreate(['gyarto' => $gyar, 'tipus' => $tip]);
			$beszallito = \App\Beszallito::firstOrCreate(['partner' => $data['beszallito']], ['cim' => 'még nincs kitöltve', 'aktiv' => 1, 'moduser_id' => $request->user()->id]);
			$mob = Mobileszkoz::where('IMEI',$data['imei'])->orWhere('IMEI2',$data['imei'])->first();
			if (isset($mob) || !is_numeric($data['ar'])){
				$imeiArr[] = $data['imei'];
				$index++;
			} else {
				if ($data['usertsz']){
					$usertsz = hruser((strlen($data['usertsz'])<6?"0".$data['usertsz']:$data['usertsz']));
					if ($usertsz) {
						$userid = \App\Felhasznalo::firstOrCreate(['tsz' => $data['usertsz']], ['nev' => $usertsz->dolgozo_nev])->id;
					}
				}
				$eszkid = Mobileszkoz::create(['tipus_id'=>$tipus->id,'IMEI' =>$data['imei'], 'IMEI2' => null, 'szamlaszam' => ($data['szamlaszam']?:''), 'ertek' => $data['ar'], 'beszallito_id' => $beszallito->id, 'datum' => $data['datum'],'statusz_id'=>($usertsz?1:5),'felhasznalo_id'=>($usertsz?$userid:null),'leltariszam' => (!empty($data['leltari'])?$data['leltari']:null), 'beruhazasi_id' => $data['beruhid']])->id;
				if ($usertsz) {
					$konf = \App\Konfiguracio::create(['felhasznalo_id' =>$userid, 'kategoria_id' =>6, 'statusz' =>'kiadás', 'megjegyzes' =>'CSV import alapján', 'moduser_id' => $request->user()->id, 'datum' => $data['datum']]);
					$konf->eszkoz()->attach($eszkid);
				}
			}
		}
		return Response::json(array(
			'result' => 'success',
			'kihagyott' => $index,
			'imei' => implode(", ", $imeiArr),
			'osszes' => count($request['data']),
			'status_code' => 200
		));
	}
	/**
	 * Folyamatos keresés gépelés közben
	 *
	 * @param Request $request
	 */
	function fetch_data(Request $request)
    {
		if($request->ajax() && !empty($request->query)){
			$column = $request->get('column');
			$query = $request->get('query');
			$query = str_replace(" ", "%", $query);
			if ($column[0]=='_'){
				$colArr = explode("_", $column);
				$devices= Mobileszkoz::when($query=="", function($q){
							$q->select('*');
						}, function($q1) use ($colArr,$query) {
							$q1->whereHas($colArr[1], function($q) use ($colArr,$query){
								return $q->where($colArr[2], 'like', "$query%");
							});
						})
				->with(['tipus','statusz', 'felhasznalo'])->orderBy('tipus_id')
				->paginate(20);
			} else {
			$devices= Mobileszkoz::where($column, 'like', "$query%")
				->with(['tipus','statusz', 'felhasznalo'])->orderBy('tipus_id')
				->paginate(20);
			}
			return view('devices.pagination_data', compact('devices'))->render();
		}
    }
}
