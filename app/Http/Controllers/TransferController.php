<?php
/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2022(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Szamhordozas;

class TransferController extends Controller
{
	public function __construct() {
		$this->middleware(['auth']);
		$this->middleware('permission:Számhordozás szerkesztése', ['except' => ['index','edit']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$szamok = Szamhordozas::with(['felhasznalo','sim'])->orderBy('datum','desc')->paginate(20);
		return view('transfers.index')->with('szamhordozasok', $szamok);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('transfers.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'usernev'=>'required',
			'tsz' => 'required|digits:6',
			'statusz' => 'required',
			'szuletesi_ido' => 'required',
			'anyjaneve' => 'required',
			'szemigszam' => 'required',
			'koltseghely' => 'required',
			'sim_id' => 'required',
			'lakcim'=>'required'
			]);
		$felhasznalo = \App\Felhasznalo::firstOrCreate(['tsz' => $request['tsz'],['nev' => $request['usernev']]]);
		$felhasznalo->fill($request->all());
		$felhasznalo->save();
		$szam = Szamhordozas::create(array_merge($request->all(), ['felhasznalo_id' => $felhasznalo->id, 'moduser_id' => $request->user()->id]));
		\App\Sim::find($request['sim_id'])->update(['simstatusz_id' => ($request['statusz']=='Elhordozás folyamatban'?6:5)]);

		if (isset($request['saveandedit'])){
			return view('transfers.edit')->with('szam', $szam->fresh())->with('flash_message', 'Számhordozás létrehozva!');
		}
		return redirect()->route('transfers.index')
			->with('flash_message', 'Számhordozás létrehozva!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id = car id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$szam = Szamhordozas::where('id',$id)->with('fajlok.moduser','sim')->firstOrFail();
		return view('transfers.edit')->with('szam', $szam);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'szuletesi_ido' => 'required',
			'anyjaneve' => 'required',
			'szemigszam' => 'required',
			'koltseghely' => 'required',
			'lakcim'=>'required'
			]);
		Szamhordozas::where('id',$id)->update($request->only('megjegyzes'));
		\App\Felhasznalo::where('id', $request['felhasznalo_id'])->update($request->only('szuletesi_ido', 'anyjaneve','szemigszam','koltseghely','lakcim'));
		if ($request->hasFile('dokumentum')){
			$date = date('Y.m');
			$path=$request->file('dokumentum')->store("uploads/szamhordozasok/$date");
			\App\SzamhordozasFajl::create(['szamhordozas_id' => $id, 'utvonal' => $path, 'fajlnev' =>$request->file('dokumentum')->getClientOriginalName(), 'moduser_id' => $request->user()->id]);
		}
		return redirect()->route('transfers.index')
			->with('flash_message', 'Számhordozás frissítve!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{

	}
	public function downloadTransfer(int $id)
	{
		$transfer = Szamhordozas::findOrFail($id);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$document = $phpWord->loadTemplate('templates/szam_elhordozas_temp.docx');

		$document->setValue('sorszam', sprintf('%06d', $transfer->id));
		$document->setValue('datum', $transfer->datum);
		$document->setValue('moduser', $transfer->moduser->name);
		$document->setValue('nev', $transfer->felhasznalo->nev);
		$document->setValue('tsz', $transfer->felhasznalo->tsz);
		$document->setValue('koltseghely', $transfer->felhasznalo->koltseghely);
		$document->setValue('anyjaneve', $transfer->felhasznalo->anyjaneve);
		$document->setValue('lakcim', $transfer->felhasznalo->lakcim);
		$document->setValue('szuletesi_ido', $transfer->felhasznalo->szuletesi_ido);
		$document->setValue('szemigszam', $transfer->felhasznalo->szemigszam);
		$document->setValue('telefonszam', $transfer->sim->telefonszam);

		if (!file_exists(public_path('tmp'))) {
			mkdir( public_path('tmp'));
		}
		$name = '/tmp/konfiguracio_MOB-SZ'.sprintf('%06d', $transfer->id);
		$export_file = public_path($name.'.docx');
		$document->saveAs($export_file);
		shell_exec("libreoffice7.0 --headless --convert-to pdf $export_file --outdir ".public_path('tmp')." && rm '$export_file'");
		return response()->file(public_path($name.'.pdf'))->deleteFileAfterSend(true);
	}

	public function setReadyTransfer(int $id)
	{
		Szamhordozas::findOrFail($id)->update(['statusz'=>'Elhordozva']);
		return redirect()->route('transfers.index')
			->with('flash_message', 'Számhordozás frissítve!');
	}

}
