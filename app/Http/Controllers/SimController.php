<?php
/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Sim;
use Response;

class SimController extends Controller
{
	public function __construct() {
		$this->middleware(['auth']);
		$this->middleware('permission:SIM kártyák szerkesztése', ['except' => ['index','edit','update','fetch_data']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$sims = Sim::with(['beszallito','felhasznalo'])->orderBy('telefonszam')->paginate(20);
		return view('sims.index')->with('sims', $sims);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('sims.create');
	}

	public function masscreate()
	{
		return view('sims.masscreate');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if ($request['mode']=='mass'){
			$this->validate($request, [
				'beszallito'=>'required',
				'megrendelesszam'=>'required|max:50',
				'rendelesszam'=>'required|max:50',
				'szallitolevelszam'=>'required|max:50',
				'datum'=>'required',
				'icc' => 'required'
				]);
			foreach($request['icc'] as $key => $value){
				Sim::where('telefonszam', $request['telefonszam'][$key])->update(['felhasznalo_id' => null, 'simstatusz_id' => 4]);//TODO: konfigurációt lezárni, ami erre a sim-re vonatkozik!
				Sim::create(['icc'=>$value,'megrendelesszam' =>$request['megrendelesszam'],'szallitolevelszam' =>$request['szallitolevelszam'],'rendelesszam' =>$request['rendelesszam'], 'telefonszam' => $request['telefonszam'][$key], 'tipus' => $request['tipus'][$key], 'netcsomag_id' => $request['netcsomag'][$key], 'beszallito_id' => $request['beszallito'], 'datum' => $request['datum'],'beruhazasi_azon' =>$request['beruhazon']]);
			}
			return redirect()->route('sims.index')
				->with('flash_message', 'Új sim kártyák bevételezve!');
		}
		$this->validate($request, [
			'icc'=>'required|gt:17|unique:App\Sim',
			'beszallito'=>'required',
			'megrendelesszam'=>'required|max:50',
			'rendelesszam'=>'required|max:50',
			'szallitolevelszam'=>'required|max:50',
			'telefonszam'=>'required|digits:11',
			'tipus'=>'required',
			'datum'=>'required',
			'pin1'=>'nullable|digits:4',
			'pin2'=>'nullable|digits:4',
			'puk1'=>'nullable|digits:8',
			'puk2'=>'nullable|digits:8'
			]);
		Sim::where('telefonszam', $request['telefonszam'])->update(['felhasznalo_id' => null, 'simstatusz_id' => 4]); //TODO: konfigurációt lezárni, ami erre a sim-re vonatkozik!
		Sim::create(['icc'=>$request['icc'],'megrendelesszam' =>$request['megrendelesszam'],'szallitolevelszam' =>$request['szallitolevelszam'],'rendelesszam' =>$request['rendelesszam'], 'telefonszam' => $request['telefonszam'], 'tipus' => $request['tipus'], 'beszallito_id' => $request['beszallito'], 'datum' => $request['datum'],'pin1'=>$request['pin1'],'pin2'=>$request['pin2'],'puk1'=>$request['puk1'],'puk2'=>$request['puk2'], 'netcsomag_id' => $request['netcsomag_id'],'beruhazasi_azon' =>$request['beruhazon']]);
		if (isset($request['saveandnext'])){
			return back()->withInput($request->except(['icc','telefonszam','pin1','pin2','puk1','puk2']))->with('flash_message', $request['telefonszam'].' sim létrehozva!');
		}
		return redirect()->route('sims.index')
			->with('flash_message', 'Sim kártya hozzáadva!');
	}

	public function csvimport(Request $request)
	{
		$this->validate($request, [
			'beszallito'=>'required',
			'datum'=>'required',
			'data'=>'required|array'
			]);
		$index = 0;
		$arr = [];
		foreach($request['data'] as $data){
			$arr[] = $data['icc'];
			if (Sim::where('icc', $data['icc'])->count() > 0){
				$index++;
				Sim::where('icc', $data['icc'])->update(['telefonszam' => $data['telefonszam'], 'pin1'=>$data['pin1'],'pin2'=>$data['pin2'],'puk1'=>$data['puk1'],'puk2'=>$data['puk2'], 'netcsomag_id' => $data['net']]);
				continue;
			} else {
			//Sim::where('telefonszam','like', $data['telefonszam'])->update(['felhasznalo_id' => null, 'simstatusz_id' => 4]);//TODO: konfigurációt lezárni, ami erre a sim-re vonatkozik!
			Sim::create(['icc'=>$data['icc'], 'megrendelesszam' =>'csv importból','szallitolevelszam' =>'csv importból','rendelesszam' =>'csv importból', 'telefonszam' => $data['telefonszam'], 'tipus' => $data['tipus'],'pin1'=>$data['pin1'],'pin2'=>$data['pin2'],'puk1'=>$data['puk1'],'puk2'=>$data['puk2'], 'netcsomag_id' => $data['net'], 'beszallito_id' => $request['beszallito'], 'datum' => $request['datum']]);
			}
		}
		Sim::whereNotIn('icc', $arr)->where('simstatusz_id',1)->update(['simstatusz_id' => 3]);
		return Response::json(array(
			'result' => 'success',
			'kihagyott' => $index,
			'status_code' => 200
		));
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id = car id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$sim = Sim::where('id',$id)->firstOrFail();
		return view('sims.edit')->with('sim', $sim);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'telefonszam'=>'required|digits:11',
			'megrendelesszam'=>'required|max:50',
			'rendelesszam'=>'required|max:50',
			'szallitolevelszam'=>'required|max:50',
			'datum'=>'required',
			'tipus'=>'required',
			'pin1'=>'nullable|digits:4',
			'pin2'=>'nullable|digits:4',
			'puk1'=>'nullable|digits:8',
			'puk2'=>'nullable|digits:8'
			]);
		Sim::where('id',$id)->update(['beruhazasi_azon' =>$request['beruhazon'],'szallitolevelszam' => $request['szallitolevelszam'],'rendelesszam' => $request['rendelesszam'],'megrendelesszam' => $request['megrendelesszam'],'telefonszam' => $request['telefonszam'], 'tipus' => $request['tipus'], 'datum' => $request['datum'], 'simstatusz_id' => $request['simstatusz_id'],'pin1'=>$request['pin1'],'pin2'=>$request['pin2'],'puk1'=>$request['puk1'],'puk2'=>$request['puk2'], 'netcsomag_id' => $request['netcsomag_id']]);

		return redirect()->route('sims.index')
			->with('flash_message', 'Sim kártya frissítve!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{

	}

	/**
	 * Folyamatos keresés gépelés közben
	 *
	 * @param Request $request
	 */
	function fetch_data(Request $request)
    {
		if($request->ajax() && !empty($request->query)){
			$column = $request->get('column');
			$query = $request->get('query');
			$query = str_replace(" ", "%", $query);
			//dd(explode("_", $column));
			if ($column[0]=='_'){
				$colArr = explode("_", $column);
				$sims= Sim::when($query=="", function($q){
							$q->select('*');
						}, function($q1) use ($colArr,$query) {
							$q1->whereHas($colArr[1], function($q) use ($colArr,$query){
								return $q->where($colArr[2], 'like', "$query%");
							});
						})
				->with(['beszallito','felhasznalo'])->orderBy('telefonszam')
				->paginate(20);
			} else {
			$sims= Sim::where($column, 'like', "$query%")
				->with(['beszallito','felhasznalo'])->orderBy('telefonszam')
				->paginate(20);
			}
			return view('sims.pagination_data', compact('sims'))->render();
		}
    }
}
