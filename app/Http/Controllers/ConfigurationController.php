<?php
/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Konfiguracio;
use App\Mobileszkoz;
use App\Accessory;
use App\Sim;
use DB;
use Response;
use \Carbon\Carbon;

class ConfigurationController extends Controller
{
	public function __construct() {
		//$this->middleware(['auth','permission:View devices']);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$configurations = Konfiguracio::with(['eszkoz.tipus','sim', 'felhasznalo'])->orderBy('datum')->paginate(20);
		return view('configurations.index')->with('configurations', $configurations);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('configurations.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'eszkoz_id'=>'required_without_all:sim_id,kieg_eszkoz_id',
			'sim_id'=>'required_without_all:eszkoz_id,kieg_eszkoz_id',
			'kieg_eszkoz_id'=>'required_without_all:eszkoz_id,sim_id',
			'usernev'=>'required',
			'tsz'=>'required|digits:6',
			'kategoria'=>'required',
			'statusz'=>'required'
			]);
		$eszkid = $request['eszkoz_id']?:[];
		$simid = $request['sim_id']?:[];
		$kiegid = $request['kieg_eszkoz_id']?:[];
		$date = Carbon::createFromFormat('Y.m.d', substr($request['datum'],0,10));
		$felhasznalo = \App\Felhasznalo::firstOrCreate(['tsz' => $request['tsz'],'nev' =>$request['usernev']])->id;
		if ($request['statusz']=='kiadás'){
			Mobileszkoz::whereIn('id',$eszkid)->update(['felhasznalo_id' =>$felhasznalo, 'statusz_id' => 1]);
			Sim::whereIn('id',$simid)->update(['felhasznalo_id' =>$felhasznalo, 'simstatusz_id' => 2]);
			Accessory::whereIn('id', $kiegid)->update(['felhasznalo_id' =>$felhasznalo, 'statusz_id' => 1]);
		} else {
			Mobileszkoz::whereIn('id',$eszkid)->update(['felhasznalo_id' =>null, 'statusz_id' => 6]);
			Sim::whereIn('id',$simid)->update(['felhasznalo_id' =>null, 'simstatusz_id' => 1]);
			Accessory::whereIn('id', $kiegid)->update(['felhasznalo_id' =>null, 'statusz_id' => 6]);
		}
		$konf = Konfiguracio::create(['felhasznalo_id' =>$felhasznalo, 'kategoria_id' =>$request['kategoria'], 'statusz' =>$request['statusz'], 'megjegyzes' => (!empty($request['megjegyzes'])?$request['megjegyzes']:null), 'moduser_id' => $request->user()->id]);
		if (!$date->isToday()){
			$konf->datum = $request['datum'];
			$konf->save();
		}
		$konf->eszkoz()->attach($eszkid);
		$konf->sim()->attach($simid);
		$konf->kiegeszito()->attach($kiegid);
		if (isset($request['saveandedit'])){
			return view('configurations.edit')->with('configuration', $konf->fresh())->with('flash_message', ($request['statusz']=='kiadás'?'Kiadás':'Visszavételi').' konfiguráció létrehozva!');
		}
		return redirect()->route('configurations.index')->with('flash_message', ($request['statusz']=='kiadás'?'Kiadás':'Visszavételi').' konfiguráció létrehozva!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id = car id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$configuration = Konfiguracio::where('id',$id)->with('fajlok.moduser','eszkoz.tipus','sim','kiegeszito')->firstOrFail();
		return view('configurations.edit')->with('configuration', $configuration);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$konf = Konfiguracio::findOrFail($id);
		$date = Carbon::createFromFormat('Y.m.d', substr($request['datum'],0,10));
		if (isset($request['newreturnconfig'])){
			$request->merge(array('megjegyzes' => 'Visszavételi konfiguráció a '.$id.'.sz. kiadás alapján.', 'statusz' => 'visszavét'));
			return redirect()->route('configurations.create')->withInput();
		}
		$konf->megjegyzes = (!empty($request['megjegyzes'])?$request['megjegyzes']:null);
		$konf->moduser_id = $request->user()->id;
		$konf->kategoria_id = $request['kategoria'];
		$felhasznalo = $request['felhasznalo_id'];
		$eszkid = $request['eszkoz_id']?:[];
		$simid = $request['sim_id']?:[];
		$kiegid = $request['kieg_eszkoz_id']?:[];
		if (!$date->isToday()){
			$konf->datum = $request['datum'];
		}
		$konf->eszkoz()->whereNotIn('id',$eszkid)->update(['felhasznalo_id' =>null, 'statusz_id' => 6]);
		$konf->sim()->whereNotIn('id',$simid)->update(['felhasznalo_id' =>null, 'simstatusz_id' => 1]);
		$konf->kiegeszito()->whereNotIn('id',$kiegid)->update(['felhasznalo_id' =>null, 'statusz_id' => 6]);
		if ($request['statusz']=='kiadás'){
			Mobileszkoz::whereIn('id',$eszkid)->update(['felhasznalo_id' =>$felhasznalo, 'statusz_id' => 1]);
			Sim::whereIn('id',$simid)->update(['felhasznalo_id' =>$felhasznalo, 'simstatusz_id' => 2]);
			Accessory::whereIn('id', $kiegid)->update(['felhasznalo_id' =>$felhasznalo, 'statusz_id' => 1]);
		} else {
			Mobileszkoz::whereIn('id',$eszkid)->update(['felhasznalo_id' =>null, 'statusz_id' => 6]);
			Sim::whereIn('id',$simid)->update(['felhasznalo_id' =>null, 'simstatusz_id' => 1]);
			Accessory::whereIn('id', $kiegid)->update(['felhasznalo_id' =>null, 'statusz_id' => 6]);
		}
		$konf->eszkoz()->sync($eszkid);
		$konf->sim()->sync($simid);
		$konf->kiegeszito()->sync($kiegid);

		if ($request->hasFile('dokumentum')){
			$date = date('Y.m');
			$path=$request->file('dokumentum')->store("uploads/konfigok/$date");
			\App\Fajl::create(['konfiguracio_id' => $id, 'utvonal' => $path, 'fajlnev' =>$request->file('dokumentum')->getClientOriginalName(), 'moduser_id' => $request->user()->id]);
		}
		$konf->save();
		return redirect()->route('configurations.index')->with('flash_message', 'Konfigurácio frissítve!');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{

	}
	public function queryTsz($tsz)
	{
		$firstuser = hruser($tsz);
		$user = \App\Felhasznalo::where('tsz',$tsz)->first();
		//$user= DB::connection('oracle')->table('APPS.V_DOLGOZO3')->join('APPS.V_MUNKAKOR','APPS.V_DOLGOZO3.V_MUNKAKOR_ID','=','APPS.V_MUNKAKOR.ID')->select(['APPS.V_MUNKAKOR.NEV', 'DOLGOZO_NEV'])->where('APPS.V_DOLGOZO3.ID',$tsz)->first();
		return Response::json(array(
			'error' => false,
			'user' => $firstuser,
			'felhasznalo' => $user,
			'status_code' => 200
		));
	}
	public function queryIMEI(Request $request,$imei)
	{
		$devid = $request->eszkoz_id;
		$eszkoz = Mobileszkoz::where('IMEI','like',$imei)
			->when(isset($request->eszkoz_id), function($q) use ($devid){
				return $q->whereNotIn('id',$devid);
			})->with(['felhasznalo','statusz','tipus'])->first();
		return Response::json(array(
			'error' => false,
			'eszkoz' => $eszkoz,
			'status_code' => 200
		));
	}
	public function querySim(Request $request,$icc_tel, $id)
	{
		$ii =  $request->ignore_inactive?[1,2,5]:[1,2,3,4,5,6];
		$simid = $request->sim_id;
		$sim = Sim::when($icc_tel=='icc', function($q) use($id,$ii){
				$q->where('icc','like', $id)
				->whereIn('simstatusz_id',$ii);
			},function($q) use ($id,$ii){
				$q->where('telefonszam','like', $id)
				->whereIn('simstatusz_id',$ii);
			})
			->when(isset($request->sim_id), function($q) use ($simid){
				return $q->whereNotIn('id',$simid);
			})
			->with(['felhasznalo'])->first();
		return Response::json(array(
			'error' => false,
			'sim' => $sim,
			'status_code' => 200
		));
	}
	public function queryAccessory(Request $request)
	{
		$accessories = null;
		if($request->ajax() && !empty($request->kieg_eszkoz_nev)){
			$query = $request->kieg_eszkoz_nev;
			$id = $request->kieg_eszkoz_id;
			$accessories = Accessory::where(function($q) use ($query){
					$q->where('nev', 'like', "%$query%");
					$q->orWhere('megjegyzes', 'like', "%$query%");
					$q->orWhere('leltariszam', 'like', "%$query%");
				})
				->when(isset($request->kieg_eszkoz_id), function($q) use ($id){
					return $q->whereNotIn('id',$id);
				})
				->whereNull('felhasznalo_id')
				->get();
		}
		return Response::json(array(
			'error' => false,
			'accessories' => $accessories,
			'status_code' => 200
		));
	}

	public function history($id)
	{
		$history = \App\KonfiguracioHistory::whereKonf($id)->with('kategoria')->orderBy('mod_datum','desc')->get();
		return view('configurations.history')->with('history', $history);
	}

	public function query(Request $request)
	{
		$results = null;
		$user = false;
		$devices = null;
		$sims = null;
		$accessories = null;
		$szam = null;
		if ($request->tsz){
			$tsz = '_'.substr($request->tsz,1);
			$results = Konfiguracio::whereHas('felhasznalo',function($q) use ($tsz){
				$q->where('tsz','like', $tsz);
			})
			->with(['eszkoz.tipus','sim'])
			->orderBy('datum','desc')->paginate(200);
			$devices = Mobileszkoz::whereHas('felhasznalo',function($q) use ($tsz){
				$q->where('tsz','like', $tsz);
			})
			->with(['tipus'])->get();
			$sims = Sim::whereHas('felhasznalo',function($q) use ($tsz){
				$q->where('tsz','like', $tsz);
			})->get();
			$accessories = Accessory::whereHas('felhasznalo',function($q) use ($tsz){
				$q->where('tsz','like', $tsz);
			})->get();
			$szam = \App\Szamhordozas::whereHas('felhasznalo',function($q) use ($tsz){
				$q->where('tsz','like', $tsz);
			})->orderBy('datum','desc')->get();
			$user = true;
		} elseif ($request->imei){
				$imei = $request->imei;
				$results = Konfiguracio::whereHas('eszkoz',function($q) use ($imei){
					$q->where('IMEI',$imei);
				})
				->with(['felhasznalo','sim','eszkoz'])
				->orderBy('datum','desc')->paginate(200);
		} elseif ($request->tel){
				$tel = $request->tel;
				$results = Konfiguracio::whereHas('sim',function($q) use ($tel){
					$q->where('telefonszam',$tel);
				})
				->with(['felhasznalo','sim','eszkoz'])
				->orderBy('datum','desc')->paginate(200);
				$szam = \App\Szamhordozas::whereHas('sim',function($q) use ($tel){
					$q->where('telefonszam', $tel);
				})->orderBy('datum','desc')->get();
		}
		return view('configurations.query')->with(['results' => $results, 'user' => $user, 'devices' => $devices, 'sims' => $sims, 'accessories' => $accessories, 'szam' => $szam]);

	}

	public function userquery(Request $request)
	{
		$results = null;
		if ($request->tsz){
			$tsz = '_'.substr($request->tsz,1);
			$results = Konfiguracio::whereHas('felhasznalo',function($q) use ($tsz){
				$q->where('tsz','like', $tsz);
			})
			->with(['eszkoz.tipus','sim'])
			->orderBy('statusz')->orderBy('datum')->paginate(200);
		}
		return view('configurations.userquery')->with('results', $results);
	}

	public function mobilequery(Request $request)
	{
		$results = null;
		if ($request->imei){
			$imei = $request->imei;
			$results = Konfiguracio::whereHas('eszkoz',function($q) use ($imei){
				$q->where('IMEI',$imei);
			})
			->with(['felhasznalo','sim','visszavetel'])
			->orderBy('datum','desc')->paginate(200);
		}
		return view('configurations.mobilequery')->with('results', $results);
	}

	/**
	 * Folyamatos keresés gépelés közben
	 *
	 * @param Request $request
	 */
	public function fetch_data(Request $request)
    {
		if($request->ajax() && !empty($request->query)){
			$column = $request->get('column');
			$query = $request->get('query');
			$query = str_replace(" ", "%", $query);
			if ($column[0]=='_'){
				$colArr = explode("_", $column);
				$configurations= Konfiguracio::when($query=="", function($q){
							$q->select('*');
						}, function($q1) use ($colArr,$query) {
							$q1->whereHas($colArr[1], function($q) use ($colArr,$query){
								return $q->where($colArr[2], 'like', "$query%");
							});
						})
				->with(['eszkoz.tipus','sim', 'felhasznalo'])->orderBy('datum')
				->paginate(20);
			} else {
			$configurations= Konfiguracio::where($column, 'like', "$query%")
				->with(['eszkoz.tipus','sim', 'felhasznalo'])->orderBy('datum')
				->paginate(20);
			}
			return view('configurations.pagination_data', compact('configurations'))->render();
		}
    }

	/**
	 * A HR telefonköny alapján sim kártyák összerendelés a felhasználókkal
	 *
	 * @param Request $request
	 */
	public function updateUsers(Request $request) {
		$sims = Sim::whereIn('simstatusz_id',[1])->get();
		$users= DB::connection('oracle')->table('APPS.V_DOLGOZO3')->select(['ID', 'DOLGOZO_NEV', 'MOBIL_TAROLT'])->whereNotNull('MOBIL_TAROLT')
				->get();
		$telArr = [];
		foreach ($users as $user) {
			foreach (explode(",",$user->mobil_tarolt) as $tel){
				if (strlen($tel) > 9) {$tel = substr($tel, 2);}
				if (strlen($tel) == 9) {
					$telArr["36".$tel] = ["tsz" => $user->id, 'nev' => $user->dolgozo_nev];
				}
			}
		}
		$i=0; $status = [1 => ['db' => 0, 'id' => []], 2 => ['db' => 0, 'id' => []], 3 => ['db' => 0, 'id' => []], 4 => ['db' => 0, 'id' => []]];
		foreach ($sims as $sim) {
			if (array_key_exists($sim->telefonszam,$telArr)){
				$i++;
				$status[$sim->simstatusz_id]['db']++;
				$status[$sim->simstatusz_id]['id'][]=$sim->id;

				$tel = $telArr[$sim->telefonszam];
				$felhasznalo = \App\Felhasznalo::firstOrCreate(['tsz' => $tel['tsz'],'nev' =>$tel['nev']])->id;
				//eszköz, sim kiadása
				$sim->felhasznalo_id = $felhasznalo;
				$sim->simstatusz_id = 2;
				$sim->save();
				$konf1 = Konfiguracio::where(['sim_id' =>$sim->id,'statusz' => 'kiadás'])->first();
				if(!$konf1){
					Konfiguracio::create(['sim_id' =>$sim->id, 'felhasznalo_id' =>$felhasznalo, 'kategoria_id' => 6, 'statusz' =>'kiadás', 'megjegyzes' => 'hr telkönyv importból', 'moduser_id' => $request->user()->id]);
				}
			}
		}
		return Response::json(array(
			'error' => false,
			'user' => $users,
			'telarr' => $telArr,
			'i' => $i,
			'status' => $status,
			'simcount' => $sims->count(),
			'status_code' => 200
		));
	}

	public function saveConfiguration(int $id)
	{
		$konfig = Konfiguracio::findOrFail($id);
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$document = $phpWord->loadTemplate('templates/konfiglap_temp.docx');
		$document->setValue('sorszam', sprintf('%06d', $konfig->id));
		$document->setValue('datum', $konfig->sortdatum);
		$document->setValue('moduser', $konfig->moduser->name);
		$document->setValue('nev', $konfig->felhasznalo->nev);
		$document->setValue('tsz', $konfig->felhasznalo->tsz);
		$document->setValue('atvetel', ($konfig->statusz=='kiadás'?'ÁTVETTEM':'ÁTADTAM'));
		$user = DB::connection('oracle1')->select(DB::raw("select ass.assignment_number tsz, pap.full_name dolgozo_nev, decode(tsz.vege,'4712.12.31','aktív dolgozó',tsz.vege) vege,  j.munkakor nev,  meszi_pkg.telepnev(ass.location_id) telep, ppg.segment3 kh, mi_kh_pkg.koltseghelynev(ppg.segment3) khnev, substr(decode(hrbtelefon.mobilszamtitkos(pap.person_id),
		null,hrbtelefon.mobiltelefonszam(pap.person_id,1),
		0,hrbtelefon.mobiltelefonszam(pap.person_id,1),
		null),1,30) mobil, pap.email_address email from apps.per_all_assignments_f ass, apps.per_all_people_f pap, berlux.fkf_hr_job j, apps.pay_people_groups ppg,(select assignment_number tsz, to_char(min(effective_start_date),'yyyy.mm.dd') kezdete, to_char(max(effective_end_date),'yyyy.mm.dd') vege from apps.per_all_assignments_f where assignment_status_type_id <> 3 and assignment_number = '".$konfig->felhasznalo->tsz."' group by assignment_number) tsz where ass.assignment_status_type_id <> 3 and ((trunc(to_date(tsz.vege,'yyyy.mm.dd'))<=trunc(sysdate) and trunc(to_date(tsz.vege,'yyyy.mm.dd')) between trunc(ass.effective_start_date) and trunc(ass.effective_end_date)) or (trunc(to_date(tsz.vege,'yyyy.mm.dd'))>trunc(sysdate) and trunc(greatest(to_date(tsz.kezdete,'yyyy.mm.dd'),sysdate)) between trunc(ass.effective_start_date) and trunc(ass.effective_end_date))) and ass.assignment_number=tsz.tsz and ass.person_id=pap.person_id and trunc(to_date(tsz.vege,'yyyy.mm.dd')) between trunc(pap.effective_start_date) and trunc(pap.effective_end_date) and ass.job_id=j.id(+) and ass.people_group_id=ppg.people_group_id(+)"));
		if (count($user)>0){
			$document->setValue('koltseghely', $user[0]->kh);
			$document->setValue('szervezet', $user[0]->khnev);
		}
		$eszkozDb = $konfig->eszkoz->count();
		$document->cloneBlock('eszkoz',$eszkozDb, true, true);
		$konfig->eszkoz->each(function($item, $key) use ($document){
			$document->setValue('eszktip#'.($key+1), $item->tipus->gyarto.' '.$item->tipus->tipus);
			$document->setValue('imei#'.($key+1), $item->IMEI);
			$document->setValue('leltari#'.($key+1), $item->leltariszam);
			$document->setValue('beruhazas#'.($key+1), ($item->beruhazas_id?:''));
		});
		$simDb = $konfig->sim->count();
		$document->cloneBlock('sim',$simDb, true, true);
		$konfig->sim->each(function($item, $key) use ($document){
			$document->setValue('icc#'.($key+1), $item->icc);
			$document->setValue('telefonszam#'.($key+1), $item->telefonszam);
		});
		$kiegDb = $konfig->kiegeszito->count();
		$document->cloneBlock('kieg',$kiegDb, true, true);
		$konfig->kiegeszito->each(function($item, $key) use ($document){
			$document->setValue('kiegnev#'.($key+1), $item->nev.($item->leltariszam?' ('.$item->leltariszam.')':''));
			$document->setValue('kieg_beruhazas#'.($key+1), ($item->beruhazas_id?'Beruh.az.:'.$item->beruhazas_id:''));
		});
		if (!file_exists(public_path('tmp'))) {
			mkdir( public_path('tmp'));
		}
		$name = '/tmp/konfiguracio_MOB-'.sprintf('%06d', $konfig->id);
		$export_file = public_path($name.'.docx');
		$document->saveAs($export_file);
		shell_exec("libreoffice7.0 --headless --convert-to pdf $export_file --outdir ".public_path('tmp')." && rm '$export_file'");
		return response()->file(public_path($name.'.pdf'))->deleteFileAfterSend(true);
	}
}
