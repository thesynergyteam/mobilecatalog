<?php

/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
	public function __construct() {
		$this->middleware(['auth', 'isAdmin']); //isAdmin middleware lets only users with a specific permission permission to access these resources
	}

	/**
	* Display a listing of the resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function index() {
		$users = User::orderBy('name')->get();
		return view('users.index')->with('users', $users);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	* Display the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function show($id) {
		return redirect('users');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$user = User::findOrFail($id); //Get user with specified id
		$roles = Role::get(); //Get all roles

		return view('users.edit', compact('user', 'roles')); //pass user and roles data to view
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$user = User::findOrFail($id); //Get role specified by id
		$roles = $request['roles']; //Retreive all roles

		if (isset($roles)) {
			$user->roles()->sync($roles);  //If one or more role is selected associate user to roles
		}
		else {
			$user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
		}
		return redirect()->route('users.index')
			->with('flash_message', 'A felhasználó szerepköre sikeresen módosítva!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
