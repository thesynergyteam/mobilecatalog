<?php
/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tipus;
use Illuminate\Validation\Rule;

class TypeController extends Controller
{
	public function __construct() {
		$this->middleware(['auth']);
		$this->middleware('permission:Eszköz típusok szerkesztése', ['except' => ['index']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$tipusok = Tipus::with('eszkoz')->orderBy('gyarto')->orderBy('tipus')->paginate(200);
		return view('types.index')->with('types', $tipusok);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('types.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'gyarto'=>'required|min:2',
			'tipus'=>['required',
					Rule::unique('mobile__tipus')->where(function ($query) use($request) {
					return $query->where('gyarto', $request->gyarto)
					->where('tipus', $request->tipus);
				})],
			]);
		Tipus::create($request->all());

		return redirect()->route('types.index')
			->with('flash_message', 'Telefon típus hozzáadva!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id = car id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$type = Tipus::where('id',$id)->firstOrFail();
		return view('types.edit')->with('type', $type);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'gyarto'=>'required|min:2',
			'tipus'=>['required',
					Rule::unique('mobile__tipus')->where(function ($query) use($request,$id) {
					return $query->where('gyarto', $request->gyarto)
					->where('tipus', $request->tipus)
					->whereNotIn('id', [$id]);
				})],
			]);
		Tipus::where('id',$id)->update($request->only('gyarto', 'tipus'));

		return redirect()->route('types.index')
			->with('flash_message', 'Telefon típus frissítve!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{

	}
}
