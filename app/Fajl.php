<?php
/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App;

class Fajl extends BaseModel
{
	protected $table = 'mobile__konfig_files';
	public $timestamps = false;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'konfiguracio_id', 'utvonal', 'fajlnev','moduser_id'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		//
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		//
	];

	public function konfiguracio()
	{
		return $this->belongsTo('App\Konfiguracio');
	}
	public function getDatumAttribute() {
		return \Carbon\Carbon::parse($this->mod_datum)->format('Y.m.d H:i:s');
	}
	public function moduser()
	{
		return $this->belongsTo('App\User');
	}
}
