<?php

/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2022(c) KoRi <kovacsr@fkf.hu>
 *
 */

if (! function_exists('hruser')) {
    function hruser($tsz = 'null') {
        $user = DB::connection('oracle1')->select(DB::raw("select ass.assignment_number tsz, pap.full_name dolgozo_nev, decode(tsz.vege,'4712.12.31','aktív dolgozó',tsz.vege) vege,  j.munkakor nev,  meszi_pkg.telepnev(ass.location_id) telep, ppg.segment3 kh, mi_kh_pkg.koltseghelynev(ppg.segment3) khnev, substr(decode(hrbtelefon.mobilszamtitkos(pap.person_id),
		null,hrbtelefon.mobiltelefonszam(pap.person_id,1),
		0,hrbtelefon.mobiltelefonszam(pap.person_id,1),
		null),1,30) mobil, pap.email_address email from apps.per_all_assignments_f ass, apps.per_all_people_f pap, berlux.fkf_hr_job j, apps.pay_people_groups ppg,(select assignment_number tsz, to_char(min(effective_start_date),'yyyy.mm.dd') kezdete, to_char(max(effective_end_date),'yyyy.mm.dd') vege from apps.per_all_assignments_f where assignment_status_type_id <> 3 and assignment_number = '$tsz' group by assignment_number) tsz where ass.assignment_status_type_id <> 3 and ((trunc(to_date(tsz.vege,'yyyy.mm.dd'))<=trunc(sysdate) and trunc(to_date(tsz.vege,'yyyy.mm.dd')) between trunc(ass.effective_start_date) and trunc(ass.effective_end_date)) or (trunc(to_date(tsz.vege,'yyyy.mm.dd'))>trunc(sysdate) and trunc(greatest(to_date(tsz.kezdete,'yyyy.mm.dd'),sysdate)) between trunc(ass.effective_start_date) and trunc(ass.effective_end_date))) and ass.assignment_number=tsz.tsz and ass.person_id=pap.person_id and trunc(to_date(tsz.vege,'yyyy.mm.dd')) between trunc(pap.effective_start_date) and trunc(pap.effective_end_date) and ass.job_id=j.id(+) and ass.people_group_id=ppg.people_group_id(+)"));
		return (count($user)>0)?$user[0]:null;
    }
}