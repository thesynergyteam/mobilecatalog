<?php

/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
	public function selectQuery($sql_stmt) {
		return DB::select($sql_stmt);
	}

	public function sqlStatement($sql_stmt) {
		DB::statement($sql_stmt);
	}
}
