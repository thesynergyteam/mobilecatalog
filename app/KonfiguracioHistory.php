<?php
/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App;

class KonfiguracioHistory extends BaseModel
{
	protected $table = 'mobile__konfig_history';
	public $timestamps = false;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'konfiguracio_id', 'kategoria_id','megjegyzes','statusz','moduser_id'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		//
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		//
	];

	public function konfiguracio()
	{
		return $this->belongsTo('App\Konfiguracio');
	}
	public function kategoria()
	{
		return $this->belongsTo('App\Kategoria');
	}
	public function moduser()
	{
		return $this->belongsTo('App\User');
	}
	public function scopeWhereKonf($query, $konfid) {
		return $query->whereHas('konfiguracio', function($q) use ($konfid){
			$q->where('id',$konfid);
		});
	}
	public function getModDatumAttribute($value) {
		return $value?\Carbon\Carbon::parse($value)->format('Y.m.d H:i:s'):'';
	}
}
