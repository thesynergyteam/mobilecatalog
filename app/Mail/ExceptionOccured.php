<?php

/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ExceptionOccured extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The body of the message.
     *
     * @var string
     */
    public $content;
	public $css;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content, $css = '')
    {
        $this->content = $content;
		$this->css = $css;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('MobileCatalog'.(\App::environment('dev')?'-dev':'').' Exception')
				->view('emails.exception')
                ->with('content', $this->content)
                ->with('css', $this->css);
    }
}