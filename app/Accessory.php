<?php
/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App;

class Accessory extends BaseModel
{
	protected $table = 'mobile__accessories';
	public $timestamps = false;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		//
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		//
	];

	public function beszallito()
	{
		return $this->belongsTo('App\Beszallito');
	}
	public function felhasznalo()
	{
		return $this->belongsTo('App\Felhasznalo')->withDefault([
			'nev' => 'nincs hozzárendelve',
			'tsz' => ''
		]);
	}
	public function konfig()
	{
		return $this->belongsTo('App\Konfiguracio');
	}
	public function statusz()
	{
		return $this->belongsTo('App\Statusz');
	}
}
