<?php
/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App;

class Mobileszkoz extends BaseModel
{
	protected $table = 'mobile__eszkoz';
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
		//
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //'email_verified_at' => 'datetime',
    ];

	public function beszallito()
	{
		return $this->belongsTo('App\Beszallito');
	}
	public function tipus()
	{
		return $this->belongsTo('App\Tipus');
	}
	public function kategoria()
	{
		return $this->belongsTo('App\Kategoria');
	}
	public function statusz()
	{
		return $this->belongsTo('App\Statusz');
	}
	public function felhasznalo()
	{
		return $this->belongsTo('App\Felhasznalo')->withDefault([
			'nev' => 'nincs hozzárendelve',
			'tsz' => ''
		]);
	}
}
