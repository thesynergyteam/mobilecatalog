<?php
/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

namespace App;

class Konfiguracio extends BaseModel
{
	protected $table = 'mobile__konfig';
	public $timestamps = false;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'eszkoz_id', 'sim_id','felhasznalo_id', 'kategoria_id','statusz','megjegyzes','moduser_id'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		//
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		//
	];

	public function eszkoz()
	{
		return $this->belongsToMany('App\Mobileszkoz', 'mobile__konfig_has_eszkoz','konfig_id','eszkoz_id');
	}
	public function sim()
	{
		return $this->belongsToMany('App\Sim','mobile__konfig_has_sim', 'konfig_id');
	}
	public function kategoria()
	{
		return $this->belongsTo('App\Kategoria');
	}
	public function kiegeszito()
	{
		return $this->belongsToMany('App\Accessory','mobile__konfig_has_accessory','konfig_id','accessory_id');
	}
	public function moduser()
	{
		return $this->belongsTo('App\User');
	}
	public function felhasznalo()
	{
		return $this->belongsTo('App\Felhasznalo')->withDefault([
			'nev' => 'nincs hozzárendelve',
			'tsz' => ''
		]);
	}
	public function fajlok()
	{
		return $this->hasMany('App\Fajl');
	}
	public function history()
	{
		return $this->hasMany('App\KonfiguracioHistory');
	}
	public function visszavetel()
	{
		return $this->hasOne('App\KonfiguracioHistory')->where('statusz', 'visszavét')
				->orderBy('mod_datum','desc')->withDefault([
			'statusz' => '',
			'mod_datum' => ''
		]);
	}

	public function getSortdatumAttribute() {
		return \Carbon\Carbon::createFromFormat('Y.m.d H:i:s', $this->datum)->format('Y.m.d');
	}
	public function getDatumAttribute($value) {
		return \Carbon\Carbon::parse($value)->format('Y.m.d H:i:s');
	}
}
