<?php

/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

return [
    'mail' => [
        'send_to_default'	=> env('MAIL_TO_DEFAULT', ''),
		'send_to_fallback'	=> env('MAIL_TO_FALLBACK', ''),
		'send_to_tjav_team'	=> env('MAIL_TO_TJAV', env('MAIL_TO_DEFAULT', '')),
		'send_to_szemelyes'	=> env('MAIL_TO_SZEM', env('MAIL_TO_DEFAULT', '')),
		'send_to_exception'	=> env('MAIL_TO_EXCEPTION', 'kovacsr@fkf.hu'),
    ],
	'permission' => [
        'admin'				=> env('ADMIN_PERMISSION', 'Administer roles & permissions'),
    ],
	'curl' => [
        'proxy'				=> env('CURL_PROXY', ''),
		'proxyuserpass'		=> env('CURL_PROXYUSERPWD', ''),
    ],
];