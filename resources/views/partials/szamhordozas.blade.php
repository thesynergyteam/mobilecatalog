@if (count($szam)>0)
<h4>Számhordozások</h4>
<table class="table table-bordered table-striped table-responsive-sm table-sm">
	<thead>
		<tr>
			<th>Dátum</th>
			<th>Státusz</th>
			<th>Felhasználó</th>
			<th>Telefonszám</th>
			<th>Műveletek</th>
		</tr>
	</thead>

	<tbody>
	@foreach($szam as $hordozas)
		<tr>
			<td>{{ $hordozas->datum }}</td>
			<td>{{ $hordozas->statusz }}</td>
			<td class='f-user'>{{ $hordozas->felhasznalo->nev }} ({{ $hordozas->felhasznalo->tsz }})</td>
			<td class='f-szam'>{{ $hordozas->sim->telefonszam }}</td>
			<td><a href="{{ route('transfers.edit', $hordozas->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Részletek</a></td>
		</tr>
	@endforeach
	</tbody>
</table>
@endif