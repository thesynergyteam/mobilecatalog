@extends('layouts.app')

@section('title', 'Jogosultság szerkesztése')

@section('content')

<div class='col-md-6 offset-md-3'>

    <h1><i class='fa fa-key'></i> {{$permission->name}} szerkesztése</h1>
    <br>
    <form action="{{ route('permissions.update', $permission->id) }}" method="POST">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="name">Jogosultság neve</label>
            <input type="text" name="name" class="form-control" value="{{$permission->name}}">
        </div>
        <br>
        <button type="submit" class="btn btn-primary" >Mentés</button>
        <button type="button" class="btn btn-secondary" onclick="window.location='{{ route('permissions.index')}}'">Mégse</button>

    </form>

</div>

@endsection