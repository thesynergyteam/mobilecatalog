@extends('layouts.app')

@section('title', 'Jogosultság létrehozása')

@section('content')

<div class='col-md-6 offset-md-3'>

    <h1><i class='fa fa-key'></i> Jogosultság létrehozása</h1>
    <br>

    <form action="{{ url('permissions') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Név</label>
            <input type="text" name="name" class="form-control" value="{{old('name')}}">
        </div><br>
        @if(!$roles->isEmpty())
            <h4>Jogosultság hozzárendelése szerepkörökhöz</h4>

            @foreach ($roles as $role)
                <input type="checkbox" name="roles[]" value="{{$role->id}}" >
                <label for="{{$role->name}}">{{ucfirst($role->name)}}</label><br>
            @endforeach
        @endif
        <br>
        <button type="submit" class="btn btn-primary" >Hozzáadás</button>
        <button type="button" class="btn btn-secondary" onclick="window.location='{{ route('permissions.index')}}'">Mégse</button>

    </form>

</div>

@endsection