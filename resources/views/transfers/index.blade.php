@extends('layouts.app')

@section('title', 'Számhordozások')

@section('transactions', 'active')
@section('szamhordozas', 'active')

@push('head')
	<script src="{{ asset('js/sorting.js') }}" defer></script>
@endpush

@section('content')

<div class="col-xl-6 offset-xl-3">
    <h1><i class="fas fa-hand-holding"></i> Számhordozások</h1>
    <hr>@can('Számhordozás szerkesztése')
	<a href="{{ route('transfers.create') }}" class="btn btn-success mb-2">Új számhordozás hozzáadása</a>@endcan
    <div class="table-responsive filtertable">
		{{ $szamhordozasok->links() }}
        <table class="table table-bordered table-striped table-sm">

            <thead>
                <tr>
					<th>Dátum</th>
					<th>Státusz</th>
					<th><form class="form-inline"><label>Felhasználó</label><input type="search" class="form-control form-control-sm ml-auto txt-filter" data-filter="f-user" placeholder="Szűkítés..." /></form></th>
                    <th><form class="form-inline"><label>Telefonszám</label><input type="search" class="form-control form-control-sm ml-auto txt-filter" data-filter="f-szam" placeholder="Szűkítés..." /></form></th>
					<th>Műveletek</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($szamhordozasok as $hordozas)
                <tr>
					<td>{{ $hordozas->datum }}</td>
					<td>{{ $hordozas->statusz }}</td>
                    <td class='f-user'>{{ $hordozas->felhasznalo->nev }} ({{ $hordozas->felhasznalo->tsz }})</td>
                    <td class='f-szam'>{{ $hordozas->sim->telefonszam }}</td>
					<td><a href="{{ route('transfers.edit', $hordozas->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Részletek</a></td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>
</div>

@endsection