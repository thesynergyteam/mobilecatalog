@extends('layouts.app')

@section('title', 'Számhordozás részletek')

@section('transactions', 'active')
@section('szamhordozas', 'active')

@push('head')
	<link href="{{ asset('css/bootstrap-datepicker3.min.css') }}"  rel="stylesheet">
	<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" defer></script>
	<script src="{{ asset('js/bootstrap-datepicker.hu.min.js') }}" defer></script>
	<script src="{{ asset('js/szamhordozas.js') }}" defer></script>
	<style>
		.cursor-pointer {
			cursor: pointer;
		}
		.datepicker-dropdown{
			z-index: 1040 !important;
		}
	</style>
@endpush
@section('content')
<div class='col-xl-6 col-md-8 offset-xl-3 offset-md-2'>

	<h1><i class='fas fa-hand-holding'></i> Számhordozás részletek</h1>
	<br>

	<form action="{{ route('transfers.update', $szam->id) }}" method="POST" enctype="multipart/form-data">
		@method('PUT')
        @csrf
		<div class="row">
			<div class="col-md-3 form-group">
				<label for="tsz">Törzsszám</label>
				<input type="text" name="tsz" id="tsz" class="form-control" value="{{$szam->felhasznalo->tsz}}" readonly="">
			</div>
			<div class="col-md-6 form-group">
				<label for="usernev">Felhasználó</label>
				<input type="text" name="usernev" id="usernev" class="form-control" value="{{ $szam->felhasznalo->nev }}" readonly="">
				<input type="hidden" id="felhasznalo" value="{{ $szam->felhasznalo_id }}">
			</div>
			<div class="col-md-3 form-group">
				<label for="statusz">Státusz</label>
				<input type="text" name="statusz" id="statusz" class="form-control" value="{{ $szam->statusz }}" readonly="">
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 form-group">
				<label for="szuletesi_ido">Születési idő</label>
				<input type="text" name="szuletesi_ido" id="szuletesi_ido" class="form-control" data-provide="datepicker" data-date-days-of-week-highlighted="6,0" data-date-today-highlight="true" data-date-max-view-mode="2" data-date-language="hu" data-date-autoclose="true" data-date-format="yyyy.mm.dd" value="{{ $szam->felhasznalo->szuletesi_ido }}" readonly="">
			</div>
			<div class="col-md-3 form-group">
				<label for="szemigszam">Szem.ig. szám</label>
				<input type="text" name="szemigszam" id="szemigszam" class="form-control" value="{{  $szam->felhasznalo->szemigszam }}" maxlength="10">
			</div>
			<div class="col-md-3 form-group">
				<label for="koltseghely">Költséghely</label>
				<input type="text" name="koltseghely" id="koltseghely" class="form-control" value="{{  $szam->felhasznalo->koltseghely }}" maxlength="10">
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 form-group">
				<label for="anyjaneve">Anyja neve</label>
				<input type="text" name="anyjaneve" id="anyjaneve" class="form-control" value="{{  $szam->felhasznalo->anyjaneve }}">
			</div>
			<div class="col-md-7 form-group">
				<label for="lakcim">Lakcím</label>
				<input type="text" name="lakcim" id="lakcim" class="form-control" value="{{  $szam->felhasznalo->lakcim }}">
			</div>
		</div>
		<hr>
		<div class="row mb-2">
			<div class="col-md-10">
				<label for="sim">SIM kártya</label>
				<input type="text" name="sim" id="sim" class="form-control" value="{{ $szam->sim->icc }} ({{ $szam->sim->telefonszam }})" readonly="">
			</div>
			<div class="col-md-2 form-group">
				<label for="datum">Dátum</label>
				<input type="text" name="datum" class="form-control" data-provide="datepicker" data-date-days-of-week-highlighted="6,0" data-date-today-highlight="true" data-date-max-view-mode="2" data-date-language="hu" data-date-autoclose="true" data-date-format="yyyy.mm.dd" value="{{ $szam->datum }}" readonly="">
			</div>
		</div>
		<div class="row">
			<div class="col-md-9 form-group">
				<label for="megjegyzes">Megjegyzés</label>
				<input type="text" name="megjegyzes" class="form-control" value="{{$szam->megjegyzes}}">
			</div>
			<div class="col-md-3 form-group">
				<label for="moduser">Moduser</label>
				<input type="text" name="moduser" class="form-control" value="{{ $szam->moduser->name }}" readonly="">
			</div>
		</div>
		<div class="row">
			<div class="col-md-7 form-group">
				<label for="dokumentum">Dokumentum</label>
				<input type="file" name="dokumentum" class="form-control-file">
			</div>
			<div class="col-md-5 form-group">
				<label for="letoltes">Feltöltött dokumentumok</label>
				<div class="d-flex flex-column">
				@foreach($szam->fajlok as $fajl)
				<span><a href="/showfile/?fajlnev={{$fajl->fajlnev}}&utvonal={{$fajl->utvonal}}" target="_blank" title="{{$fajl->datum}} {{$fajl->moduser->name}}">{{$fajl->fajlnev}}</a>@can('Fájlok törlése')<a class="float-right" onclick='return confirm("Biztos vagy a fájl törlésben?\n{{$fajl->fajlnev}}")' href="/delfile/szamhordozas/{{$fajl->id}}">&times;</a> @endcan</span>
				@endforeach
				</div>
			</div>
		</div>

		<br>
		<div class="d-flex justify-content-between">
			@can('Számhordozás szerkesztése')<button type="submit" class="btn btn-primary" >Mentés</button>@endcan
			@can('Konfiglap nyomtatás')
			@if($szam->statusz!='Behordozás')
			<a class="btn btn-info" href="{{ route('transfers.download',$szam)}}" target='_blank'><i class="fa-print fas"></i> Számhordozás nyomtatás</a>
			@endif
			@endcan
			@can('Számhordozás szerkesztése')
			@if($szam->statusz=='Elhordozás folyamatban')
			<a class="btn btn-outline-danger" onclick="return confirm('Biztos a számhordozás készre állításában?')" href="{{ route('transfers.setready',$szam)}}" ><i class="fa-check fas"></i> Számhordozás elvégezve</a>
			@endif
			@endcan
			<button type="button" class="btn btn-secondary" onclick="window.location='{{ route('transfers.index')}}'">Mégse</button>
		</div>
	</form>
</div>

@endsection