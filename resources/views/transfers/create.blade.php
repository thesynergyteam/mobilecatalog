@extends('layouts.app')

@section('title', 'Számhordozás hozzáadása')

@section('transactions', 'active')
@section('szamhordozas', 'active')

@push('head')
	<link href="{{ asset('css/bootstrap-datepicker3.min.css') }}"  rel="stylesheet">
	<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" defer></script>
	<script src="{{ asset('js/bootstrap-datepicker.hu.min.js') }}" defer></script>
	<script src="{{ asset('js/szamhordozas.js') }}" defer></script>
	<style>
		.cursor-pointer {
			cursor: pointer;
		}
		.datepicker-dropdown{
			z-index: 1040 !important;
		}
	</style>
@endpush
@section('content')
<div class='col-xl-6 col-md-8 offset-xl-3 offset-md-2'>

	<h1><i class='fas fa-hand-holding'></i> Számhordozás létrehozása</h1>
	<br>

	<form action="{{ url('transfers') }}" method="POST">
		@csrf
		<div class="row">
			<div class="col-md-3 form-group">
				<label for="tsz">Törzsszám</label>
				<div class="input-group">
					<input type="text" name="tsz" id="tsz" class="form-control" value="{{old('tsz')}}" maxlength="6" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
					<div class="input-group-append">
						<button type="button" class="btn btn-primary tszkeres">Keresés</button>
					</div>
				</div>
			</div>
			<div class="col-md-6 form-group">
				<label for="usernev_full">Felhasználó</label>
				<input type="text" name="usernev_full" id="usernev_full" class="form-control" value="{{ old('usernev_full') }}" readonly="">
				<input type="hidden" name="usernev" id="usernev" value="{{ old('usernev') }}" readonly="">
			</div>
			<div class="col-md-3 form-group">
				<label for="statusz">Státusz</label>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="statusz" id="behordozas" value="Behordozás" @if('Behordozás'==old('statusz')) checked @endif >
					<label class="form-check-label" for="behordozas"> Behordozás <i class="fas fa-arrow-right"></i><i class="fas fa-building"></i></label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="statusz" id="elhordozas" value="Elhordozás folyamatban" @if('Elhordozás folyamatban'==old('statusz')) checked @endif >
					<label class="form-check-label" for="elhordozas"> Elhordozás <i class="fas fa-building"></i><i class="fas fa-arrow-right"></i></label>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 form-group">
				<label for="szuletesi_ido">Születési idő</label>
				<input type="text" name="szuletesi_ido" id="szuletesi_ido" class="form-control" data-provide="datepicker" data-date-days-of-week-highlighted="6,0" data-date-today-highlight="true" data-date-max-view-mode="2" data-date-language="hu" data-date-autoclose="true" data-date-format="yyyy.mm.dd" value="{{ old('szuletesi_ido') }}" readonly="">
			</div>
			<div class="col-md-3 form-group">
				<label for="szemigszam">Szem.ig. szám</label>
				<input type="text" name="szemigszam" id="szemigszam" class="form-control" value="{{ old('szemigszam') }}" maxlength="10">
			</div>
			<div class="col-md-3 form-group">
				<label for="koltseghely">Költséghely</label>
				<input type="text" name="koltseghely" id="koltseghely" class="form-control" value="{{ old('koltseghely') }}" maxlength="10">
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 form-group">
				<label for="anyjaneve">Anyja neve</label>
				<input type="text" name="anyjaneve" id="anyjaneve" class="form-control" value="{{ old('anyjaneve') }}">
			</div>
			<div class="col-md-7 form-group">
				<label for="lakcim">Lakcím</label>
				<input type="text" name="lakcim" id="lakcim" class="form-control" value="{{ old('lakcim') }}">
			</div>
		</div>
		<div class="row mb-2">
			<div class="col-12 simmezo">
				<label for="sim_text">SIM kártya</label>
				<div class="input-group">
					<input type="text" class="form-control" readonly="" id="sim_text" value="@if(old('sim_id')){{ \App\Sim::where('id',old('sim_id'))->first()->icc}} ({{\App\Sim::where('id',old('sim_id'))->first()->telefonszam}})@endif">
					<input type="hidden" name="sim_id" id="sim_id" value="{{ old('sim_id') }}">
					<div class="input-group-append">
						<button type="button" class="btn btn-danger newsimadd">Hozzáadás</button>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-8 form-group">
				<label for="megjegyzes">Megjegyzés</label>
				<input type="text" name="megjegyzes" class="form-control" value="{{old('megjegyzes')}}">
			</div>
			<div class="col-md-2 form-group">
				<label for="datum">Dátum</label>
				<input type="text" name="datum" class="form-control" data-provide="datepicker" data-date-days-of-week-highlighted="6,0" data-date-today-highlight="true" data-date-max-view-mode="2" data-date-language="hu" data-date-autoclose="true" data-date-format="yyyy.mm.dd" value="{{ old('datum')?:date('Y.m.d') }}" readonly="">
			</div>
		</div>

		<br>
		<button type="submit" class="btn btn-primary" >Létrehoz</button>
		<button type="submit" class="btn btn-primary" name="saveandedit">Létrehoz és szerkeszt</button>
		<button type="button" class="btn btn-secondary" onclick="window.location='{{ route('transfers.index')}}'">Mégse</button>

	</form>
	<div class="modal fade" id="dialog_sim_message" tabindex="-1" role="dialog" >
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">SIM kártya keresése</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Bezár"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 form-group">
							<label for="search_sim">Keresés ICC /telefonszám alapján</label>
							<div class="input-group">
								<input type="text" name="search_sim" id="search_sim" class="form-control" placeholder="Rövidített keresés, pl: %42644" value="{{old('search_sim')}}" maxlength="15">
								<div class="input-group-append">
									<button type="button" class="btn btn-outline-primary simkeres" data-sim="icc">ICC keres</button>
									<button type="button" class="btn btn-outline-primary simkeres" data-sim="tel">Telszám keres</button>
								</div>
							</div>
						</div>
						<div class="col-md-12 form-group">
							<label for="eredmeny_sim">Eredmény</label>
							<textarea id="eredmeny_sim" rows="4" class="form-control" readonly=""></textarea>
							<input type="hidden" id="eredmeny_sim_id">
							<input type="hidden" id="eredmeny_sim_text">
						</div>
						<div class="mx-3 alert alert-danger d-none" role="alert" id="warning_sim">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary d-none">Kiválaszt</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Mégse</button>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection