@extends('layouts.app')

@section('title', 'Új eszköz típus hozzáadása')

@section('types', 'active')
@section('db', 'active')

@section('content')

<div class='col-xl-4 col-md-6 offset-xl-4 offset-md-3'>

    <h1><i class='fas fa-mobile-alt'></i> Új eszköz típus hozzáadása</h1>
    <br>

    <form action="{{ url('types') }}" method="POST">
        @csrf
		<div class="row">
			<div class="col-md-6 form-group">
				<label for="gyarto">Gyártó</label>
				<input type="text" name="gyarto" class="form-control" list=gyartok value="{{old('gyarto')}}">
				<datalist id=gyartok>
					@foreach(\App\Tipus::select('gyarto')->distinct()->get() as $tipus)
					<option value="{{$tipus->gyarto}}">{{$tipus->gyarto}}</option>
					@endforeach
				</datalist>
			</div>
			<div class="col-md-6 form-group">
				<label for="tipus">Típus</label>
				<input type="text" name="tipus" class="form-control" list=tipusok value="{{old('tipus')}}">
				<datalist id=tipusok>
					@foreach(\App\Tipus::select('tipus')->distinct()->get()->sort() as $tipus)
					<option value="{{$tipus->tipus}}">{{$tipus->tipus}}</option>
					@endforeach
				</datalist>
			</div>
		</div>
        <br>
        <button type="submit" class="btn btn-primary" >Hozzáadás</button>
        <button type="button" class="btn btn-secondary" onclick="window.location='{{ route('types.index')}}'">Mégse</button>

    </form>

</div>

@endsection