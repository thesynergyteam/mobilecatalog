@extends('layouts.app')

@section('title', 'Eszköz típusok')

@section('types', 'active')
@section('db', 'active')

@push('head')
	<script src="{{ asset('js/sorting.js') }}" defer></script>
@endpush

@section('content')

<div class="col-xl-6 offset-xl-3">
    <h1><i class="fas fa-mobile-alt"></i> Eszköz típusok</h1>
    <hr>@can('Eszköz típusok szerkesztése')
	<a href="{{ route('types.create') }}" class="btn btn-success mb-2">Új eszköz típus hozzáadása</a>@endcan
    <div class="table-responsive filtertable">
		{{ $types->links() }}
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
					<th>Darab</th>
					<th><form class="form-inline"><label>Gyártó</label><input type="search" class="form-control form-control-sm ml-auto txt-filter" data-filter="f-gyarto" placeholder="Szűkítés..." /></form></th>
                    <th><form class="form-inline"><label>Típus</label><input type="search" class="form-control form-control-sm ml-auto txt-filter" data-filter="f-tipus" placeholder="Szűkítés..." /></form></th>
                    @can('Eszköz típusok szerkesztése')
					<th>Műveletek</th>
					@endcan
                </tr>
            </thead>

            <tbody>
                @foreach ($types as $type)
                <tr>
					<td>{{ $type->eszkoz->count() }}db</td>
                    <td class='f-gyarto'>{{ $type->gyarto }}</td>
                    <td class='f-tipus'>{{ $type->tipus }}</td>
                    @can('Eszköz típusok szerkesztése')
					<td>
                    <a href="{{ route('types.edit', $type->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Részletek</a>
                    </td>
					@endcan
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>
</div>

@endsection