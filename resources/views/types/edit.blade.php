@extends('layouts.app')

@section('title', 'Eszköz típus szerkesztése')

@section('types', 'active')
@section('db', 'active')

@section('content')

<div class='col-xl-4 col-md-6 offset-xl-4 offset-md-3'>

    <h1><i class='fas fa-mobile-alt'></i> Eszköz típus részletek </h1>
    <br>
    <form action="{{ route('types.update', $type->id) }}" method="POST">
        @method('PUT')
        @csrf
		<div class="row">
			<div class="col-md-4 form-group">
				<label for="gyarto">Gyártó</label>
				<input type="text" name="gyarto" class="form-control" value="{{$type->gyarto}}">
			</div>
			<div class="col-md-8 form-group">
				<label for="tipus">Típus</label>
				<input type="text" name="tipus" class="form-control" value="{{$type->tipus}}">
			</div>
		</div>
        <br>
        <button type="submit" class="btn btn-primary" >Mentés</button>
        <button type="button" class="btn btn-secondary" onclick="window.location='{{ route('types.index')}}'">Vissza</button>

    </form>

</div>

@endsection