@extends('errors.minimal')

@section('code')
<img src="images/no_ie_support.jpg" width="50px">
@endsection

@section('title', __('Explorer nem használható'))

@section('image')
<div style="background-image: url({{ asset('/svg/404.svg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
</div>
@endsection

@section('message', __('Sajnáljuk, az alkalmazás megtekintéséhez az Internet Explorer böngésző nem használható.'))
