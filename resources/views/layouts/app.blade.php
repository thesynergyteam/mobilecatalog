<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Scripts -->
	<script src="{{ asset('js/app.js') }}" defer></script>

	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

	<!-- Styles -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="{{asset('css/fontawesome.min.css')}}" rel="stylesheet">
		<style>
	p.teaser {
		text-indent: 30px;
	}
	input.form-control::placeholder {
		color: #c1bfbf;
	}
	.navbar-light .navbar-nav .active>.nav-link, .navbar-light .navbar-nav .nav-link.active {
		text-decoration: underline;
	}
	</style>
	@stack('head')
</head>
<body>
	<div id="app">
		<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm sticky-top">
			<div class="container">
				<a class="navbar-brand" href="{{ url('/') }}">
					{{ config('app.name', 'Laravel') }}
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<!-- Left Side Of Navbar -->
					<ul class="navbar-nav mr-auto">
						@auth
						<li class="nav-item dropdown @yield('transactions')">
							<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown">
								<i class="fas fa-random"></i> Tranzakciók
							</a>
							<div class="dropdown-menu">
								<a class="dropdown-item @yield('masssim')" href="{{ route('sims.masscreate') }}"><i class="fas fa-fw fa-file-import"></i> Tömeges sim bevételezés</a>
								<a class="dropdown-item @yield('massdevice')" href="{{ route('devices.masscreate') }}"><i class="fas fa-fw fa-file-import"></i> Tömeges eszköz bevételezés</a>
								<a class="dropdown-item @yield('configurations')" href="{{ route('configurations.index') }}"><i class="fas fa-fw fa-cogs"></i> Konfigurációk</a>
								@can('Számhordozás megtekintése')<a class="dropdown-item @yield('szamhordozas')" href="{{ route('transfers.index') }}"><i class="fas fa-fw fa-hand-holding"></i> Számhordozás</a>@endcan
								<a class="dropdown-item disabled" href=""><i class="fas fa-fw fa-trash-alt"></i> Selejtezés</a>
							</div>
						</li>
						<li class="nav-item dropdown @yield('db')">
							<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown">
								<i class="fas fa-database"></i> Törzsadatok
							</a>
							<div class="dropdown-menu">
								<a class="dropdown-item @yield('suppliers')" href="{{ route('suppliers.index') }}"><i class="fas fa-fw fa-users"></i> Beszállítók</a>
								<a class="dropdown-item @yield('types')" href="{{ route('types.index') }}"><i class="fas fa-fw fa-copyright"></i> Eszköz típusok</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item @yield('devices')" href="{{ route('devices.index') }}"><i class="fas fa-fw fa-mobile-alt"></i> Mobil eszközök</a>
								<a class="dropdown-item @yield('sims')" href="{{ route('sims.index') }}"><i class="fas fa-fw fa-sim-card"></i> Sim kártyák</a>
								<a class="dropdown-item @yield('accessories')" href="{{ route('accessories.index') }}"><i class="fas fa-fw fa-keyboard"></i>  Kiegészítő eszközök</a>
							</div>
						</li>
						<li class="nav-item @yield('queries')">
							<a class="nav-link @yield('userquery')" href="{{ route('configurations.query') }}"><i class="fas fa-fw fa-search"></i> Lekérdezések</a>
						</li>

						@endauth
					</ul>

					<!-- Right Side Of Navbar -->
					<ul class="navbar-nav ml-auto">
						<!-- Authentication Links -->
						@guest
							<li class="nav-item">
								<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
							</li>
							@if (Route::has('register'))
								<li class="nav-item">
									<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
								</li>
							@endif
						@else
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown">
									{{ Auth::user()->name }}
								</a>
								<div class="dropdown-menu dropdown-menu-right">
									@can(config('custom.permission.admin'))
									<a class="dropdown-item" href="{{ route('users.index') }}"><i class="fas fa-fw fa-unlock"></i> Jogosultság kezelés</a>
									@endcan
									<a class="dropdown-item" href="{{ route('logout') }}"
									   onclick="event.preventDefault();
													 document.getElementById('logout-form').submit();">
										<i class="fas fa-fw fa-sign-out-alt"></i> {{ __('Logout') }}
									</a>

									<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
										@csrf
									</form>
								</div>
							</li>
						@endguest
					</ul>
				</div>
			</div>
		</nav>
		@if(Session::has('flash_message'))
		<div class="container">
			<div class="alert alert-success"><em> {!! session('flash_message') !!}</em>
			</div>
		</div>
		@endif
		<div class="col-md-8 offset-md-2">
				@include ('errors.list') {{-- Including error file --}}
		</div>
		<main class="py-4">
			@yield('content')
		</main>
	</div>
</body>
</html>
