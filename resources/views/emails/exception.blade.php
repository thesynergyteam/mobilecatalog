<?php

/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <style>{!! $css ?? '' !!}</style>
    </head>
    <body>
        {!! $content ?? '' !!}
    </body>
</html>