@extends('layouts.app')

@section('title', 'Új eszközök bevételezése')

@section('transactions', 'active')
@section('massdevice', 'active')

@push('head')
	<link href="{{ asset('css/bootstrap-datepicker3.min.css') }}"  rel="stylesheet">
	<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" defer></script>
	<script src="{{ asset('js/bootstrap-datepicker.hu.min.js') }}" defer></script>
	<script src="{{ asset('js/jquery.csv.min.js') }}" type='text/javascript' defer></script>
	<script src="{{ asset('js/massdevice.js') }}" defer></script>
	<style>
		.fileInput{
			position: relative;
			margin-left: auto;
		}
		#txtFileUpload{
			position: relative;
			opacity: 0;
			width: 150px;
			height: 37px;
			z-index: 2;
		}
		.fileInput .btn-warning{
			position: absolute;
			top: 0;
			left: 0;
		}
		.close{
			position: absolute;
			right: 5px;
		}
	</style>
@endpush

@section('content')

<div class='col-md-8 offset-md-2'>

    <h1><i class='fas fa-sim-card'></i> Új eszközök bevételezése</h1>
    <br>

    <form action="{{ url('devices') }}" method="POST">
        @csrf

		<input type="hidden" name="mode" value="mass">
		<div class="row">
			<div class="col-sm-5 form-group">
				<label for="beszallito">Beszállító</label>
				<select class="custom-select" name="beszallito">
					<option value="" selected disabled>Válassz beszállítót!</option>
					@foreach(\App\Beszallito::all() as $besz)
					<option value="{{$besz->id}}" {{ old('beszallito')==$besz->id?'selected':'' }}>{{$besz->partner}}</option>
					@endforeach
				</select>
			</div>
			<div class="col-sm-5 form-group">
				<label for="szamlaszam">Számlaszám</label>
				<input type="text" name="szamlaszam" class="form-control" value="{{old('szamlaszam')}}" >
			</div>
			<div class="col-md-2 form-group">
				<label for="datum">Vásárlás dátuma</label>
				<input type="text" name="datum" class="form-control" data-provide="datepicker" data-date-days-of-week-highlighted="6,0" data-date-today-highlight="true" data-date-max-view-mode="2" data-date-language="hu" data-date-autoclose="true" data-date-format="yyyy.mm.dd" value="{{ old('datum') }}" readonly="">
			</div>
		</div>
		<hr>
		<div class="form-row">
			<div class="col-6 col-sm-3">
				<select class="custom-select" id="tipus_input">
					<option value="" selected disabled>Válassz típust!</option>
					@foreach(\App\Tipus::orderBy('gyarto')->orderBy('tipus')->get() as $tipus)
					<option value="{{$tipus->id}}">{{$tipus->nev}}</option>
					@endforeach
				</select>
			</div>
			<div class="col-6 col-sm-3">
				<input type="text" class="form-control" id="imei_input" maxlength="15" placeholder="IMEI/SN/FKF-leltári">
			</div>
			<div class="col-6 col-sm-3">
				<input type="text" class="form-control" id="imei2_input" maxlength="15" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="IMEI2">
			</div>
			<div class="col-6 col-sm-1">
				<input type="text" class="form-control" id="leltari_input" maxlength="6" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Leltári szám">
			</div>
			<div class="col-6 col-sm-2">
				<div class="input-group">
					<input type="text" class="form-control" id="ertek_input" maxlength="6" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Érték (nettó)">
					<div class="input-group-append">
						<button type="button" class="btn btn-primary deviceadd">Hozzáad</button>
					</div>
				</div>
			</div>
		</div>
		<div class="form-row mt-1">
			<div class="col-12">
				<ul class="list-group">
					@if(old('imei'))
                        @for( $i =0; $i < count(old('imei')); $i++)
					<li class="list-group-item">
						<div class="row">
							<input type="hidden" name="tipus[]" value="{{ old('tipus.'.$i) }}">
							<input type="hidden" name="imei[]" value="{{ old('imei.'.$i) }}">
							<input type="hidden" name="imei2[]" value="{{ old('imei2.'.$i) }}">
							<input type="hidden" name="ertek[]" value="{{ old('ertek.'.$i) }}">
							<input type="hidden" name="leltari[]" value="{{ old('leltari.'.$i) }}">
							<span class="col-sm-4">{{ \App\Tipus::select('gyarto','tipus')->where('id',old('tipus.'.$i))->first()->tipus }}</span>
							<span class="col-6 col-sm-3">IMEI: {{ old('imei.'.$i) }}</span>
							<span class="col-6 col-sm-3">IMEI2: {{ old('imei2.'.$i) }}</span>
							<span class="col-sm-2"><span class="fas fa-barcode" title="Leltári szám: {{ old('leltari.'.$i) }}"></span> érték.: {{ old('ertek.'.$i) }}</span>
							<button type="button" class="close">&times;</button>
						</div>
					</li>
                        @endfor
                    @endif

				</ul>
			</div>
		</div>
        <br>
		<div class="d-flex">
			<button type="submit" class="btn btn-primary" >Rögzítés</button>
			<button type="button" class="btn btn-secondary" onclick="window.location='{{ route('devices.index')}}'">Mégse</button>
			<div class="fileInput">
				<input type="file" id="txtFileUpload" accept=".csv">
				<button type="button" class="btn btn-warning btn-block" >CSV import</button>
			</div>
		</div>
    </form>

</div>
<div class="modal fade" id="import-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Import varázsló</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-12">
						<h4>Megfelel az alábbi oszlop elrendezés?</h4>
					</div>
				</div>
				<div class="fl-table mt-1">
					<table class='table table-striped table-bordered table-sm'>
						<thead><tr><th>típus</th><th>IMEI/SN/FKF-leltári</th><th>leltári szám</th><th>ár</th><th>dátum</th><th>beszállító</th><th>számlaszám</th><th>tsz</th><th>beruhid</th></tr></thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Mégse</button>
				<button type="button" class="btn btn-primary editSave">Igen, mentem!</button>
			</div>
		</div>
	</div>
</div>

@endsection