@extends('layouts.app')

@section('title', 'Mobileszközök')

@section('devices', 'active')
@section('db', 'active')

@push('head')
	<script src="{{ asset('js/pagination.js') }}" defer></script>
	<style>
		.pagination{
			margin: 0 !important;
		}
	</style>
@endpush

@section('content')

<div class="col-xl-8 offset-xl-2">
    <h1><i class="fas fa-mobile-alt"></i> Mobil eszközök</h1>
    <hr>
	<div class="d-flex justify-content-between">
		@can('Mobil eszközök szerkesztése')<a href="{{ route('devices.create') }}" class="btn btn-success mb-2">Új eszköz hozzáadása</a>@endcan
		<a href="{{ route('types.index') }}" class="btn btn-success mb-2">Eszköz típusok</a>
	</div>
    <div class="table-responsive filtertable">
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
					<th><form class="form-inline"><label>Típus (gyártó nélkül)</label><input type="search" class="form-control form-control-sm ml-auto ajax-filter" data-column="_tipus_tipus" placeholder="Szűkítés..." /></form>
						<small>Ha nem elejétől keresel, kezdd %-kal! (pl: %S10)</small>
					</th>
                    <th><form><label>IMEI/SN/FKF-leltári</label><input type="search" class="form-control form-control-sm ml-auto ajax-filter" data-column="imei" placeholder="Szűkítés..." /></form></th>
					<th>Státusz</th>
					<th><form class="form-inline"><label>felhasználó</label><input type="search" class="form-control form-control-sm ml-auto ajax-filter" data-column="_felhasznalo_nev" placeholder="Szűkítés..." /></form></th>
                    <th>Műveletek</th>
                </tr>
            </thead>
			<tbody>
				@include('devices/pagination_data')
            </tbody>

        </table>
		<input type="hidden" name="hidden_page" id="hidden_page" value="1" />
		<input type="hidden" name="hidden_url" id="hidden_url" value="devices" />
		<input type="hidden" name="hidden_column" id="hidden_column" value="imei" />
		<input type="hidden" name="hidden_query" id="hidden_query" value="" />
    </div>
</div>

@endsection