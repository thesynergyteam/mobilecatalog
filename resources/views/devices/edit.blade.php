@extends('layouts.app')

@section('title', 'Eszköz szerkesztése')

@section('devices', 'active')
@section('db', 'active')

@section('content')

<div class='col-xl-6 col-md-8 offset-xl-3 offset-md-2'>

    <h1><i class='fa fa-mobile-alt'></i> Mobil eszköz részletek </h1>
    <br>
    <form action="{{ route('devices.update', $device->id) }}" method="POST">
        @method('PUT')
        @csrf
		<div class="row">
			<div class="col-md-6 form-group">
				<label for="tipus">Eszköz típus</label>
				<input type="text" name="tipus" class="form-control" value="{{$device->tipus->nev}}" readonly="readonly">
			</div>
			<div class="col-md-6 form-group">
				<label for="leltariszam">Leltáriszám</label>
				<input type="text" name="leltariszam" class="form-control" value="{{$device->leltariszam}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 form-group">
				<label for="imei">IMEI/SN/FKF-leltári</label>
				<input type="text" name="imei" class="form-control" value="{{$device->IMEI}}" readonly="readonly">
			</div>
			<div class="col-md-6 form-group">
				<label for="imei2">IMEI2</label>
				<input type="text" name="imei2" class="form-control" value="{{$device->IMEI2}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 form-group">
				<label for="beszallito">Beszállító</label>
				<input type="text" name="beszallito" class="form-control" value="{{$device->beszallito->partner}}" readonly="readonly">
			</div>
			<div class="col-md-6 form-group">
				<label for="ertek">Érték (nettó)</label>
				<input type="text" name="ertek" class="form-control" value="{{ $device->ertek }}" readonly="readonly">
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 form-group">
				<label for="szamlaszam">Számlaszám</label>
				<input type="text" name="szamlaszam" class="form-control" value="{{$device->szamlaszam}}" readonly="readonly">
			</div>
			<div class="col-md-6 form-group">
				<label for="datum">Dátum</label>
				<input type="text" name="datum" class="form-control" value="{{ $device->datum }}" readonly="readonly">
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 form-group">
				<label for="gyartasiszam">Gyártási szám</label>
				<input type="text" name="gyartasiszam" class="form-control" value="{{$device->gyartasiszam}}">
			</div>
			<div class="col-md-4 form-group">
				<label for="statusz">Státusz</label>
				@can('Mobil eszközök szerkesztése')
				<select class="custom-select" name="statusz">
					@foreach(\App\Statusz::all() as $statusz)
					<option value="{{$statusz->id}}" @if($device->statusz_id==$statusz->id) selected @endif>{{$statusz->nev}}</option>
					@endforeach
				</select>
				@else
				<input type="text" name="statusz_nev" class="form-control" value="{{$device->statusz->nev}}" readonly="">
				<input type="hidden" name="statusz" value="{{ $device->statusz_id }}">
				@endcan
			</div>
			<div class="col-md-4 form-group">
				<label for="unev">Felhasználó</label>
				<input type="text" name="nev" class="form-control" value="{{$device->felhasznalo->nev}}" readonly="">
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 form-group">
				<label for="beruhazasi_id">Beruházási azonosító</label>
				<input type="text" name="beruhazasi_id" class="form-control" value="{{$device->beruhazasi_id}}" @cannot('Mobil eszközök szerkesztése')readonly="readonly"@endcannot>
			</div>
		</div>
        <br>
        @can('Mobil eszközök szerkesztése')<button type="submit" class="btn btn-primary" >Mentés</button>@endcan
        <button type="button" class="btn btn-secondary" onclick="window.location='{{ route('devices.index')}}'">Vissza</button>

    </form>

</div>

@endsection