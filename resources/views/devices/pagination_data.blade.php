<tr class="bg-transparent">
	<td colspan="5" class="py-0">
		<div class="d-flex align-items-center">
			<span class="m-3">Találat: {{ $devices->total() }}</span> {!! $devices->links() !!}
		</div>
	</td>
</tr>
@foreach ($devices as $device)
<tr>
	<td>{{ $device->tipus->nev }}</td>
	<td>{{ $device->IMEI }}<br>{{ $device->IMEI2?$device->IMEI2:'' }}</td>
	<td>{{ $device->statusz->nev }}</td>
	<td>{{ $device->felhasznalo->nev_tsz }}</td>
	<td>
	<a href="{{ route('devices.edit', $device->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Részletek</a>
	</td>
</tr>
@endforeach
<tr class="bg-transparent">
	<td colspan="5">
	{!! $devices->links() !!}
	</td>
</tr>
