@extends('layouts.app')

@section('title', 'Új eszköz hozzáadása')

@section('devices', 'active')
@section('db', 'active')

@push('head')
	<link href="{{ asset('css/bootstrap-datepicker3.min.css') }}"  rel="stylesheet">
	<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" defer></script>
	<script src="{{ asset('js/bootstrap-datepicker.hu.min.js') }}" defer></script>
@endpush

@section('content')

<div class='col-xl-6 col-md-8 offset-xl-3 offset-md-2'>

    <h1><i class='fas fa-mobile-alt'></i> Új eszköz hozzáadása</h1>
    <br>

    <form action="{{ url('devices') }}" method="POST">
        @csrf
		<div class="row">
			<div class="col-md-4 form-group">
				<label for="tipus">Eszköz típus</label>
				<select class="custom-select" name="tipus">
					<option value="" selected disabled>Válassz típust!</option>
					@foreach(\App\Tipus::orderBy('gyarto')->orderBy('tipus')->get() as $tipus)
					<option value="{{$tipus->id}}" {{ old('tipus')==$tipus->id?'selected':'' }}>{{$tipus->nev}}</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-3 form-group">
				<label for="imei">IMEI/SN/FKF-leltári</label>
				<input type="text" name="imei" class="form-control" value="{{old('imei')}}"  maxlength="15">
			</div>
			<div class="col-md-3 form-group">
				<label for="imei2">IMEI2</label>
				<input type="text" name="imei2" class="form-control" value="{{old('imei2')}}"  maxlength="15" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
			</div>
			<div class="col-md-2 form-group">
				<label for="gyartasiszam">Szériaszám</label>
				<input type="text" name="gyartasiszam" class="form-control" value="{{old('gyartasiszam')}}" >
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 form-group">
				<label for="beszallito">Beszállító</label>
				<select class="custom-select" name="beszallito">
					<option value="" selected disabled>Válassz beszállítót!</option>
					@foreach(\App\Beszallito::all() as $besz)
					<option value="{{$besz->id}}" {{ old('beszallito')==$besz->id?'selected':'' }}>{{$besz->partner}} ({{$besz->kontakt}})</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-4 form-group">
				<label for="szamlaszam">Számlaszám</label>
				<input type="text" name="szamlaszam" class="form-control" value="{{old('szamlaszam')}}" >
			</div>
			<div class="col-md-3 form-group">
				<label for="leltariszam">Leltáriszám</label>
				<input type="text" name="leltariszam" class="form-control" value="{{old('leltariszam')}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 form-group">
				<label for="ertek">Érték (nettó)</label>
				<input type="text" name="ertek" class="form-control" value="{{ old('ertek') }}"  maxlength="6" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
			</div>
			<div class="col-md-4 form-group">
				<label for="datum">Vásárlás dátuma</label>
				<input type="text" name="datum" class="form-control" data-provide="datepicker" data-date-days-of-week-highlighted="6,0" data-date-today-highlight="true" data-date-max-view-mode="2" data-date-language="hu" data-date-autoclose="true" data-date-format="yyyy.mm.dd" value="{{ old('datum') }}" readonly="">
			</div>
			<div class="col-md-4 form-group">
				<label for="statusz">Státusz</label>
				<select class="custom-select" name="statusz">
					@foreach(\App\Statusz::all() as $stat)
					<option value="{{$stat->id}}" {{ old('statusz')==$stat->id?'selected':'' }}>{{$stat->nev}}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 form-group">
				<label for="beruhazasi_id">Beruházási azonosító</label>
				<input type="text" name="beruhazasi_id" class="form-control" value="{{old('beruhazasi_id')}}" >
			</div>
		</div>
        <br>
        <button type="submit" class="btn btn-primary" >Hozzáadás</button>
        <button type="button" class="btn btn-secondary" onclick="window.location='{{ route('devices.index')}}'">Mégse</button>

    </form>

</div>

@endsection