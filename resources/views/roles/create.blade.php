@extends('layouts.app')

@section('title', 'Szerepkör létrehozása')

@section('content')

<div class='col-md-6 offset-md-3 col-sm-8 offset-sm-2'>

    <h1><i class='fa fa-key'></i> Szerepkör létrehozása</h1>
    <hr>

    <form action="{{ url('roles') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Név</label>
            <input type="text" name="name" class="form-control" value="{{old('name')}}">
        </div>

        <h5><b>Jogosultság hozzárendelése</b></h5>

        <div class='form-group'>
            @foreach ($permissions as $permission)
                <input type="checkbox" name="permissions[]" value="{{$permission->id}}" >
                <label for="{{$permission->name}}">{{ucfirst($permission->name)}}</label><br>

            @endforeach
        </div>

        <button type="submit" class="btn btn-primary" >Hozzáadás</button>
        <button type="button" class="btn btn-secondary" onclick="window.location='{{ route('roles.index')}}'">Mégse</button>

    </form>

</div>

@endsection