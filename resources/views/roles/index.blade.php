@extends('layouts.app')

@section('title', 'Szerepkörök')

@push('head')
	<script src="{{ asset('js/sorting.js') }}" defer></script>
@endpush

@section('content')

<div class="col-xl-8 offset-xl-2">
    <h1><i class="fa fa-key"></i> Szerepkörök
    <a href="{{ route('users.index') }}" class="btn btn-outline-secondary pull-right">Felhasználók</a>
    <a href="{{ route('permissions.index') }}" class="btn btn-outline-secondary pull-right">Jogosultságok</a></h1>
    <hr>
    <a href="{{ URL::to('roles/create') }}" class="btn btn-success mb-2">Új szerepkör hozzáadás</a>
    <div class="table-responsive filtertable">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Szerepkör</th>
                    <th><form class="form-inline"><label>Jogosultság</label><input type="search" class="form-control form-control-sm ml-auto txt-filter" data-filter="f-jog" placeholder="Szűkítés..." /></form></th>
                    <th width="15%">Művelet</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($roles as $role)
                <tr>

                    <td>{{ $role->name }}</td>

                    <td class='f-jog'>{{ str_replace(array('[',']','"'),'', $role->permissions()->pluck('name')->toJson(JSON_UNESCAPED_UNICODE)) }}</td>{{-- Retrieve array of permissions associated to a role and convert to string --}}
                    <td>
                    <a href="{{ URL::to('roles/'.$role->id.'/edit') }}" class="btn btn-info pull-left" style="margin-right: 3px;">Szerkeszt</a>

                    <form action="{{ route('roles.destroy', $role->id) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger" >Töröl</button>
                    </form>
                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>
</div>

@endsection