@extends('layouts.app')

@section('title', 'Szerepkör szerkesztése')

@section('content')

<div class='col-md-6 offset-md-3 col-sm-8 offset-sm-2'>
    <h1><i class='fa fa-key'></i> Szerepkör szerkesztése: {{$role->name}}</h1>
    <hr>

    <form action="{{ route('roles.update', $role->id) }}" method="POST">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="name">Szerepkör neve</label>
            <input type="text" name="name" class="form-control" value="{{ $role->name }}">
        </div>

        <h5><b>Jogosultság hozzárendelése</b></h5>
        @foreach ($permissions as $permission)
            <input type="checkbox" name="permissions[]" value="{{$permission->id}}" {{ $role->permissions->contains('id',$permission->id )?'checked':''}}>
            <label for="{{$permission->name}}">{{ucfirst($permission->name)}}</label><br>
        @endforeach
        <br>
        <button type="submit" class="btn btn-primary" >Mentés</button>
        <button type="button" class="btn btn-secondary" onclick="window.location='{{ route('roles.index')}}'">Mégse</button>

    </form>
</div>

@endsection