@extends('layouts.app')

@section('title', 'Kiegészítők')

@section('accessories', 'active')
@section('db', 'active')

@push('head')
	<script src="{{ asset('js/sorting.js') }}" defer></script>
@endpush

@section('content')

<div class="col-xl-8 offset-xl-2">
    <h1><i class="fas fa-keyboard"></i> Kiegészítő eszközök</h1>
    <hr>
	<div class="d-flex justify-content-between">
		<a href="{{ route('accessories.create') }}" class="btn btn-success mb-2">Új eszköz hozzáadása</a>
	</div>
	{{ $accessories->links() }}
    <div class="table-responsive filtertable">
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
					<th><form class="form-inline"><label>Név</label><input type="search" class="form-control form-control-sm ml-auto txt-filter" data-filter="f-tipus" placeholder="Szűkítés..." /></form></th>
                    <th><form class="form-inline"><label>Leltári szám</label><input type="search" class="form-control form-control-sm ml-auto txt-filter" data-filter="f-lsz" placeholder="Szűkítés..." /></form></th>
					<th><form class="form-inline"><label>felhasználó</label><input type="search" class="form-control form-control-sm ml-auto txt-filter" data-filter="f-user" placeholder="Szűkítés..." /></form></th>
                    <th>Műveletek</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($accessories as $accessory)
                <tr>
                    <td class='f-tipus'>{{ $accessory->nev }}<br>{{ $accessory->megjegyzes?$accessory->megjegyzes:'' }}</td>
                    <td class='f-lsz'>{{ $accessory->leltariszam }}</td>
                    <td class='f-user'>{{ $accessory->felhasznalo->nev_tsz }}</td>
                    <td>
                    <a href="{{ route('accessories.edit', $accessory->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Részletek</a>
                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>
</div>

@endsection