@extends('layouts.app')

@section('title', 'Kiegészítő szerkesztése')

@section('accessories', 'active')
@section('db', 'active')

@section('content')

<div class='col-xl-6 col-md-8 offset-xl-3 offset-md-2'>

    <h1><i class='fa fa-keyboard'></i> Kiegészítő eszköz részletek </h1>
    <br>
    <form action="{{ route('accessories.update', $accessory->id) }}" method="POST">
        @method('PUT')
        @csrf
		<div class="row">
			<div class="col-md-4 form-group">
				<label for="nev">Eszköz neve</label>
				<input type="text" name="nev" class="form-control" value="{{$accessory->nev}}">
			</div>
			<div class="col-md-3 form-group">
				<label for="leltariszam">Leltáriszám</label>
				<input type="text" name="leltariszam" class="form-control" value="{{$accessory->leltariszam}}"  maxlength="6" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
			</div>
			<div class="col-md-5 form-group">
				<label for="beszallito">Beszállító</label>
				<input type="text" name="beszallito" class="form-control" value="{{$accessory->beszallito->partner}}  ({{$accessory->beszallito->kontakt}})" readonly="readonly">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 form-group">
				<label for="megjegyzes">Megjegyzés</label>
				<input type="text" name="megjegyzes" class="form-control" value="{{$accessory->megjegyzes}}" >
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 form-group">
				<label for="ertek">Érték (nettó)</label>
				<input type="text" name="ertek" class="form-control" value="{{ $accessory->ertek }}" readonly="readonly">
			</div>
			<div class="col-md-4 form-group">
				<label for="datum">Vásárlás dátuma</label>
				<input type="text" name="datum" class="form-control" value="{{ $accessory->datum }}" readonly="readonly">
			</div>
			<div class="col-md-4 form-group">
				<label for="statusz">Státusz</label>
				@can('Kiegészítő eszközök szerkesztése')
				<select class="custom-select" name="statusz">
					@foreach(\App\Statusz::all() as $stat)
					<option value="{{$stat->id}}" @if($accessory->statusz_id==$stat->id) selected @endif>{{$stat->nev}}</option>
					@endforeach
				</select>
				@else
				<input type="text" name="statusz_nev" class="form-control" value="{{$accessory->statusz->nev}}" readonly="">
				<input type="hidden" name="statusz" value="{{ $accessory->statusz_id }}">
				@endcan
			</div>
		</div>
        <br>
        <button type="submit" class="btn btn-primary" >Mentés</button>
        <button type="button" class="btn btn-secondary" onclick="window.location='{{ route('accessories.index')}}'">Vissza</button>

    </form>

</div>

@endsection