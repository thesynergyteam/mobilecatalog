@extends('layouts.app')

@section('title', 'Új kiegészítő hozzáadása')

@section('accessories', 'active')
@section('db', 'active')

@push('head')
	<link href="{{ asset('css/bootstrap-datepicker3.min.css') }}"  rel="stylesheet">
	<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" defer></script>
	<script src="{{ asset('js/bootstrap-datepicker.hu.min.js') }}" defer></script>
@endpush

@section('content')

<div class='col-xl-6 col-md-8 offset-xl-3 offset-md-2'>

    <h1><i class='fas fa-keyboard'></i> Új kiegészítő hozzáadása</h1>
    <br>

    <form action="{{ url('accessories') }}" method="POST">
        @csrf
		<div class="row">
			<div class="col-md-4 form-group">
				<label for="nev">Eszköz neve</label>
				<input type="text" id="nev" name="nev" class="form-control" value="{{old('nev')}}">
			</div>
			<div class="col-md-3 form-group">
				<label for="leltariszam">Leltáriszám</label>
				<input type="text" name="leltariszam" id="leltariszam" class="form-control" value="{{old('leltariszam')}}"  maxlength="6" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
			</div>
			<div class="col-md-5 form-group">
				<label for="beszallito">Beszállító</label>
				<select class="custom-select" name="beszallito" id="beszallito">
					<option value="" selected disabled>Válassz beszállítót!</option>
					@foreach(\App\Beszallito::all() as $besz)
					<option value="{{$besz->id}}" {{ old('beszallito')==$besz->id?'selected':'' }}>{{$besz->partner}} ({{$besz->kontakt}})</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 form-group">
				<label for="megjegyzes">Megjegyzés</label>
				<input type="text" name="megjegyzes" id="megjegyzes" class="form-control" value="{{old('megjegyzes')}}" >
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 form-group">
				<label for="ertek">Érték (nettó)</label>
				<input type="text" name="ertek" id="ertek" class="form-control" value="{{ old('ertek') }}"  maxlength="6" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
			</div>
			<div class="col-md-4 form-group">
				<label for="datum">Vásárlás dátuma</label>
				<input type="text" name="datum" id="datum" class="form-control" data-provide="datepicker" data-date-days-of-week-highlighted="6,0" data-date-today-highlight="true" data-date-max-view-mode="2" data-date-language="hu" data-date-autoclose="true" data-date-format="yyyy.mm.dd" value="{{ old('datum') }}" readonly="">
			</div>
			<div class="col-md-4 form-group">
				<label for="statusz_nev">Státusz</label>
				<input type="text" name="statusz_nev" id="statusz_nev" class="form-control" value="új-raktáron" readonly="">
				<input type="hidden" name="statusz" value="5">
			</div>
		</div>
        <br>
		<div class="d-flex justify-content-between">
			<div class="col-md-2 form-inline p-0">
				<label for="db">darabszám</label>
				<input type="text" name="db" id="db" class="col-4 form-control offset-1" value="{{ old('db')?old('db'):"1" }}" maxlength="2" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
			</div>
			<button type="submit" class="btn btn-primary">Hozzáadás</button>
			<button type="button" class="btn btn-secondary" onclick="window.location='https://mobile.fkf.hu/accessories'">Mégse</button>		
		</div>

    </form>

</div>

@endsection