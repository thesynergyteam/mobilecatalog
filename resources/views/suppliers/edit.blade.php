@extends('layouts.app')

@section('title', 'Beszállító')

@section('suppliers', 'active')
@section('db', 'active')

@section('content')

<div class='col-xl-6 col-md-8 offset-xl-3 offset-md-2'>

    <h1><i class='fas fa-users'></i> Beszállító részletek </h1>
    <br>
    <form action="{{ route('suppliers.update', $supplier->id) }}" method="POST">
        @method('PUT')
        @csrf
		<div class="row">
			<div class="col-md-6 form-group">
				<label for="partner">Partner</label>
				<input type="text" name="partner" class="form-control" value="{{$supplier->partner}}">
			</div>
			<div class="col-md-6 form-group">
				<label for="cim">Cím</label>
				<input type="text" name="cim" class="form-control" value="{{$supplier->cim}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 form-group">
				<label for="kontakt">Kontakt</label>
				<input type="text" name="kontakt" class="form-control" value="{{$supplier->kontakt}}">
			</div>
			<div class="col-md-3 form-group">
				<label for="email">E-mail</label>
				<input type="email" name="email" class="form-control" value="{{ $supplier->email }}">
			</div>
			<div class="col-md-3 form-group">
				<label for="telefon">Telefon</label>
				<input type="text" name="telefon" class="form-control" value="{{$supplier->telefon}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 form-group">
				<label for="megjegyzes">megjegyzes</label>
				<input type="text" name="megjegyzes" class="form-control" value="{{$supplier->megjegyzes}}">
			</div>
			<div class="col-md-2 form-group">
				<label for="aktiv">Aktív</label>
				<input type="checkbox" name="aktiv" class="form-control" value="1" {{ $supplier->aktiv?'checked':'' }}>
			</div>
		</div>
        <br>
        @can('Beszállítók szerkesztése')<button type="submit" class="btn btn-primary" >Mentés</button>@endcan
        <button type="button" class="btn btn-secondary" onclick="window.location='{{ route('suppliers.index')}}'">Vissza</button>
		@can('Beszállítók szerkesztése')<button type="button" class="btn btn-outline-primary float-right" onclick="window.location='{{ route('suppliers.history', $supplier->id)}}'">Előzmények</button>@endcan

    </form>

</div>

@endsection