@extends('layouts.app')

@section('title', 'Beszállító előzményei')

@section('suppliers', 'active')
@section('db', 'active')

@section('content')

<div class="col-xl-8 offset-xl-2">
    <h1><i class="fas fa-users"></i> {{$history->first()->partner}} beszállító előzményei</h1>
    <hr>
    <a href="{{ route('suppliers.edit', $history->first()->id) }}" class="btn btn-success mb-2">Vissza</a>
    <div class="table-responsive filtertable">
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
					<th>Partner</th>
                    <th>Cím</th>
					<th>Kontakt</th>
                    <th>Email</th>
					<th>Telefon</th>
					<th>Aktív</th>
					<th>Módosítás</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($history as $hist)
                <tr>
                    <td>{{ $hist->partner }}</td>
                    <td>{{ $hist->cim }}</td>
                    <td>{{ $hist->kontakt }}</td>
					<td>{{ $hist->email }}</td>
					<td>{{ $hist->telefon }}</td>
					<td>{{ $hist->aktiv?'aktív':'inaktív' }}</td>
					<td>{{ $hist->mod_datum }}<br>{{ $hist->name }}</td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>
</div>

@endsection