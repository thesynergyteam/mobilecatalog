@extends('layouts.app')

@section('title', 'Beszállító hozzáadása')

@section('suppliers', 'active')
@section('db', 'active')

@section('content')

<div class='col-xl-6 col-md-8 offset-xl-3 offset-md-2'>

    <h1><i class='fas fa-users'></i> Beszállító hozzáadása</h1>
    <br>

    <form action="{{ url('suppliers') }}" method="POST">
        @csrf
		<div class="row">
			<div class="col-md-6 form-group">
				<label for="partner">Partner</label>
				<input type="text" name="partner" class="form-control" value="{{old('partner')}}">
			</div>
			<div class="col-md-6 form-group">
				<label for="cim">Cím</label>
				<input type="text" name="cim" class="form-control" value="{{old('cim')}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 form-group">
				<label for="kontakt">Kontakt</label>
				<input type="text" name="kontakt" class="form-control" value="{{old('kontakt')}}">
			</div>
			<div class="col-md-3 form-group">
				<label for="email">E-mail</label>
				<input type="email" name="email" class="form-control" value="{{ old('email') }}">
			</div>
			<div class="col-md-3 form-group">
				<label for="telefon">Telefon</label>
				<input type="text" name="telefon" class="form-control" value="{{ old('telefon')}}">
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 form-group">
				<label for="megjegyzes">megjegyzes</label>
				<input type="text" name="megjegyzes" class="form-control" value="{{old('megjegyzes')}}">
			</div>
			<div class="col-md-2 text-center form-group">
				<label for="aktiv">Aktív</label>
				<input type="checkbox" name="aktiv" class="form-control" value="1" checked="">
			</div>
		</div>
        <br>
        <button type="submit" class="btn btn-primary" >Hozzáadás</button>
        <button type="button" class="btn btn-secondary" onclick="window.location='{{ route('suppliers.index')}}'">Mégse</button>

    </form>

</div>

@endsection