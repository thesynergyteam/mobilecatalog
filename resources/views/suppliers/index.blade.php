@extends('layouts.app')

@section('title', 'Beszállítók')

@section('suppliers', 'active')
@section('db', 'active')

@push('head')
	<script src="{{ asset('js/sorting.js') }}" defer></script>
@endpush

@section('content')

<div class="col-xl-8 offset-xl-2">
    <h1><i class="fas fa-users"></i> Beszállítók</h1>
    <hr>@can('Beszállítók szerkesztése')
    <a href="{{ route('suppliers.create') }}" class="btn btn-success mb-2">Új beszállító hozzáadása</a>@endcan
    <div class="table-responsive filtertable">
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
					<th><form class="form-inline"><label>Partner</label><input type="search" class="form-control form-control-sm ml-auto txt-filter" data-filter="f-partner" placeholder="Szűkítés..." /></form></th>
                    <th><form class="form-inline"><label>Cím</label><input type="search" class="form-control form-control-sm ml-auto txt-filter" data-filter="f-cim" placeholder="Szűkítés..." /></form></th>
					<th><form class="form-inline"><label>Kontakt</label><input type="search" class="form-control form-control-sm ml-auto txt-filter" data-filter="f-kontakt" placeholder="Szűkítés..." /></form></th>
                    <th>Műveletek</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($suppliers as $supplier)
                <tr class="{{$supplier->aktiv?'':'text-danger'}}">
                    <td class='f-partner'>{{ $supplier->partner }}</td>
                    <td class='f-cim'>{{ $supplier->cim }}</td>
                    <td class='f-kontakt'>{{ $supplier->kontakt }}</td>
                    <td>
                    <a href="{{ route('suppliers.edit', $supplier->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Részletek</a>
                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>
</div>

@endsection