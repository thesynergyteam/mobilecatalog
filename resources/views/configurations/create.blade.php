@extends('layouts.app')

@section('title', 'Konfiguráció létrehozása')

@section('configurations', 'active')
@section('transactions', 'active')

@push('head')
	<link href="{{ asset('css/bootstrap-datepicker3.min.css') }}"  rel="stylesheet">
	<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" defer></script>
	<script src="{{ asset('js/bootstrap-datepicker.hu.min.js') }}" defer></script>
	<script src="{{ asset('js/konfiguracio.js') }}" defer></script>
	<style>
		.cursor-pointer {
			cursor: pointer;
		}
		#searchResult{
			list-style: none;
			padding: 0px 15px;
			background: white;
			border: 1px solid #aaa;
			position: absolute;
			margin: 0;
			top: 37px;
			left: 35px;
			border: 1px solid #ced4da;
			border-top: 0;
			z-index: 1;
			border-bottom-right-radius: 4px;
			border-bottom-left-radius: 4px;
		}

		#searchResult li{
			padding: 4px;
			margin-bottom: 1px;
		}

		#searchResult li:hover{
			cursor: pointer;
			background-color: #c8f6fb;
		}
	</style>
@endpush

@section('content')

<div class='col-xl-6 col-md-8 offset-xl-3 offset-md-2'>

	<h1><i class='fas fa-cogs'></i> Konfiguráció létrehozása</h1>
	<br>

	<form action="{{ url('configurations') }}" method="POST">
		@csrf
		<div class="row">
			<div class="col-md-3 form-group">
				<label for="tsz">Törzsszám</label>
				<div class="input-group">
					<input type="text" name="tsz" id="tsz" class="form-control" value="{{old('tsz')}}" maxlength="6" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
					<div class="input-group-append">
						<button type="button" class="btn btn-primary tszkeres">Keresés</button>
					</div>
				</div>
			</div>
			<div class="col-md-6 form-group">
				<label for="usernev_full">Felhasználó</label>
				<input type="text" name="usernev_full" id="usernev_full" class="form-control" value="{{ old('usernev_full') }}" readonly="">
				<input type="hidden" name="usernev" id="usernev" value="{{ old('usernev') }}" readonly="">
			</div>
			<div class="col-md-3 form-group">
				<label for="kategoria">Kategória</label>
				<select class="custom-select" name="kategoria">
					<option value="" selected disabled>Válassz kategoriát!</option>
					@foreach(\App\Kategoria::all() as $kat)
					<option value="{{$kat->id}}" @if($kat->id==old('kategoria')) selected @endif>{{$kat->id}} - {{$kat->nev}}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-12 eszkozmezo">
				<label for="eszkoz">Mobileszközök</label>
				<button type="button" class="btn btn-primary float-right mb-2 newdeviceadd">Hozzáadás</button>
				@if(old('eszkoz_id'))
                    @for( $i =0; $i < count(old('eszkoz_id')); $i++)
					<div class="input-group">
						<input type="text" class="form-control" readonly="" value="{{ \App\Mobileszkoz::where('id',old('eszkoz_id.'.$i))->first()->tipus->nev}} ({{\App\Mobileszkoz::where('id',old('eszkoz_id.'.$i))->first()->IMEI}})">
						<input type="hidden" name="eszkoz_id[]" value="{{ old('eszkoz_id.'.$i) }}">
						<div class="input-group-append">
							<button type="button" class="btn btn-danger delete">Töröl</button>
						</div>
					</div>
                    @endfor
                @endif
			</div>
		</div>
		<hr>
		<div class="row mb-2">
			<div class="col-12 simmezo">
				<label for="eszkoz">SIM kártyák</label>
				<button type="button" class="btn btn-primary float-right mb-2 newsimadd">Hozzáadás</button>
				@if(old('sim_id'))
                    @for( $i =0; $i < count(old('sim_id')); $i++)
					<div class="input-group">
						<input type="text" class="form-control" readonly="" value="{{ \App\Sim::where('id',old('sim_id.'.$i))->first()->icc}} ({{\App\Sim::where('id',old('sim_id.'.$i))->first()->telefonszam}})">
						<input type="hidden" name="sim_id[]" value="{{ old('sim_id.'.$i) }}">
						<div class="input-group-append">
							<button type="button" class="btn btn-danger delete">Töröl</button>
						</div>
					</div>
                    @endfor
                @endif
			</div>
		</div>
		<hr>
		<div class="form-row mt-1">
			<div class="col-12">
				<label for="eszkoz">Kiegészítők</label>
				<ul class="list-group">
					@if(old('kieg_eszkoz_id'))
                        @for( $i =0; $i < count(old('kieg_eszkoz_id')); $i++)
					<li class="list-group-item">
						<div class="row">
							<input type="hidden" name="kieg_eszkoz_id[]" value="{{ old('kieg_eszkoz_id.'.$i) }}">
							<span class="col-11">{{ \App\Accessory::where('id',old('kieg_eszkoz_id.'.$i))->first()->nev }}</span>
							<button type="button" class="close">&times;</button>
						</div>
					</li>
                        @endfor
                    @endif

				</ul>
			</div>
		</div>
		<div class="form-row">
			<div class="col-12">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text"> + </span>
					</div>
					<input type="text" name="kieg_eszkoz_nev" id="kieg_eszkoz_nev" class="form-control" placeholder="Kiegészítő hozzáadása">
					<ul id="searchResult"></ul>
					<input type="hidden" name="kieg_eszkoz" id="kieg_eszkoz">
					<div class="input-group-append">
						<button type="button" class="btn btn-primary deviceadd">Hozzáad</button>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-8 form-group">
				<label for="megjegyzes">Megjegyzés</label>
				<input type="text" name="megjegyzes" class="form-control" value="{{old('megjegyzes')}}">
			</div>
			<div class="col-md-2 form-group">
				<label for="statusz">Típus</label>
				<select class="custom-select" name="statusz">
					<option value="kiadás" @if(old('statusz')=='kiadás') selected @endif>kiadás</option>
					<option value="visszavét" @if(old('statusz')=='visszavét') selected @endif>visszavét</option>
				</select>
			</div>
			<div class="col-md-2 form-group">
				<label for="datum">Kiadás dátuma</label>
				<input type="text" name="datum" class="form-control" data-provide="datepicker" data-date-days-of-week-highlighted="6,0" data-date-today-highlight="true" data-date-max-view-mode="2" data-date-language="hu" data-date-autoclose="true" data-date-format="yyyy.mm.dd" value="{{ old('datum')?substr(old('datum'),0,10):date('Y.m.d') }}" readonly="">
			</div>
		</div>


		<br>
		<button type="submit" class="btn btn-primary" >Létrehoz</button>
		<button type="submit" class="btn btn-primary" name="saveandedit">Létrehoz és szerkeszt</button>
		<button type="button" class="btn btn-secondary" onclick="window.location='{{ route('configurations.index')}}'">Mégse</button>

	</form>
	<div class="modal fade" id="dialog_device" tabindex="-1" role="dialog" >
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Mobil eszköz keresése</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Bezár"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 form-group">
							<label for="search_imei">Keresés IMEI/SN/FKF-leltári alapján</label>
							<div class="input-group">
								<input type="text" name="search_imei" id="search_imei" class="form-control" placeholder="Rövidített keresés, pl: %0966" value="{{old('search_imei')}}" maxlength="15">
								<div class="input-group-append">
									<button type="button" class="btn btn-primary imeikeres">Keresés</button>
								</div>
							</div>
						</div>
						<div class="col-md-12 form-group">
							<label for="eredmeny">Eredmény</label>
							<textarea id="eredmeny" rows="4" class="form-control" readonly=""></textarea>
							<input type="hidden" id="eredmeny_id">
							<input type="hidden" id="eredmeny_text">
						</div>
						<div class="mx-3 alert alert-danger d-none" role="alert" id="warning">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary d-none">Kiválaszt</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Mégse</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="dialog_sim_message" tabindex="-1" role="dialog" >
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">SIM kártya keresése</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Bezár"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 form-group">
							<label for="search_sim">Keresés ICC /telefonszám alapján</label>
							<div class="input-group">
								<input type="text" name="search_sim" id="search_sim" class="form-control" placeholder="Rövidített keresés, pl: %42644" value="{{old('search_sim')}}" maxlength="15">
								<div class="input-group-append">
									<button type="button" class="btn btn-outline-primary simkeres" data-sim="icc">ICC keres</button>
									<button type="button" class="btn btn-outline-primary simkeres" data-sim="tel">Telszám keres</button>
								</div>
							</div>
						</div>
						<div class="col-md-12 form-group">
							<label for="eredmeny_sim">Eredmény</label>
							<textarea id="eredmeny_sim" rows="4" class="form-control" readonly=""></textarea>
							<input type="hidden" id="eredmeny_sim_id">
							<input type="hidden" id="eredmeny_sim_text">
						</div>
						<div class="mx-3 alert alert-danger d-none" role="alert" id="warning_sim">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary d-none">Kiválaszt</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Mégse</button>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection