<tr class="bg-transparent">
	<td colspan="5" class="py-0">
		<div class="d-flex align-items-center">
			<span class="m-3">Találat: {{ $configurations->total() }}</span> {!! $configurations->links() !!}
		</div>
	</td>
</tr>
@foreach ($configurations as $configuration)
<tr>
	<td><span class="far fa-lg {{$configuration->statusz=='kiadás'?'fa-arrow-alt-circle-up':'fa-arrow-alt-circle-down text-danger'}}"></span> {{$configuration->datum}}</td>
	<td><ul class="pl-3 mb-0">@foreach ($configuration->eszkoz as $eszkoz)<li>{{$eszkoz->tipus->nev}}</li>@endforeach
			@foreach ($configuration->kiegeszito as $kiegeszito)<li>{{$kiegeszito->nev}}</li>@endforeach</ul></td>
	<td><ul class="pl-3 mb-0">@foreach ($configuration->sim as $sim)<li>{{ $sim->telefonszam }}</li> @endforeach</ul></td>
	<td class='f-user'>{{ $configuration->felhasznalo->nev_tsz }}</td>
	<td>
	<a href="{{ route('configurations.edit', $configuration->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Részletek</a>
	</td>
</tr>
@endforeach
<tr class="bg-transparent">
	<td colspan="5">
	{!! $configurations->links() !!}
	</td>
</tr>
