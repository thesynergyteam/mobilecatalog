@extends('layouts.app')

@section('title', 'Konfiguráció előzményei')

@section('configurations', 'active')
@section('transactions', 'active')

@section('content')

<div class="col-xl-8 offset-xl-2">
    <h1><i class="fas fa-cogs"></i> Konfiguráció előzményei</h1>
	@if($history)
	<h4>@if($history->first()->konfiguracio->eszkoz->count()>0){{$history->first()->konfiguracio->eszkoz->first()->tipus->nev}},@endif
		@if($history->first()->konfiguracio->sim->count()>0){{ $history->first()->konfiguracio->sim->first()->icc_tel}},@endif
		 {{$history->first()->konfiguracio->felhasznalo->nev_tsz}}</h4>
    <hr>
    <a href="{{ route('configurations.edit', $history->first()->konfiguracio_id) }}" class="btn btn-success mb-2">Vissza</a>
    <div class="table-responsive filtertable">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
					<th>Kategória</th>
                    <th>Megjegyzés</th>
                    <th>Státusz</th>
                    <th>Módosítás</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($history as $hist)
                <tr>
                    <td>{{ $hist->kategoria->nev }}</td>
                    <td>{{ $hist->megjegyzes }}</td>
                    <td class="text-center {{ empty($hist->statusz)?'text-muted small':''}}">{{ !empty($hist->statusz)?$hist->statusz:'--előző állapot--' }}</td>
                    <td>{{ $hist->mod_datum }}<br>{{ $hist->moduser->name }}</td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>
	@else
	<h4>Előzmény nem található!</h4>
	@endif
</div>

@endsection