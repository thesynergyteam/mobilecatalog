@extends('layouts.app')

@section('title', 'Lekérdezések')

@section('queries', 'active')

@section('content')

<div class="col-xl-8 offset-xl-2">

	<form action="{{ route('configurations.query') }}" method="GET">
		@csrf
		<div class="d-flex flex-wrap">
			<div class="form-inline ml-auto mr-auto ml-lg-0">
				<h4><i class="fas fa-user"></i> Mobiltelefon konfigurációk lekérdezése</h4>
			</div>
			<div class="form-inline ml-auto mr-auto mr-lg-0">
				<input type="text" name="imei" class="form-control" value="{{old('imei')}}" maxlength="15" placeholder="IMEI/SN/FKF-leltári">
				<button class="btn btn-info form-control">Keresés</button>
			</div>
			<div class="form-inline ml-auto mr-auto mr-lg-0">
				<input type="text" name="tel" class="form-control" value="{{old('tel')}}" maxlength="11" onkeypress="return event.charCode == 13 || (event.charCode >= 48 && event.charCode <= 57)" placeholder="Telefonszám: 36201234567">
				<button class="btn btn-info form-control">Keresés</button>
			</div>
		</div>
		<hr>
		<div class="d-flex flex-wrap">
			<div class="form-inline ml-auto mr-auto ml-md-0">
				<h4><i class="fas fa-user"></i> Felhasználó konfigurációk lekérdezése</h4>
			</div>
			<div class="form-inline ml-auto mr-auto mr-md-0">
				<input type="text" name="tsz" class="form-control" value="{{old('tsz')}}" maxlength="6" onkeypress="return event.charCode == 13 || (event.charCode >= 48 && event.charCode <= 57)" placeholder="Törzsszám">
				<button class="btn btn-info form-control">Keresés</button>
			</div>
		</div>
		<hr>
	</form>
@if($user && $results && $results->isNotEmpty())
	<h3 class="text-center">{{ $results[0]->felhasznalo->nev_tsz }}</h3>
	<h4>Nevén lévő tárgyak</h4>
	<ul style="list-style-type: none;padding:15px;">
	@foreach($devices as $device)
	<li><i class="fas fa-mobile-alt"></i> {{ $device->tipus->gyarto }} {{ $device->tipus->tipus}} {{$device->IMEI}}</li>
	@endforeach
	@foreach($sims as $sim)
	<li><i class="fas fa-sim-card"></i> {{ $sim->telefonszam }} {{ $sim->icc}}</li>
	@endforeach
	@foreach($accessories as $accessory)
	<li><i class="fas fa-headset"></i> {{ $accessory->nev }}</li>
	@endforeach
	</ul>
	@include('partials/szamhordozas')
	<h4>Konfigurációs lapok</h4>
	{{ $results->links() }}
	<table class="table table-bordered table-striped table-responsive-sm table-sm">
		<thead>
			<tr>
				<th>ID</th>
				<th>Dátum</th>
				<th>Eszköz</th>
				<th>Tel.szám</th>
				<th>Megjegyzés</th>
				<th>Műveletek</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($results as $result)
			<tr>
				<td>{{$result->id}} <span class="far fa-lg {{$result->statusz=='kiadás'?'fa-arrow-alt-circle-up':'fa-arrow-alt-circle-down text-danger'}}"></span></td>
				<td style="padding:.5rem.75rem;width:130px"> {{$result->datum}}</td>
				<td><ul class="pl-3 mb-0">@foreach ($result->eszkoz as $eszkoz)<li>{{$eszkoz->tipus->nev}} <small>{{ $eszkoz->IMEI}}</small></li>@endforeach
					@foreach ($result->kiegeszito as $kiegeszito)<li>{{$kiegeszito->nev}}</li>@endforeach</ul></td>
					<td><ul class="pl-3 mb-0">@foreach ($result->sim as $sim)<li>{{ $sim->telefonszam }} <small>{{$sim->icc}}</small></li> @endforeach</ul></td>
				<td>{{ $result->megjegyzes }}</td>
				<td style="width:117px">
				<a href="{{ route('configurations.edit', $result->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Részletek</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@endif
@if(!$user && $results && $results->isNotEmpty())
	@include('partials/szamhordozas')
	{{ $results->links() }}
	<h4>Konfigurációs lapok</h4>
	<table class="table table-bordered table-striped table-responsive-sm table-sm">
		<thead>
			<tr>
				<th>ID</th>
				<th>Dátum</th>
				<th>Felhasználó</th>
				<th>Eszköz</th>
				<th>Tel.szám</th>
				<th>Megjegyzés</th>
				<th>Műveletek</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($results as $result)
			<tr>
				<td>{{$result->id}} <span class="far fa-lg {{$result->statusz=='kiadás'?'fa-arrow-alt-circle-up':'fa-arrow-alt-circle-down text-danger'}}"></span></td>
				<td style="padding:.5rem.75rem;width:130px"> {{$result->datum}}</td>
				<td>{{ $result->felhasznalo->nev}}</td>
				<td><ul class="pl-3 mb-0">@foreach ($result->eszkoz as $eszkoz)<li>{{$eszkoz->tipus->nev}} <small>{{ $eszkoz->IMEI}}</small></li> @endforeach</ul></td>
				<td><ul class="pl-3 mb-0">@foreach ($result->sim as $sim)<li>{{ $sim->telefonszam }} <small>{{$sim->icc}}</small></li> @endforeach</ul></td>
				<td>{{ $result->megjegyzes }}</td>
				<td style="width:117px">
				<a href="{{ route('configurations.edit', $result->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Részletek</a>
				</td>
			</tr>
			@endforeach
		</tbody>

	</table>
@endif
</div>

@endsection