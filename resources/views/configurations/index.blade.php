@extends('layouts.app')

@section('title', 'Konfigurációk')

@section('configurations', 'active')
@section('transactions', 'active')

@push('head')
	<script src="{{ asset('js/pagination.js') }}" defer></script>
	<style>
		.pagination{
			margin: 0 !important;
		}
	</style>
@endpush

@section('content')

<div class="col-xl-8 offset-xl-2">
    <h1><i class="fas fa-cogs"></i> Konfigurációk</h1>
    <hr>
    <a href="{{ route('configurations.create') }}" class="btn btn-success mb-2">Új konfiguráció</a>
    <div class="table-responsive filtertable">
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
					<th>Dátum</th>
					<th>Eszköz</th>
					<th><form><label>Tel.szám</label><input type="search" class="form-control form-control-sm ml-auto ajax-filter" data-column="_sim_telefonszam" placeholder="Szűkítés..." /></form></th>
					<th><form class="form-inline"><label>Felhasználó</label><input type="search" class="form-control form-control-sm ml-auto ajax-filter" data-column="_felhasznalo_nev" placeholder="Szűkítés..." /></form></th>
                    <th>Műveletek</th>
                </tr>
            </thead>

            <tbody>
				@include('configurations/pagination_data')
            </tbody>

        </table>
		<input type="hidden" name="hidden_page" id="hidden_page" value="1" />
		<input type="hidden" name="hidden_url" id="hidden_url" value="configurations" />
		<input type="hidden" name="hidden_column" id="hidden_column" value="_sim_telefonszam" />
		<input type="hidden" name="hidden_query" id="hidden_query" value="" />
    </div>
</div>

@endsection