@extends('layouts.app')

@section('title', 'Konfiguráció')

@section('configurations', 'active')
@section('transactions', 'active')

@push('head')
	<link href="{{ asset('css/bootstrap-datepicker3.min.css') }}"  rel="stylesheet">
	<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" defer></script>
	<script src="{{ asset('js/bootstrap-datepicker.hu.min.js') }}" defer></script>
	<script src="{{ asset('js/konfiguracio.js') }}" defer type='text/javascript'></script>
	<style>
		#searchResult{
			list-style: none;
			padding: 0px 15px;
			background: white;
			border: 1px solid #aaa;
			position: absolute;
			margin: 0;
			top: 37px;
			left: 35px;
			border: 1px solid #ced4da;
			border-top: 0;
			z-index: 1;
			border-bottom-right-radius: 4px;
			border-bottom-left-radius: 4px;
		}

		#searchResult li{
			padding: 4px;
			margin-bottom: 1px;
		}

		#searchResult li:hover{
			cursor: pointer;
			background-color: #c8f6fb;
		}
	</style>
@endpush

@section('content')

<div class='col-xl-6 col-md-8 offset-xl-3 offset-md-2'>

	<h1><i class='fas fa-cogs'></i> Konfiguráció részletek </h1>
	<br>
	<form action="{{ route('configurations.update', $configuration->id) }}" method="POST" enctype="multipart/form-data">
		@method('PUT')
		@csrf
		<div class="row">
			<div class="col-md-3 form-group">
				<label for="tsz">Törzsszám</label>
				<input type="text" name="tsz" class="form-control" value="{{$configuration->felhasznalo->tsz}}" readonly="">
			</div>
			<div class="col-md-6 form-group">
				<label for="usernev">Felhasználó</label>
				<input type="text" name="usernev" class="form-control" value="{{ $configuration->felhasznalo->nev }}" readonly="">
				<input type="hidden" name="usernev_full" value="{{$configuration->felhasznalo->nev}}">
				<input type="hidden" name="felhasznalo_id" value="{{$configuration->felhasznalo_id}}">
			</div>
			<div class="col-md-3 form-group">
				<label for="kategoria">Kategória</label>
				@if($configuration->statusz=='visszavét')
				<input type="text" name="kategoria_text" class="form-control" value="{{$configuration->kategoria_id}} - {{$configuration->kategoria->nev}}" readonly="">
				<input type="hidden" name="kategoria" value="{{$configuration->kategoria_id}}">
				@else
				<select class="custom-select" name="kategoria">
					@foreach(\App\Kategoria::all() as $kat)
					<option value="{{$kat->id}}" @if($configuration->kategoria_id==$kat->id) selected @endif>{{$kat->id}} - {{$kat->nev}}</option>
					@endforeach
				</select>
				@endif
			</div>
		</div>
		<div class="row my-3">
			<div class="col-12 eszkozmezo">
				<label for="eszkoz">Mobileszközök</label>
			@can('Konfiguráció módosítása')
				<button type="button" class="btn btn-primary float-right mb-2 newdeviceadd">Hozzáadás</button>
			@endcan
				@foreach($configuration->eszkoz as $eszkoz)
				<div class="input-group">
					<input type="text" class="form-control" readonly="" value="{{$eszkoz->tipus->nev}} ({{$eszkoz->IMEI}})">
					<input type="hidden" name="eszkoz_id[]" value="{{$eszkoz->id}}">
					@can('Konfiguráció módosítása')
					<div class="input-group-append">
						<button type="button" class="btn btn-danger delete">Töröl</button>
					</div>
					@endcan
				</div>
				@endforeach
			</div>
		</div>
		<hr>
		<div class="row mb-2">
			<div class="col-12 simmezo">
				<label for="eszkoz">SIM kártyák</label>
			@can('Konfiguráció módosítása')
				<button type="button" class="btn btn-primary float-right mb-2 newsimadd">Hozzáadás</button>
			@endcan
				@foreach($configuration->sim as $sim)
				<div class="input-group">
					<input type="text" class="form-control" readonly="" value="{{$sim->icc}} ({{$sim->telefonszam}})">
					<input type="hidden" name="sim_id[]" value="{{$sim->id}}">
					@can('Konfiguráció módosítása')
					<div class="input-group-append">
						<button type="button" class="btn btn-danger delete">Töröl</button>
					</div>
					@endcan
				</div>
				@endforeach
			</div>
		</div>
		<hr>
		<div class="form-row mt-1">
			<div class="col-12">
				<label for="eszkoz">Kiegészítők</label>
				<ul class="list-group">
					@foreach($configuration->kiegeszito as $kieg)
					<li class="list-group-item">
						<div class="row">
							<span class="col-11">{{$kieg->nev}}</span> @can('Konfiguráció módosítása') <button type="button" class="close">&times;</button> @endcan
							<input type="hidden" name="kieg_eszkoz_id[]" value="{{ $kieg->id }}">
						</div>
					</li>
					@endforeach
				</ul>
			</div>
		</div>
		@can('Konfiguráció módosítása')
		<div class="form-row">
			<div class="col-12">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text"> + </span>
					</div>
					<input type="text" name="kieg_eszkoz_nev" id="kieg_eszkoz_nev" class="form-control" placeholder="Kiegészítő hozzáadása">
					<ul id="searchResult"></ul>
					<input type="hidden" name="kieg_eszkoz" id="kieg_eszkoz">
					<div class="input-group-append">
						<button type="button" class="btn btn-primary deviceadd">Hozzáad</button>
					</div>
				</div>
			</div>
		</div>
		@endcan
		<hr>
		<div class="row">
			<div class="col-md-8 form-group">
				<label for="megjegyzes">megjegyzes</label>
				<input type="text" name="megjegyzes" class="form-control" value="{{$configuration->megjegyzes}}">
			</div>
			<div class="col-md-2 form-group">
				<label for="statusz">Típus</label>
				<input type="text" name="statusz" class="form-control" value="{{$configuration->statusz}}" readonly="">
			</div>
			<div class="col-md-2 form-group">
				<label for="datum">Kiadás dátuma</label>
				<input type="text" name="datum" class="form-control" @can('Konfiguráció módosítása')data-provide="datepicker" @endcan data-date-days-of-week-highlighted="6,0" data-date-today-highlight="true" data-date-max-view-mode="2" data-date-language="hu" data-date-autoclose="true" data-date-format="yyyy.mm.dd" value="{{ old('datum')?:$configuration->datum }}" readonly="">
			</div>
		</div>
		<div class="row">
			<div class="col-md-9 form-group">
				<label for="dokumentum">Dokumentum</label>
				<input type="file" name="dokumentum" class="form-control-file">
			</div>
			<div class="col-md-3 form-group">
				<label for="letoltes">Feltöltött dokumentumok</label>
				<div class="d-flex flex-column">
				@foreach($configuration->fajlok as $fajl)
				<span><a href="/showfile/?fajlnev={{$fajl->fajlnev}}&utvonal={{$fajl->utvonal}}" target="_blank" title="{{$fajl->datum}} {{$fajl->moduser->name}}">{{$fajl->fajlnev}}</a>@can('Fájlok törlése')<a class="float-right" onclick='return confirm("Biztos vagy a fájl törlésben?\n{{$fajl->fajlnev}}")' href="/delfile/konfiguracio/{{$fajl->id}}">&times;</a> @endcan</span>
				@endforeach
				</div>
			</div>
		</div>
		<br>
		<div class="d-flex">
			<button type="submit" class="btn btn-primary" >Mentés</button>
			<button type="button" class="btn btn-secondary" onclick="window.location='{{ route('configurations.index')}}'">Vissza</button>
			@can('Konfiglap nyomtatás')
			<a class="btn btn-info ml-auto" href="{{ route('configurations.download',$configuration)}}" target='_blank'><i class="fa-print fas"></i> Konfiglap nyomtatás</a>
			@endcan
			@if($configuration->statusz=='kiadás')
			<button type="submit" class="btn btn-danger ml-auto" name="newreturnconfig">Visszavétel konfiglap készítése</button>
			@endif
			<button type="button" class="btn btn-outline-primary ml-auto" onclick="window.location='{{ route('configurations.history', $configuration->id)}}'">Előzmények</button>
		</div>

	</form>
	@can('Konfiguráció módosítása')
	<div class="modal fade" id="dialog_device" tabindex="-1" role="dialog" >
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Mobil eszköz keresése</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Bezár"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 form-group">
							<label for="search_imei">Keresés IMEI/SN/FKF-leltári alapján</label>
							<div class="input-group">
								<input type="text" name="search_imei" id="search_imei" class="form-control" placeholder="Rövidített keresés, pl: %0966" maxlength="15">
								<div class="input-group-append">
									<button type="button" class="btn btn-primary imeikeres">Keresés</button>
								</div>
							</div>
						</div>
						<div class="col-md-12 form-group">
							<label for="eredmeny">Eredmény</label>
							<textarea id="eredmeny" rows="4" class="form-control" readonly=""></textarea>
							<input type="hidden" id="eredmeny_id">
							<input type="hidden" id="eredmeny_text">
						</div>
						<div class="mx-3 alert alert-danger d-none" role="alert" id="warning">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary d-none">Kiválaszt</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Mégse</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="dialog_sim_message" tabindex="-1" role="dialog" >
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">SIM keresése</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Bezár"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 form-group">
							<label for="search_sim">Keresés icc/telefonszám alapján</label>
							<div class="input-group">
								<input type="text" name="search_sim" id="search_sim" class="form-control" placeholder="Rövidített keresés, pl: %0966" maxlength="15">
								<div class="input-group-append">
									<button type="button" class="btn btn-primary simkeres">Keresés</button>
								</div>
							</div>
						</div>
						<div class="col-md-12 form-group">
							<label for="eredmeny_sim">Eredmény</label>
							<textarea id="eredmeny_sim" rows="4" class="form-control" readonly=""></textarea>
							<input type="hidden" id="eredmeny_sim_id">
							<input type="hidden" id="eredmeny_sim_text">
						</div>
						<div class="mx-3 alert alert-danger d-none" role="alert" id="warning">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary d-none">Kiválaszt</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Mégse</button>
				</div>
			</div>
		</div>
	</div>
	@endcan
</div>

@endsection