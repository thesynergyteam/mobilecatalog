<tr class="bg-transparent">
	<td colspan="5" class="py-0">
		<div class="d-flex align-items-center">
			<span class="m-3">Találat: {{ $sims->total() }}</span> {!! $sims->links() !!}
		</div>
	</td>
</tr>
@foreach ($sims as $sim)
<tr class="{{ $sim->simstatusz_id<=2?'':'text-danger' }}">
	<td><span class="fas fa-lg fa-fw {{ $sim->simstatusz_id==1?'fa-warehouse':($sim->simstatusz_id==2?'fa-user':($sim->simstatusz_id==4?'fa-sync-alt':'fa-trash-alt')) }}"></span> {{ $sim->icc }}</td>
	<td>{{ $sim->beszallito->partner }}</td>
	<td><span class="fas fa-lg fa-fw {{ $sim->tipus=='hang'?'fa-phone':'fa-wifi' }}"></span> {{ $sim->telefonszam }}</td>
	<td>{{ $sim->felhasznalo->nev_tsz }}</td>
	<td>
	<a href="{{ route('sims.edit', $sim->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Részletek</a>
	</td>
</tr>
@endforeach
<tr class="bg-transparent">
	<td colspan="5">
	{!! $sims->links() !!}
	</td>
</tr>
