@extends('layouts.app')

@section('title', 'Új sim kártya hozzáadása')

@section('sims', 'active')
@section('db', 'active')

@push('head')
	<link href="{{ asset('css/bootstrap-datepicker3.min.css') }}"  rel="stylesheet">
	<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" defer></script>
	<script src="{{ asset('js/bootstrap-datepicker.hu.min.js') }}" defer></script>
@endpush

@section('content')

<div class='col-xl-6 col-md-8 offset-xl-3 offset-md-2'>

    <h1><i class='fas fa-sim-card'></i> Új sim kártya hozzáadása</h1>
    <br>

    <form action="{{ url('sims') }}" method="POST">
        @csrf
		<div class="row">
			<div class="col-md-3 form-group">
				<label for="icc">ICC<span class="text-danger">*</span></label>
				<input type="text" name="icc" id="icc" class="form-control" value="{{old('icc')}}" maxlength="12" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
			</div>
			<div class="col-md-4 form-group">
				<label for="beszallito">Beszállító<span class="text-danger">*</span></label>
				<select class="custom-select" name="beszallito" id="beszallito">
					<option value="" selected disabled>Válassz beszállítót!</option>
					@foreach(\App\Beszallito::all() as $besz)
					<option value="{{$besz->id}}" {{ old('beszallito')==$besz->id?'selected':'' }}>{{$besz->partner}} ({{$besz->kontakt}})</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-4 form-group">
				<label for="megrendelesszam">Megrendelés szám<span class="text-danger">*</span></label>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">ORDER-</span>
					</div>
					<input type="text" name="megrendelesszam" id="megrendelesszam" class="form-control" value="{{old('megrendelesszam')}}" maxlength="11" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 form-group">
				<label for="telefonszam">Telefonszám<span class="text-danger">*</span></label>
				<input type="text" name="telefonszam" id="telefonszam" class="form-control" value="{{old('telefonszam')}}" maxlength="11" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="36201234657">
			</div>
			<div class="col-md-2 form-group">
				<label for="tipus">Típus</label>
				<select class="custom-select" name="tipus" id="tipus">
					<option value="hang" {{ old('tipus')=='adat'?'':'selected' }}>hang</option>
					<option value="adat" {{ old('tipus')=='adat'?'selected':'' }}>adat</option>
				</select>
			</div>
			<div class="col-md-3 form-group">
				<label for="szallitolevelszam">Szállítólevél szám<span class="text-danger">*</span></label>
				<input type="text" name="szallitolevelszam" id="szallitolevelszam" class="form-control" value="{{ old('szallitolevelszam')}}" >
			</div>
			<div class="col-md-3 form-group">
				<label for="rendelesszam">Rendelés szám<span class="text-danger">*</span></label>
				<input type="text" name="rendelesszam" id="rendelesszam" class="form-control" value="{{ old('rendelesszam')}}" >
			</div>
		</div>
		<div class="row">
			<div class="col-6 col-sm-3 col-md-2 form-group">
				<label for="pin1">PIN1</label>
				<input type="text" name="pin1" id="pin1" class="form-control" value="{{ old('pin1') }}"  maxlength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
			</div>
			<div class="col-6 col-sm-3 col-md-2 form-group">
				<label for="pin2">PIN2</label>
				<input type="text" name="pin2" id="pin2" class="form-control" value="{{ old('pin2') }}"  maxlength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
			</div>
			<div class="col-6 col-sm-3 col-md-2 pr-md-0 form-group">
				<label for="puk1">PUK1</label>
				<input type="text" name="puk1" id="puk1" class="form-control" value="{{ old('puk1') }}"  maxlength="8" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
			</div>
			<div class="col-6 col-sm-3 col-md-2 pr-md-0 form-group">
				<label for="pin2">PUK2</label>
				<input type="text" name="puk2" id="puk2" class="form-control" value="{{ old('puk2') }}"  maxlength="8" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
			</div>
			<div class="col-6 col-md-3 form-group">
				<label for="datum">Szállítás<span class="text-danger">*</span></label>
				<input type="text" name="datum" id="datum" class="form-control" data-provide="datepicker" data-date-days-of-week-highlighted="6,0" data-date-today-highlight="true" data-date-max-view-mode="2" data-date-language="hu" data-date-autoclose="true" data-date-format="yyyy.mm.dd" value="{{ old('datum') }}" readonly="" placeholder="dátum">
			</div>
			<div class="col-6 col-md-3 form-group">
				<label for="netcsomag_id">Net csomag</label>
				<select class="custom-select" name="netcsomag_id" id="netcsomag_id">
					@foreach(\DB::table('mobile__net_csomagok')->get() as $csomag)
					<option value="{{$csomag->id}}" {{ old('netcsomag_id')==$csomag->id?'selected':''}}>{{$csomag->nev}} {{$csomag->limit}} ({{$csomag->down}}/{{$csomag->up}})</option>
					@endforeach
				</select>
			</div>
			<div class="col-12 col-sm-6 col-md-8  form-group">
				<label for="beruhazon">Beruházási azonosító</label>
				<input type="text" name="beruhazon" id="beruhazon" class="form-control" value="{{ old('beruhazon') }}"  maxlength="24" >
			</div>
		</div>
        <br>
        <button type="submit" class="btn btn-primary" >Hozzáadás</button>
		<button type="submit" class="btn btn-primary" name="saveandnext">Létrehoz és következő</button>
        <button type="button" class="btn btn-secondary" onclick="window.location='{{ route('sims.index')}}'">Mégse</button>

    </form>

</div>

@endsection