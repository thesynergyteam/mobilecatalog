@extends('layouts.app')

@section('title', 'Sim kártya szerkesztése')

@section('sims', 'active')
@section('db', 'active')

@push('head')
	<link href="{{ asset('css/bootstrap-datepicker3.min.css') }}"  rel="stylesheet">
	<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" defer></script>
	<script src="{{ asset('js/bootstrap-datepicker.hu.min.js') }}" defer></script>
@endpush

@section('content')

<div class='col-xl-6 col-md-8 offset-xl-3 offset-md-2'>

    <h1><i class='fa fa-sim-card'></i> Sim kártya részletek </h1>
    <br>
    <form action="{{ route('sims.update', $sim->id) }}" method="POST">
        @method('PUT')
        @csrf
		<div class="row">
			<div class="col-md-3 form-group">
				<label for="icc">ICC</label>
				<input type="text" name="icc" id="icc" class="form-control" value="{{$sim->icc}}" readonly="">
			</div>
			<div class="col-md-4 form-group">
				<label for="beszallito">Beszállító</label>
				<input type="text" name="beszallito" id="beszallito" class="form-control" value="{{$sim->beszallito->partner}} ({{$sim->beszallito->kontakt}})" readonly="">
			</div>
			<div class="col-md-4 form-group">
				<label for="megrendelesszam">Megrendelés szám<span class="text-danger">*</span></label>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">ORDER-</span>
					</div>
					<input type="text" name="megrendelesszam" id="megrendelesszam" class="form-control" value="{{$sim->megrendelesszam}}" @can('SIM kártyák szerkesztése') @else readonly="" @endcan >
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 form-group">
				<label for="telefonszam">Telefonszám<span class="text-danger">*</span></label>
				<input type="text" name="telefonszam" id="telefonszam" class="form-control" value="{{$sim->telefonszam}}" @can('SIM kártyák szerkesztése') maxlength="11" onkeypress="return event.charCode >= 48 && event.charCode <= 57" @else readonly="" @endcan>
			</div>
			<div class="col-md-2 form-group">
				<label for="tipus">Típus</label>
				@can('SIM kártyák szerkesztése')
				<select class="custom-select" name="tipus" id="tipus">
					<option value="hang" @if($sim->tipus=='hang') selected @endif>hang</option>
					<option value="adat" @if($sim->tipus=='adat') selected @endif>adat</option>
				</select>
				@else
				<input type="text" name="tipus" id="tipus" class="form-control" value="{{ $sim->tipus }}" readonly="">
				@endcan
			</div>
			<div class="col-md-3 form-group">
				<label for="szallitolevelszam">Szállítólevél szám<span class="text-danger">*</span></label>
				<input type="text" name="szallitolevelszam" id="szallitolevelszam" class="form-control" value="{{ $sim->szallitolevelszam}}" @can('SIM kártyák szerkesztése') @else readonly="" @endcan >
			</div>
			<div class="col-md-3 form-group">
				<label for="rendelesszam">Rendelés szám<span class="text-danger">*</span></label>
				<input type="text" name="rendelesszam" id="rendelesszam" class="form-control" value="{{ $sim->rendelesszam}}" @can('SIM kártyák szerkesztése') @else readonly="" @endcan>
			</div>
		</div>
		<div class="row">
			<div class="col-6 col-sm-3 col-md-2 form-group">
				<label for="pin1">PIN1</label>
				<input type="text" name="pin1" id="pin1" class="form-control" value="{{ $sim->pin1 }}" @can('SIM kártyák szerkesztése') maxlength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57" @else readonly="" @endcan>
			</div>
			<div class="col-6 col-sm-3 col-md-2 form-group">
				<label for="pin2">PIN2</label>
				<input type="text" name="pin2" id="pin2" class="form-control" value="{{ $sim->pin2 }}" @can('SIM kártyák szerkesztése') maxlength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57" @else readonly="" @endcan>
			</div>
			<div class="col-6 col-sm-3 col-md-2 pr-md-0 form-group">
				<label for="puk1">PUK1</label>
				<input type="text" name="puk1" id="puk1" class="form-control" value="{{ $sim->puk1 }}" @can('SIM kártyák szerkesztése') maxlength="8" onkeypress="return event.charCode >= 48 && event.charCode <= 57" @else readonly="" @endcan>
			</div>
			<div class="col-6 col-sm-3 col-md-2 pr-md-0 form-group">
				<label for="pin2">PUK2</label>
				<input type="text" name="puk2" id="puk2" class="form-control" value="{{ $sim->puk2 }}" @can('SIM kártyák szerkesztése') maxlength="8" onkeypress="return event.charCode >= 48 && event.charCode <= 57" @else readonly="" @endcan>
			</div>
			<div class="col-6 col-md-3 form-group">
				<label for="datum">Szállítás<span class="text-danger">*</span></label>
				@can('SIM kártyák szerkesztése')
				<input type="text" name="datum" id="datum" class="form-control" data-provide="datepicker" data-date-days-of-week-highlighted="6,0" data-date-today-highlight="true" data-date-max-view-mode="2" data-date-language="hu" data-date-autoclose="true" data-date-format="yyyy.mm.dd" value="{{$sim->datum}}" readonly="" placeholder="dátum">
				@else
				<input type="text" name="datum" id="datum" class="form-control" value="{{$sim->datum}}" readonly="">
				@endcan
			</div>
		</div>
		<div class="row">
			<div class="col-6 col-md-3 form-group">
				<label for="simstatusz_id">Státusz</label>
				@can('SIM kártyák szerkesztése')
				<select class="custom-select" name="simstatusz_id" id="simstatusz_id">
					@foreach(\DB::table('mobile__simstatusz')->get() as $statusz)
					<option value="{{$statusz->id}}" {{ $sim->simstatusz_id==$statusz->id?'selected':''}}>{{$statusz->nev}}</option>
					@endforeach
				</select>
				@else
				<input type="text" class="form-control" value="{{ \DB::table('mobile__simstatusz')->find($sim->simstatusz_id)->nev }}" readonly="">
				<input type="hidden" name="simstatusz_id" value="{{ $sim->simstatusz_id }}">
				@endcan
			</div>
			<div class="col-6 col-md-4 form-group">
				<label for="netcsomag_id">Net csomag</label>
				<select class="custom-select" name="netcsomag_id" id="netcsomag_id">
					@foreach(\DB::table('mobile__net_csomagok')->get() as $csomag)
					<option value="{{$csomag->id}}" {{ $sim->netcsomag_id==$csomag->id?'selected':''}}>{{$csomag->nev}} {{$csomag->limit}} ({{$csomag->down}}/{{$csomag->up}})</option>
					@endforeach
				</select>
			</div>
			<div class="col-6 col-md-4 form-group">
				<label for="nev">Felhasználó</label>
				<input type="text" name="nev" id="nev" class="form-control" value="{{$sim->felhasznalo->nev}}" readonly="">
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-sm-6  form-group">
				<label for="beruhazon">Beruházási azonosító</label>
				<input type="text" name="beruhazon" id="beruhazon" class="form-control" value="{{ $sim->beruhazasi_azon }}"  maxlength="24" >
			</div>
		</div>
        <br>
        <button type="submit" class="btn btn-primary" >Mentés</button>
        <button type="button" class="btn btn-secondary" onclick="window.location='{{ route('sims.index')}}'">Vissza</button>

    </form>

</div>

@endsection