@extends('layouts.app')

@section('title', 'Új sim kártyák bevételezése')

@section('transactions', 'active')
@section('masssim', 'active')

@push('head')
	<link href="{{ asset('css/bootstrap-datepicker3.min.css') }}"  rel="stylesheet">
	<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" defer></script>
	<script src="{{ asset('js/bootstrap-datepicker.hu.min.js') }}" defer></script>
	<script src="{{ asset('js/jquery.csv.min.js') }}" type='text/javascript' defer></script>
	<script src="{{ asset('js/masssim.js') }}" defer></script>
	<style>
		.close{
			position: absolute;
			right: 5px;
		}
		.fileInput{
			position: relative;
			margin-left: auto;
		}
		#txtFileUpload{
			position: relative;
			opacity: 0;
			width: 150px;
			height: 37px;
			z-index: 2;
		}
		.fileInput .btn-warning{
			position: absolute;
			top: 0;
			left: 0;
		}
		.sticky-top {
			z-index: 10;
		}
	</style>
@endpush

@section('content')

<div class='col-xl-6 col-md-8 offset-xl-3 offset-md-2'>

    <h1><i class='fas fa-sim-card'></i> Új sim kártyák bevételezése</h1>
    <br>

    <form action="{{ url('sims') }}" method="POST">
        @csrf

		<input type="hidden" name="mode" value="mass">
		<div class="row">
			<div class="col-sm-6 form-group">
				<label for="beszallito">Beszállító<span class="text-danger">*</span></label>
				<select class="custom-select" name="beszallito" id="beszallito">
					<option value="" selected disabled>Válassz beszállítót!</option>
					@foreach(\App\Beszallito::all() as $besz)
					<option value="{{$besz->id}}" {{ old('beszallito')==$besz->id?'selected':'' }}>{{$besz->partner}} ({{$besz->kontakt}})</option>
					@endforeach
				</select>
			</div>
			<div class="col-sm-6 form-group">
				<label for="megrendelesszam">Megrendelés szám<span class="text-danger">*</span></label>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">ORDER-</span>
					</div>
					<input type="text" name="megrendelesszam" id="megrendelesszam" class="form-control" value="{{old('megrendelesszam')}}" maxlength="11" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3 form-group">
				<label for="szallitolevelszam">Szállítólevél szám<span class="text-danger">*</span></label>
				<input type="text" name="szallitolevelszam" id="szallitolevelszam" class="form-control" value="{{ old('szallitolevelszam')}}" >
			</div>
			<div class="col-sm-3 form-group">
				<label for="rendelesszam">Rendelés szám<span class="text-danger">*</span></label>
				<input type="text" name="rendelesszam" id="rendelesszam" class="form-control" value="{{ old('rendelesszam')}}" >
			</div>
			<div class="col-sm-2 form-group">
				<label for="datum">Dátum<span class="text-danger">*</span></label>
				<input type="text" name="datum" id="datum" class="form-control" data-provide="datepicker" data-date-days-of-week-highlighted="6,0" data-date-container="body" data-date-today-highlight="true" data-date-max-view-mode="2" data-date-language="hu" data-date-autoclose="true" data-date-format="yyyy.mm.dd" value="{{ old('datum') }}" readonly="" placeholder="dátum">
			</div>
			<div class="col-sm-4 form-group">
				<label for="beruhazon">Beruházási azonosító</label>
				<input type="text" name="beruhazon" id="beruhazon" class="form-control" value="">
			</div>
		</div>
		<hr>
		<div class="form-row">
			<div class="col-6 col-sm-4">
				<input type="text" class="form-control" id="icc_input" maxlength="12" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="ICC">
			</div>
			<div class="col-6 col-sm-3">
				<input type="text" class="form-control" id="telefonszam_input" maxlength="11" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Tel.szám: 36201234657">
			</div>
			<div class="col-6 col-sm-2">
				<select class="custom-select" id="tipus_input">
					<option value="hang" selected>hang</option>
					<option value="adat">adat</option>
				</select>
			</div>
			<div class="col-6 col-sm-3 form-group">
				<select class="custom-select" id="netcsomag_input">
					@foreach(\DB::table('mobile__net_csomagok')->get() as $csomag)
					<option value="{{$csomag->id}}">{{$csomag->nev}} {{$csomag->limit}} ({{$csomag->down}}/{{$csomag->up}})</option>
					@endforeach
				</select>
			</div>
			<div class="ml-auto pr-1">
				<button type="button" class="btn btn-primary simadd">Hozzáad</button>
			</div>
		</div>
		<div class="form-row mt-1">
			<div class="col-12">
				<ul class="list-group">
					@if(old('icc'))
                        @for( $i =0; $i < count(old('icc')); $i++)
					<li class="list-group-item">
						<div class="row">
							<input type="hidden" name="icc[]" value="{{ old('icc.'.$i) }}">
							<input type="hidden" name="telefonszam[]" value="{{ old('telefonszam.'.$i) }}">
							<input type="hidden" name="tipus[]" value="{{ old('tipus.'.$i) }}">
							<input type="hidden" name="netcsomag[]" value="{{ old('netcsomag.'.$i) }}">
							<span class="col-sm-5">ICC: {{ old('icc.'.$i) }}</span>
							<span class="col-7 col-sm-4">Tel.: {{ old('telefonszam.'.$i) }}</span>
							<span class="col-5 col-sm-3">típus: {{ old('tipus.'.$i) }} @if(old('netcsomag.'.$i)>1) <span class="fas fa-wifi"></span> @endif</span>
							<button type="button" class="close">&times;</button>
						</div>
					</li>
                        @endfor
                    @endif

				</ul>
			</div>
		</div>
        <br>
		<div class="d-flex">
			<button type="submit" class="btn btn-primary" >Rögzítés</button>
			<button type="button" class="btn btn-secondary" onclick="window.location='{{ route('sims.index')}}'">Mégse</button>
			<div class="fileInput">
				<input type="file" id="txtFileUpload" accept=".csv">
				<button type="button" class="btn btn-warning btn-block" >CSV import</button>
			</div>
		</div>

    </form>

</div>

@endsection