@extends('layouts.app')

@section('title', 'Sim kártyák')

@section('sims', 'active')
@section('db', 'active')

@push('head')
	<script src="{{ asset('js/pagination.js') }}" defer></script>
	<style>
		.pagination{
			margin: 0 !important;
		}
	</style>
@endpush

@section('content')

<div class="col-xl-8 offset-xl-2">
    <h1><i class="fas fa-sim-card"></i> Sim kártyák</h1>
    <hr>
	@can('SIM kártyák szerkesztése')<a href="{{ route('sims.create') }}" class="btn btn-success mb-2">Új sim kártya hozzáadása</a>@endcan
	@can('SIM kártyák szerkesztése')<a href="{{ route('sims.masscreate') }}" class="btn btn-success mb-2">Új sim kártyák tömeges hozzáadása</a>@endcan
    <div class="table-responsive filtertable">
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
					<th><form class="form-inline"><label>ICC</label><input type="search" class="form-control form-control-sm ml-auto ajax-filter" data-column="icc" placeholder="Szűkítés..." /></form></th>
                    <th>Beszállító</th>
					<th><form class="form-inline"><label>Tel.szám</label><input type="search" class="form-control form-control-sm ml-auto ajax-filter" data-column="telefonszam" placeholder="Szűkítés..." /></form></th>
					<th><form class="form-inline"><label>Felhasználó</label><input type="search" class="form-control form-control-sm ml-auto ajax-filter" data-column="_felhasznalo_nev" placeholder="Szűkítés..." /></form></th>
                    <th>Műveletek</th>
                </tr>
            </thead>

            <tbody>
				@include('sims/pagination_data')
            </tbody>

        </table>
		<input type="hidden" name="hidden_page" id="hidden_page" value="1" />
		<input type="hidden" name="hidden_url" id="hidden_url" value="sims" />
		<input type="hidden" name="hidden_column" id="hidden_column" value="telefonszam" />
		<input type="hidden" name="hidden_query" id="hidden_query" value="" />
    </div>
</div>

@endsection