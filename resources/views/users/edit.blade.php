@extends('layouts.app')

@section('title', 'Felhasználó szerkesztése')

@section('content')

<div class='col-md-6 offset-md-3'>

    <h1><i class='fa fa-user'></i> {{$user->name}} szerkesztése</h1>
    <hr>
    <form action="{{ route('users.update', $user->id) }}" method="POST">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="username">Felhasználó név</label>
            <input type="text" name="username" class="form-control" readonly value="{{ $user->username }}" >
        </div>

        <h4><b>Szerepkör módosítás</b></h4>
		<h6><b>A SuperAdmin kivételével a többi szerepkör ideiglenesen állítható csak itt, címtárból visszaszinkronizál!</b></h6>

        <div class='form-group'>
            @foreach ($roles as $role)
            <input type="checkbox" name="roles[]" id="{{$role->name}}" value="{{$role->id}}" {{ $user->roles->contains('id',$role->id )?'checked':''}}>
            <label for="{{$role->name}}">{{ucfirst($role->name)}}<br></label><br>
            @endforeach
        </div>
        <button type="submit" class="btn btn-primary" >Mentés</button>
        <button type="button" class="btn btn-secondary" onclick="window.location='/users'">Mégse</button>

    </form>

</div>

@endsection