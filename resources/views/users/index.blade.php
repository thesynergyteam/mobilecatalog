@extends('layouts.app')

@section('title', 'Felhasználók')

@push('head')
	<script src="{{ asset('js/sorting.js') }}" defer></script>
@endpush

@section('content')

<div class="col-xl-8 offset-xl-2">
    <h1><i class="fa fa-users"></i> Felhasználó adminisztráció
	<a href="{{ route('roles.index') }}" class="btn btn-outline-secondary pull-right">Szerepkörök</a>
    <a href="{{ route('permissions.index') }}" class="btn btn-outline-secondary pull-right">Jogosultságok</a></h1>
    <hr>
    <div class="table-responsive filtertable">
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
                    <th><form class="form-inline"><label>Név</label><input type="search" class="form-control form-control-sm ml-auto txt-filter" data-filter="f-nev" placeholder="Szűkítés..." /></form></th>
                    <th>Usernév</th>
                    <th>Utolsó bejelentkezés</th>
                    <th><form class="form-inline"><label>Felhasználó szerepkörei</label><input type="search" class="form-control form-control-sm ml-auto txt-filter" data-filter="f-role" placeholder="Szűkítés..." /></form></th>
                    <th>Műveletek</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($users as $user)
                <tr>

                    <td class='f-nev'>{{ $user->name }}</td>
                    <td>{{ $user->username }}</td>
                    <td>{{ $user->last_login }}</td>
                    <td class='f-role'>{{  $user->roles()->pluck('name')->implode(', ') }}</td>{{-- Retrieve array of roles associated to a user and convert to string --}}
                    <td>
                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Szerkeszt</a>
                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>

</div>

@endsection