/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

$.ajaxSetup({
  headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
		'Content-Type':'application/json'
  }
});

$('.tszkeres').click(function(){
	if ($('#tsz').val().trim()=='') return;
	$.get('/querytsz/'+$('#tsz').val().trim())
		.done(function( adat ) {
			if (adat.error===false && adat.user!==null){
				$('#usernev_full').val(adat.user.dolgozo_nev+' ('+adat.user.nev+')');
				$('#usernev').val(adat.user.dolgozo_nev);
			} else {
				$('#usernev_full').val('nincs találat');
			}
		})
		.fail(function( jqXHR ) {
			if ( jqXHR.status== 401 ) {
				window.location.assign("/login");
			}
		});
});

$('.imeikeres').click(function(){
	$('#warning,#dialog_device .modal-footer .btn-primary').addClass('d-none');
	$('#eredmeny_id,#eredmeny_text').val('');
	if ($('#search_imei').val().trim()=='') return;
	$.get('/queryimei/'+encodeURIComponent($('#search_imei').val().trim())+'?'+$('form').serialize())
		.done(function( adat ) {
			if (adat.error===false && adat.eszkoz!==null){
				var eredmeny='';
				eredmeny +="Típus: "+adat.eszkoz.tipus.gyarto+' '+adat.eszkoz.tipus.tipus;
				eredmeny +="\nStátusz: "+adat.eszkoz.statusz.nev;
				eredmeny +="\nFelhasználó: "+adat.eszkoz.felhasznalo.nev;
				eredmeny +="\nGyártási szám: "+adat.eszkoz.gyartasiszam;
				$('#eredmeny_id').val(adat.eszkoz.id);
				$('#eredmeny_text').val(adat.eszkoz.tipus.gyarto+' '+adat.eszkoz.tipus.tipus+' ('+adat.eszkoz.IMEI+')');
				$('#eredmeny').val(eredmeny);
				$('#search_imei').val(adat.eszkoz.IMEI);
				switch(adat.eszkoz.statusz_id){
					case 1 : $('#warning').html('Hozzá van rendelve valakihez az eszköz!<br>(Nem készült visszavét konfig)').removeClass('d-none');
								$('#dialog_device .modal-footer .btn-primary').removeClass('d-none'); break;
					case 2 : $('#warning').text('Selejtezett eszköz!').removeClass('d-none'); break;
					case 3 : $('#warning').text('Garancián van az eszköz!').removeClass('d-none'); break;
					case 4 : $('#warning').text('Ellopott eszköz!').removeClass('d-none'); break;
					default: $('#dialog_device .modal-footer .btn-primary').removeClass('d-none');
				}
			} else {
				$('#eredmeny').val('Nincs találat!');
			}
		})
		.fail(function( jqXHR ) {
			if ( jqXHR.status== 401 ) {
				window.location.assign("/login");
			}
		});
});

$('.simkeres').click(function(){
	$('#warning_sim,#dialog_sim_message .modal-footer .btn-primary').addClass('d-none');
	$('#eredmeny_sim_id,#eredmeny_sim_text').val('');
	if ($('#search_sim').val().trim()=='') return;
	$.get('/querysim/'+$(this).data('sim')+'/'+encodeURIComponent($('#search_sim').val().trim())+'?ignore_inactive=1&'+$('form').serialize())
		.done(function( adat ) {
			if (adat.error===false && adat.sim!==null){
				var eredmeny='';
				eredmeny +="ICC: "+adat.sim.icc;
				eredmeny +="\nTelefonszám: "+adat.sim.telefonszam;
				eredmeny +="\nTípus: "+adat.sim.tipus;
				eredmeny +="\nFelhasználó: "+adat.sim.felhasznalo.nev;
				$('#eredmeny_sim_id').val(adat.sim.id);
				$('#eredmeny_sim_text').val(adat.sim.icc+' ('+adat.sim.telefonszam+')');
				$('#eredmeny_sim').val(eredmeny);
				if (adat.sim.simstatusz_id<3){
					if (adat.sim.felhasznalo.tsz!==''){
						$('#warning_sim').html('Hozzá van rendelve valakihez a sim kártya!<br>(Nem készült visszavét konfig)').removeClass('d-none');
					}
					$('#dialog_sim_message .modal-footer .btn-primary').removeClass('d-none');
				} else {
					$('#warning_sim').text('Inaktivált SIM kártya!').removeClass('d-none');
				}
			} else {
				$('#eredmeny_sim').val('Nincs találat!');
			}
		})
		.fail(function( jqXHR ) {
			if ( jqXHR.status== 401 ) {
				window.location.assign("/login");
			}
		});
});

$('#tsz').change(function(){
	$('#usernev, #usernev_full').val('');
});

$('form').on('click', '.delete',function(){
	$(this).closest('.input-group').remove();
});
$('.newdeviceadd').click(function(){
	$('#search_imei, #eredmeny,#eredmeny_id,#eredmeny_text').val('');
	$('#dialog_device .modal-footer .btn-primary,#warning').addClass('d-none');
	$('#dialog_device').modal('show');
});
$('.newsimadd').click(function(){
	$('#search_sim, #eredmeny_sim,#eredmeny_sim_id,#eredmeny_sim_text').val('');
	$('#dialog_sim_message .modal-footer .btn-primary,#warning').addClass('d-none');
	$('#dialog_sim_message').modal('show');
});

$('#dialog_device .modal-footer .btn-primary').click(function(){
	if ($('#eredmeny_text').val()!=''){
		$('.eszkozmezo').append('<div class="input-group"><input type="text" class="form-control" readonly="" value="'+$('#eredmeny_text').val()+'"><input type="hidden" name="eszkoz_id[]" value="'+$('#eredmeny_id').val()+'"><div class="input-group-append"><button type="button" class="btn btn-danger delete">Töröl</button></div></div>');
	}
	$('#dialog_device').modal('hide');
});

$('#dialog_sim_message .modal-footer .btn-primary').click(function(){
	if ($('#eredmeny_sim_text').val()!=''){
		$('.simmezo').append('<div class="input-group"><input type="text" class="form-control" readonly="" value="'+$('#eredmeny_sim_text').val()+'"><input type="hidden" name="sim_id[]" value="'+$('#eredmeny_sim_id').val()+'"><div class="input-group-append"><button type="button" class="btn btn-danger delete">Töröl</button></div></div>');
	}
	$('#dialog_sim_message').modal('hide');
});

$(function(){

	$('#chkDevice').change(function(){
		if(!this.checked) {
			$('#eszkoz_nev, #eszkoz').val('').removeClass('bg-white cursor-pointer');
		} else {
			$('#eszkoz_nev').addClass('bg-white cursor-pointer');
		}
	});

	$('#chkSim').change(function(){
		if(!this.checked) {
			$('#sim_nev, #sim').val('').removeClass('bg-white cursor-pointer');
		} else {
			$('#sim_nev').addClass('bg-white cursor-pointer');
		}
	});

	$("#kieg_eszkoz_nev").keyup(function(){
        var search = $(this).val();

        if(search != "" && search.length>2){
			$.get('/queryaccessory/?'+$('form').serialize())
				.done(function( response ) {
					if (response.error===false && response.accessories!==null && response.accessories.length>0){
						$("#searchResult").empty();
						for( var i = 0; i<response.accessories.length; i++){
							var acc = response.accessories[i];
							var id = acc['id'];
							var name = acc['nev'];

							$("#searchResult").append("<li value='"+id+"'>"+name+"</li>");
						}
						// binding click event to li
						$("#searchResult li").bind("click",function(){
							setText(this);
						});
					}
				});

        } else {
			$("#searchResult").empty();
		}

    });
	function setText(element){
		$("#kieg_eszkoz_nev").val($(element).text()).data('id',$(element).val()).data('text',$(element).text());
		$("#searchResult").empty();
	}

	$('.deviceadd').click(function(){
		if ($("#kieg_eszkoz_nev").val()=='' || $("#kieg_eszkoz_nev").data('id')=='') return;
		$('<li>').addClass('list-group-item').append(
				$('<div>').addClass('row').append(
					$('<input>').prop({type:'hidden',name:'kieg_eszkoz_id[]'}).val($("#kieg_eszkoz_nev").data('id')))
				.append(
					$('<span>').addClass('col-11').text($("#kieg_eszkoz_nev").data('text')))
				.append(
					$('<button>').prop('type','button').addClass('close').html('&times;'))
				).prependTo($('ul.list-group'));
		$('#kieg_eszkoz_nev').val('').data('id','');
	});

	$('.list-group').on('click','.close', function(){
		$(this).closest('.list-group-item').remove();
	});
});
