/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2022(c) KoRi <kovacsr@fkf.hu>
 *
 */

$.ajaxSetup({
  headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
		'Content-Type':'application/json'
  }
});


$(function(){

	$('.tszkeres').click(function(){
		if ($('#tsz').val().trim()=='') return;
		$.get('/querytsz/'+$('#tsz').val().trim())
			.done(function( adat ) {
				if (adat.error===false && adat.user!==null){
					$('#usernev_full').val(adat.user.dolgozo_nev+' ('+adat.user.nev+')');
					$('#usernev').val(adat.user.dolgozo_nev);
					$('#koltseghely').val(adat.user.kh);
					if(adat.felhasznalo!==null){
						$('#szuletesi_ido').val(adat.felhasznalo.szuletesi_ido);
						$('#szemigszam').val(adat.felhasznalo.szemigszam);
						$('#anyjaneve').val(adat.felhasznalo.anyjaneve);
						$('#lakcim').val(adat.felhasznalo.lakcim);
					}
				} else {
					$('#usernev_full').val('nincs találat');
				}
			})
			.fail(function( jqXHR ) {
				if ( jqXHR.status== 401 ) {
					window.location.assign("/login");
				}
			});
	});

	$('.simkeres').click(function(){
		$('#warning_sim,#dialog_sim_message .modal-footer .btn-primary').addClass('d-none');
		$('#eredmeny_sim_id,#eredmeny_sim_text').val('');
		if ($('#search_sim').val().trim()=='') return;
		$.get('/querysim/'+$(this).data('sim')+'/'+encodeURIComponent($('#search_sim').val().trim())+'?ignore_inactive=1&'+$('form').serialize())
			.done(function( adat ) {
				if (adat.error===false && adat.sim!==null){
					var eredmeny='';
					eredmeny +="ICC: "+adat.sim.icc;
					eredmeny +="\nTelefonszám: "+adat.sim.telefonszam;
					eredmeny +="\nTípus: "+adat.sim.tipus;
					eredmeny +="\nFelhasználó: "+adat.sim.felhasznalo.nev;
					$('#eredmeny_sim_id').val(adat.sim.id);
					$('#eredmeny_sim_text').val(adat.sim.icc+' ('+adat.sim.telefonszam+')');
					$('#eredmeny_sim').val(eredmeny);
					if (adat.sim.simstatusz_id<3){
						if (adat.sim.felhasznalo.tsz!==''){
							$('#warning_sim').html('Hozzá van rendelve valakihez a sim kártya!<br>(Nem készült visszavét konfig)').removeClass('d-none');
						}
						$('#dialog_sim_message .modal-footer .btn-primary').removeClass('d-none');
					} else {
						$('#warning_sim').text('Inaktivált SIM kártya!').removeClass('d-none');
					}
				} else {
					$('#eredmeny_sim').val('Nincs találat!');
				}
			})
			.fail(function( jqXHR ) {
				if ( jqXHR.status== 401 ) {
					window.location.assign("/login");
				}
			});
	});

	$('#tsz').change(function(){
		$('#usernev_full, #usernev, #szuletesi_ido, #szemigszam, #koltseghely').val('');
	});

	$('.newsimadd').click(function(){
		$('#search_sim, #eredmeny_sim,#eredmeny_sim_id,#eredmeny_sim_text').val('');
		$('#dialog_sim_message .modal-footer .btn-primary,#warning').addClass('d-none');
		$('#dialog_sim_message').modal('show');
	});

	$('#dialog_sim_message .modal-footer .btn-primary').click(function(){
		if ($('#eredmeny_sim_text').val()!=''){
			$('#sim_text').val($('#eredmeny_sim_text').val());
			$('#sim_id').val($('#eredmeny_sim_id').val());
		}
		$('#dialog_sim_message').modal('hide');
	});
});
