/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */


$(document).ready(function(){

	function fetch_data(page, query, column)
	{
		$.ajax({
			url:"/"+$('#hidden_url').val()+"/fetch_data?page="+page+"&column="+column+"&query="+encodeURIComponent(query),
			success:function(data)
			{
				$('tbody').html('');
				$('tbody').html(data);
			}
		});
	}

	$(document).on('input', '.ajax-filter', function(){
		$('#hidden_column').val($(this).data('column'));
		$('#hidden_query').val($(this).val());
		fetch_data(1, $(this).val(), $(this).data('column'));
	});

	$(document).on('click', '.pagination a', function(event){
		event.preventDefault();
		var page = $(this).attr('href').split('page=')[1];
		$('#hidden_page').val(page);

		$('li').removeClass('active');
		$(this).parent().addClass('active');
		fetch_data(page, $('#hidden_query').val(), $('#hidden_column').val() );
	});

});