/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

$.ajaxSetup({
  headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
		'Content-Type':'application/json'
  }
});

$(function() {
	$('.simadd').click(function(){
		if ($('#icc_input').val()=='' || $('#telefonszam_input').val()=='' || $('#telefonszam_input').val().length!==11) return;
		$('<li>').addClass('list-group-item').append(
				$('<div>').addClass('row').append(
					$('<input>').prop({type:'hidden',name:'icc[]'}).val($('#icc_input').val()))
				.append(
					$('<input>').prop({type:'hidden',name:'telefonszam[]'}).val($('#telefonszam_input').val()))
				.append(
					$('<input>').prop({type:'hidden',name:'tipus[]'}).val($('#tipus_input').val()))
				.append(
					$('<input>').prop({type:'hidden',name:'netcsomag[]'}).val($('#netcsomag_input').val()))
				.append(
					$('<span>').addClass('col-sm-5').text('ICC: '+$('#icc_input').val()))
				.append(
					$('<span>').addClass('col-7 col-sm-4').text('Tel.: '+$('#telefonszam_input').val()))
				.append(
					$('<span>').addClass('col-5 col-sm-3').html('típus: '+$('#tipus_input').val()+($('#netcsomag_input').val()>1?' <span class="fas fa-wifi" title="'+$('#netcsomag_input option:selected').text()+'"></span>':'')))
				.append(
					$('<button>').prop('type','button').addClass('close').html('&times;'))
				).prependTo($('ul.list-group'));
		$('#icc_input, #telefonszam_input').val('');
		$('#netcsomag_input').val(1).change();
	});

	$('.list-group').on('click','.close', function(){
		$(this).closest('.list-group-item').remove();
	});

	//vezető nullákkal kiegészítés
	function pad(num, size) {
		while (num.length < size) num = "0" + num;
		return num;
	}

	//hang / adat sim megállapítása
	function tipus(tip) {
		return tip.includes('Hipernet')?'adat':'hang';
	}

	//net megállapítása
	function net(tip) {
		if (/Start/.test(tip)) { return 2; }
		if (/Active/.test(tip)) { return 3; }
		if (/Medium/.test(tip)) { return 4; }
		if (/Heavy/.test(tip)) { return 5; }
		if (/Unlimited/.test(tip)) { return 6; }
		return 1;
	}

	$("#txtFileUpload").change(function(evt){ //csv importálás
		$('.hibauzenet').remove();
		var data = null;
		var file = evt.target.files[0];
		var reader = new FileReader();
		var pd; var poststring;
		reader.readAsText(file);
		reader.onload = function(event) {
			var csvData = event.target.result;
			data = $.csv.toArrays(csvData, {"separator":";"});
			if (data && data.length > 0) {
				console.log("Importing -" + data.length + "- rows...");
				var postCont ={
					'beszallito': $('#beszallito').val(),
					'datum': $('#datum').val()
				};
				var array=[];
				for (i = 0; i < data.length; i++){	//csv oszlopok: telszám;tarifacsomag;pin1;pin2;puk1;puk2;icc('893620xxxxxxx0F')
					if (data[i][0].match(/[A-Za-z]+/)) { continue;}
					pd = {};
					pd.telefonszam = (data[i][0].length==9?'36':'')+data[i][0];
					pd.tipus = tipus(data[i][1]);
					pd.pin1 = pad(data[i][2],4);
					pd.pin2 = pad(data[i][3],4);
					pd.puk1 = pad(data[i][4],8);
					pd.puk2 = pad(data[i][5],8);
					pd.icc = data[i][6].substr(6,12);
					pd.net = net(data[i][1]);
					array.push(pd);
				}
				postCont.data = array;
				poststring = JSON.stringify(postCont);
				console.log(poststring);
				$.post( '/sims/csvimport', poststring )
					.done(function(adat){
						if (adat.result==="success"){
							$('.navbar').after('<div class="container hibauzenet"><div class="alert alert-success"><p>Sikeres importálás!</p><p>Kihagyott darab: '+adat.kihagyott+'</p></div></div>');
						}
					})
					.fail(function(xhr){
						$('<div>').addClass('container hibauzenet').append($('<div>').addClass('alert alert-danger').append($('<ul>'))).insertAfter($('.navbar'));
						$.each(xhr.responseJSON.errors, function(key,value) {
							$('.hibauzenet ul').append('<li>'+value+'</li');
						});
					});
			} else {
				alert("Nincs adat importáláshoz!");
			}
			$("#txtFileUpload").wrap('<form>').closest('form').get(0).reset();
			$("#txtFileUpload").unwrap();
		};
		reader.onerror = function() {
			alert("Nem tudom olvasni: " + file.fileName);
		};
	});
});