/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */

$.ajaxSetup({
  headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
		'Content-Type':'application/json'
  }
});

$(function() {
	var postCont ={};
	$('.deviceadd').click(function(){
		if ($('#imei_input').val()=='' || $('#tipus_input').val()==$("#tipus_input option:first").val() || $('#ertek_input').val()=='') return;
		$('<li>').addClass('list-group-item').append(
				$('<div>').addClass('row').append(
					$('<input>').prop({type:'hidden',name:'tipus[]'}).val($('#tipus_input').val()))
				.append(
					$('<input>').prop({type:'hidden',name:'imei[]'}).val($('#imei_input').val()))
				.append(
					$('<input>').prop({type:'hidden',name:'imei2[]'}).val($('#imei2_input').val()))
				.append(
					$('<input>').prop({type:'hidden',name:'ertek[]'}).val($('#ertek_input').val()))
				.append(
					$('<input>').prop({type:'hidden',name:'leltari[]'}).val($('#leltari_input').val()))
				.append(
					$('<span>').addClass('col-sm-4').text($('#tipus_input option:selected').text()))
				.append(
					$('<span>').addClass('col-6 col-sm-3').text('IMEI.: '+$('#imei_input').val()))
				.append(
					$('<span>').addClass('col-6 col-sm-3').text('IMEI2: '+$('#imei2_input').val()))
				.append(
					$('<span>').addClass('col-sm-2').html('<span class="fas fa-barcode" title="Leltári szám: '+$('#leltari_input').val()+'"></span> érték: '+$('#ertek_input').val()))
				.append(
					$('<button>').prop('type','button').addClass('close').html('&times;'))
				).prependTo($('ul.list-group'));
		$('#imei_input, #imei2_input, #ertek_input, #leltari_input').val('');
		$('#tipus_input').val($("#tipus_input option:first").val()).change();
	});

	$('.list-group').on('click','.close', function(){
		$(this).closest('.list-group-item').remove();
	});

	$("#txtFileUpload").change(function(evt){ //csv importálás
		$('.hibauzenet').remove();
		var data = null;
		var file = evt.target.files[0];
		var reader = new FileReader();
		var pd;
		reader.readAsText(file);
		reader.onload = function(event) {
			var csvData = event.target.result;
			data = $.csv.toArrays(csvData, {"separator":";"});
			if (data && data.length > 0) {
				console.log("Importing -" + data.length + "- rows...");
				$("#import-modal .table tbody").children().remove();
				var array=[];
				for (i = 0; i < data.length; i++){	//csv oszlopok: típus(szöveges),imei,leltari szám, ár, dátum, beszállító, számlaszám,beruhazasi id;
					pd = {};
					pd.tipus = data[i][0].trim();
					pd.imei = data[i][1].trim();
					pd.leltari = data[i][2].trim();
					pd.ar = data[i][3].trim();
					pd.datum = data[i][4].trim();
					pd.beszallito = data[i][5].trim();
					pd.szamlaszam = data[i][6].trim();
					pd.usertsz = data[i][7]?data[i][7].trim():null;
					pd.beruhid = data[i][8]?data[i][8].trim():null;
					array.push(pd);
					if (i<3){
						$("#import-modal .table tbody").append("<tr><td>"+pd.tipus+"</td><td>"+pd.imei+"</td><td>"+pd.leltari+"</td><td>"+pd.ar+"</td><td>"+pd.datum+"</td><td>"+pd.beszallito+"</td><td>"+pd.szamlaszam+"</td><td>"+(pd.usertsz?pd.usertsz:'')+"</td><td>"+(pd.beruhid?pd.beruhid:'')+"</td></tr>");
					}
				}
				postCont.data = array;

				$("#import-modal").modal("show");
			} else {
				alert("Nincs adat importáláshoz!");
			}
			$("#txtFileUpload").wrap('<form>').closest('form').get(0).reset();
			$("#txtFileUpload").unwrap();
		};
		reader.onerror = function() {
			alert("Nem tudom olvasni: " + file.fileName);
		};
	});

	$(".editSave").click(function(){
		$.post( '/devices/csvimport', JSON.stringify(postCont) )
			.done(function(adat){
				if (adat.result==="success"){
					$("#import-modal").modal("hide");
					$('.navbar').after('<div class="container hibauzenet"><div class="alert alert-success"><p>Sikeres importálás!</p><p>Kihagyott darab: '+adat.kihagyott+'/'+adat.osszes+'</p><p>Kihagyott imei: '+adat.imei+'</p></div></div>');
				}
			})
			.fail(function(xhr){
				$('<div>').addClass('container hibauzenet').append($('<div>').addClass('alert alert-danger').append($('<ul>'))).insertAfter($('.navbar'));
				$.each(xhr.responseJSON.errors, function(key,value) {
					$('.hibauzenet ul').append('<li>'+value+'</li');
				});
			});

	});
});