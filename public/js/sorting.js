/*
 * This file is part of the MobileCatalog Software package.
 *
 * @copyright 2021(c) KoRi <kovacsr@fkf.hu>
 *
 */


$('.txt-filter').on('input',function(){
	var cl = '.'+$(this).data('filter');
	var filter = this.value.toUpperCase();
	$('.filtertable tbody tr').each(function(index){	// Loop through all list items, and hide those who don't match the search query
		if ($(this).find(cl).text().toUpperCase().indexOf(filter) >= 0){
			$(this).show();
		} else {
			$(this).hide();
		}
	});
});